#!/bin/env python3
import re
import collections
import itertools
import input_readers

def main():
    'read input data, process it if necessary and call the part workers'
    #data = input_readers.read_input_str()
    # data = input_readers.read_input_int_list_multiline()
    data = input_readers.read_input_str_list()
    # data = input_readers.read_input_table_tab()

    data = parse(data)

    tdata = parse('''position=< 9,  1> velocity=< 0,  2>
position=< 7,  0> velocity=<-1,  0>
position=< 3, -2> velocity=<-1,  1>
position=< 6, 10> velocity=<-2, -1>
position=< 2, -4> velocity=< 2,  2>
position=<-6, 10> velocity=< 2, -2>
position=< 1,  8> velocity=< 1, -1>
position=< 1,  7> velocity=< 1,  0>
position=<-3, 11> velocity=< 1, -2>
position=< 7,  6> velocity=<-1, -1>
position=<-2,  3> velocity=< 1,  0>
position=<-4,  3> velocity=< 2,  0>
position=<10, -3> velocity=<-1,  1>
position=< 5, 11> velocity=< 1, -2>
position=< 4,  7> velocity=< 0, -1>
position=< 8, -2> velocity=< 0,  1>
position=<15,  0> velocity=<-2,  0>
position=< 1,  6> velocity=< 1,  0>
position=< 8,  9> velocity=< 0, -1>
position=< 3,  3> velocity=<-1,  1>
position=< 0,  5> velocity=< 0, -1>
position=<-2,  2> velocity=< 2,  0>
position=< 5, -2> velocity=< 1,  2>
position=< 1,  4> velocity=< 2,  1>
position=<-2,  7> velocity=< 2, -2>
position=< 3,  6> velocity=<-1, -1>
position=< 5,  0> velocity=< 1,  0>
position=<-6,  0> velocity=< 2,  0>
position=< 5,  9> velocity=< 1, -2>
position=<14,  7> velocity=<-2,  0>
position=<-3,  6> velocity=< 2, -1>'''.splitlines())

    assert part1(tdata) == None
    print('part1:', part1(data))

    # assert part2('') ==
    print('part2:', part2(data))

def parse(input):
    # position=<-10436, -42528> velocity=< 1,  4>
    DataPoint = collections.namedtuple('DataPoint', ['x', 'y', 'dx', 'dy'])
    data2 = []
    for line in input:
        res = re.match(r'position=<(.*),(.*)> velocity=<(.*),(.*)>$', line)
        data2.append(DataPoint(*map(int, res.groups())))
    return tuple(data2)


def minmax(sky):
    minx = min([min(values) for values in sky.values()])
    maxx = max([max(values) for values in sky.values()])
    miny = min(sky.keys())
    maxy = max(sky.keys())
    return minx, maxx, miny, maxy


def print_sky(sky):
    minx, maxx, miny, maxy = minmax(sky)
    for y in range(miny, maxy+1):
        row = tuple(sky[y])
        for x in range(minx, maxx+1):
            print('#' if x in row else '.', end='')
        print()
    #input('press enter')

def part1(_data):
    'Do the part1 calculation'
    def calc_after(sec):
        res = {}  # {y: [x]}
        for dp in _data:
            x = dp.x + dp.dx*sec
            y = dp.y + dp.dy*sec
            res.setdefault(y, []).append(x)
        return res

    last_man = None  # manhattan distance between corners
    for sec in itertools.count():
        sky = calc_after(sec)
        minx, maxx, miny, maxy = minmax(sky)
        man = abs(maxy - miny) + abs(maxx - minx)
        if last_man != None and last_man < man:
            sec -= 1  # we want the last one
            break
        last_man = man
    print('sec', sec)
    print_sky(calc_after(sec))

def part2(_data):
    'Do the part2 calculation'
    pass


if __name__ == '__main__':
    main()
