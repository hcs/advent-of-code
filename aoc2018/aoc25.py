#!/bin/env python3

import functools
import time
import input_readers

def timeme(method):
    """Decorator that prints the elapsed time after a call"""
    @functools.wraps(method)
    def wrapper(*args, **kw):
        start_time = time.time()
        result = method(*args, **kw)
        end_time = time.time()
        print('timed', wrapper.__name__, end_time - start_time, 's')
        return result
    return wrapper


def main():
    'read input data, process it if necessary and call the part workers'
    #data = input_readers.read_input_str()
    # data = input_readers.read_input_int_list_multiline()
    # data = input_readers.read_input_str_list()
    data = input_readers.read_input_table_tab(sep=',')
    tdata = (
        (0,0,0,0),
        (3,0,0,0),
        (0,3,0,0),
        (0,0,3,0),
        (0,0,0,3),
        (0,0,0,6),
        (9,0,0,0),
        (12,0,0,0))

    assert part1(tdata) == 2
    print('part1:', part1(data))

    # assert part2('') ==
    print('part2:', part2(data))

def dist(pos1, pos2):
    #print(pos1, pos2, sum(map(abs, ((c1-c2) for (c1, c2) in zip(pos1, pos2)))))
    return sum(map(abs, ((c1-c2) for (c1, c2) in zip(pos1, pos2))))

def find_constellation(point, points):
    constellation = {point}
    #print('F', point, points)
    for point2 in tuple(points):
        if point2 not in constellation:
            if dist(point, point2) <= 3 :
                constellation.add(point2)
                points.remove(point2)
                constellation.update(find_constellation(point2, set(points)))
    #print('C', constellation)
    points -= constellation
    return constellation


@timeme
def part1(_data):
    'Do the part1 calculation'
    points = set(_data)
    #print(points)
    constellations = set()
    while points:
        point = points.pop()
        constellation = find_constellation(point, points)
        constellations.add(frozenset(constellation))
    #print(constellations)
    #print(len(constellations))
    return len(constellations)



@timeme
def part2(_data):
    'Do the part2 calculation'
    pass
    # No part 2 to solve today


if __name__ == '__main__':
    main()
