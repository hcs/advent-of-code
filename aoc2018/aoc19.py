#!/bin/env python3

import functools
import itertools
import time
import input_readers

def timeme(method):
    """Decorator that prints the elapsed time after a call"""
    @functools.wraps(method)
    def wrapper(*args, **kw):
        start_time = time.time()
        result = method(*args, **kw)
        end_time = time.time()
        print('timed', wrapper.__name__, end_time - start_time, 's')
        return result
    return wrapper


def main():
    'read input data, process it if necessary and call the part workers'
    #data = input_readers.read_input_str()
    # data = input_readers.read_input_int_list_multiline()
    data = input_readers.read_input_str_list()
    #data = input_readers.read_input_table_tab(sep=' ')
    data = [row.split() for row in data]
    data = [(inst, *map(int, vals)) for (inst, *vals) in data]

    tdata = '''#ip 0
seti 5 0 1
seti 6 0 2
addi 0 1 0
addr 1 2 3
setr 1 0 0
seti 8 0 4
seti 9 0 5'''.splitlines()
    tdata = [row.split() for row in tdata]
    tdata = [(inst, *map(int, vals)) for (inst, *vals) in tdata]
    #print(tdata)

    assert part1(tdata) == 6
    print('part1:', part1(data))

    # assert part2('') ==
    print('part2:', part2(data))

OPCODE = {
    'addr': lambda r, a, b: r[a] + r[b],
    'addi': lambda r, a, b: r[a] + b,
    'mulr': lambda r, a, b: r[a] * r[b],
    'muli': lambda r, a, b: r[a] * b,
    'banr': lambda r, a, b: r[a] & r[b],
    'bani': lambda r, a, b: r[a] & b,
    'borr': lambda r, a, b: r[a] | r[b],
    'bori': lambda r, a, b: r[a] | b,
    'setr': lambda r, a, b: r[a],
    'seti': lambda r, a, b: a,
    'gtir': lambda r, a, b: a > r[b],
    'gtri': lambda r, a, b: int(r[a] > b),
    'gtrr': lambda r, a, b: int(r[a] > r[b]),
    'eqir': lambda r, a, b: int(a == r[b]),
    'eqri': lambda r, a, b: int(r[a] == b),
    'eqrr': lambda r, a, b: int(r[a] == r[b]),

}

@timeme
def part1(_data):
    'Do the part1 calculation'

    prog = list(_data)
    r = regs = [0] * 6  # registers

    ip_inst, instruction_reg = prog.pop(0) # what register is the instruction pointer
    assert ip_inst == '#ip'

    last_addr = len(prog)-2

    for iteration in itertools.count():
        inst = prog[regs[instruction_reg]]
        if iteration % 1000000 == 0:
            print(f'it= {iteration} ip={regs[instruction_reg]} {regs} {inst} ') # , end='')

        #regs[inst[3]] = OPCODE[inst[0]](regs, inst[1], inst[2])

        opcode, a, b, c = inst

        if opcode == 'addr': r[c] = r[a] + r[b]
        elif opcode == 'addi': r[c] = r[a] + b
        elif opcode == 'mulr': r[c] = r[a] * r[b]
        elif opcode == 'muli': r[c] = r[a] * b
        elif opcode == 'banr': r[c] = r[a] & r[b]
        elif opcode == 'bani': r[c] = r[a] & b
        elif opcode == 'borr': r[c] = r[a] | r[b]
        elif opcode == 'bori': r[c] = r[a] | b
        elif opcode == 'setr': r[c] = r[a]
        elif opcode == 'seti': r[c] = a
        elif opcode == 'gtir': r[c] = a > r[b]
        elif opcode == 'gtri': r[c] = int(r[a] > b)
        elif opcode == 'gtrr': r[c] = int(r[a] > r[b])
        elif opcode == 'eqir': r[c] = int(a == r[b])
        elif opcode == 'eqri': r[c] = int(r[a] == b)
        elif opcode == 'eqrr': r[c] = int(r[a] == r[b])

        if regs[instruction_reg] < -1 or regs[instruction_reg] > last_addr:
            break  # program ended
        regs[instruction_reg] += 1
    #print(f'ip={regs[instruction_reg]} {regs} {inst} ')
    return regs[0]


@timeme
def part2(_data):
    'Do the part2 calculation'
    prog = list(_data)
    r = regs = [0] * 6  # registers
    regs[0] = 1

    ip_inst, instruction_reg = prog.pop(0) # what register is the instruction pointer
    assert ip_inst == '#ip'

    print('program formatted for analysis')
    for (addr, inst) in enumerate(prog):
        print('{:>2} {} {:>2} {:>2} {:>2} : '.format(addr, *inst), end='')
        if inst[-1] == instruction_reg:
            print('Jump ', end='')
        else:
            print(f'r{inst[-1]}=', end='')
        print()
    print('end of program')

    last_addr = len(prog)-2

    for iteration in itertools.count():
        inst = prog[regs[instruction_reg]]
        if iteration % 1000000 == 0:
            print(f'it= {iteration} ip={regs[instruction_reg]} {regs} {inst} ') # , end='')

        #regs[inst[3]] = OPCODE[inst[0]](regs, inst[1], inst[2])

        opcode, a, b, c = inst

        if opcode == 'addr': r[c] = r[a] + r[b]
        elif opcode == 'addi': r[c] = r[a] + b
        elif opcode == 'mulr': r[c] = r[a] * r[b]
        elif opcode == 'muli': r[c] = r[a] * b
        elif opcode == 'banr': r[c] = r[a] & r[b]
        elif opcode == 'bani': r[c] = r[a] & b
        elif opcode == 'borr': r[c] = r[a] | r[b]
        elif opcode == 'bori': r[c] = r[a] | b
        elif opcode == 'setr': r[c] = r[a]
        elif opcode == 'seti': r[c] = a
        elif opcode == 'gtir': r[c] = a > r[b]
        elif opcode == 'gtri': r[c] = int(r[a] > b)
        elif opcode == 'gtrr': r[c] = int(r[a] > r[b])
        elif opcode == 'eqir': r[c] = int(a == r[b])
        elif opcode == 'eqri': r[c] = int(r[a] == b)
        elif opcode == 'eqrr': r[c] = int(r[a] == r[b])

        if regs[instruction_reg] < -1 or regs[instruction_reg] > last_addr:
            break  # program ended
        regs[instruction_reg] += 1
    print(f'ip={regs[instruction_reg]} {regs} {inst} ')
    return regs[0]

if __name__ == '__main__':
    main()
