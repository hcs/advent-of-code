#!/bin/env python3
'''
r0 input
r1 work
r2 counter
r3 pointer
r4 biggest
r5 big


JT from 4
 0 seti       123         0  4 : r4=123
 1 bani         4       456  4 : r4= 123 ban 456 = 72
 2 eqri         4        72  4 : r4= r4 == 72
 3 addr         4         3  3 : Jump skip next if r4 == 72
 4 seti         0         0  3 : Jump 0
 5 seti         0         6  4 : r4=0

JT from 30
 6 bori         4     65536  5 : r5= r4 bor 65536
 7 seti   1855046         9  4 : r4= 1855046
JT from 27
 8 bani         5       255  2 : r2= r5 ban 255
 9 addr         4         2  4 : r4+=r2  = 1855046 + (r5 ban 255)
10 bani         4  16777215  4 : r4= r4 ban 16777215
11 muli         4     65899  4 : r4= r4*65899
12 bani         4  16777215  4 : r4= r4 ban 16777215
13 gtir       256         5  2 : r2= 256>r5
14 addr         2         3  3 : Jump skip next if 256>r5
15 addi         3         1  3 : Jump skip next r5>=256
16 seti        27         0  3 : Jump to 28

if r5>=256
17 seti         0         9  2 : r2=0

JT loop while r2=count(0) (r2+1)*256 <= r5(big)
18 addi         2         1  1 : r1=r2+1
19 muli         1       256  1 : r1=r1*256
20 gtrr         1         5  1 : r1=r1>r5
21 addr         1         3  3 : Jump skip next if (r2+1)*256 > r5
22 addi         3         1  3 : Jump skip next if else
23 seti        25         5  3 : Jump to 26 / break if (r2+1)*256 > r5
24 addi         2         1  2 : r2+=1  # else
25 seti        17         0  3 : Jump 18
# r1
# r2 = ...

26 setr         2         7  5 : r5=r2
27 seti         7         9  3 : Jump 8

JT from 16
28 eqrr         4         0  2 : r2=r4==r0
29 addr         2         3  3 : Jump END PROGRAM if r4==r0
30 seti         5         3  3 : Jump to 6
---------------


it= 0 ip=0 [356, 0, 0, 0, 0, 0] ('seti', 123, 0, 4)
it= 1 ip=1 [356, 0, 0, 1, 123, 0] ('bani', 4, 456, 4)
it= 2 ip=2 [356, 0, 0, 2, 72, 0] ('eqri', 4, 72, 4)
it= 3 ip=3 [356, 0, 0, 3, 1, 0] ('addr', 4, 3, 3)
it= 4 ip=5 [356, 0, 0, 5, 1, 0] ('seti', 0, 6, 4)
it= 5 ip=6 [356, 0, 0, 6, 0, 0] ('bori', 4, 65536, 5)
it= 6 ip=7 [356, 0, 0, 7, 0, 65536] ('seti', 1855046, 9, 4)
it= 7 ip=8 [356, 0, 0, 8, 1855046, 65536] ('bani', 5, 255, 2)
it= 8 ip=9 [356, 0, 0, 9, 1855046, 65536] ('addr', 4, 2, 4)
it= 9 ip=10 [356, 0, 0, 10, 1855046, 65536] ('bani', 4, 16777215, 4)
it= 10 ip=11 [356, 0, 0, 11, 1855046, 65536] ('muli', 4, 65899, 4)
it= 11 ip=12 [356, 0, 0, 12, 122245676354, 65536] ('bani', 4, 16777215, 4)
it= 12 ip=13 [356, 0, 0, 13, 6880578, 65536] ('gtir', 256, 5, 2)
it= 13 ip=14 [356, 0, False, 14, 6880578, 65536] ('addr', 2, 3, 3)
it= 14 ip=15 [356, 0, False, 15, 6880578, 65536] ('addi', 3, 1, 3)
it= 15 ip=17 [356, 0, False, 17, 6880578, 65536] ('seti', 0, 9, 2)

it= 16 ip=18 [356, 0, 0, 18, 6880578, 65536] ('addi', 2, 1, 1)
it= 17 ip=19 [356, 1, 0, 19, 6880578, 65536] ('muli', 1, 256, 1)
it= 18 ip=20 [356, 256, 0, 20, 6880578, 65536] ('gtrr', 1, 5, 1)
it= 19 ip=21 [356, 0, 0, 21, 6880578, 65536] ('addr', 1, 3, 3)
it= 20 ip=22 [356, 0, 0, 22, 6880578, 65536] ('addi', 3, 1, 3)
it= 21 ip=24 [356, 0, 0, 24, 6880578, 65536] ('addi', 2, 1, 2)
it= 22 ip=25 [356, 0, 1, 25, 6880578, 65536] ('seti', 17, 0, 3)

it= 23 ip=18 [356, 0, 1, 18, 6880578, 65536] ('addi', 2, 1, 1)
it= 24 ip=19 [356, 2, 1, 19, 6880578, 65536] ('muli', 1, 256, 1)
it= 25 ip=20 [356, 512, 1, 20, 6880578, 65536] ('gtrr', 1, 5, 1)
it= 26 ip=21 [356, 0, 1, 21, 6880578, 65536] ('addr', 1, 3, 3)
it= 27 ip=22 [356, 0, 1, 22, 6880578, 65536] ('addi', 3, 1, 3)
it= 28 ip=24 [356, 0, 1, 24, 6880578, 65536] ('addi', 2, 1, 2)
it= 29 ip=25 [356, 0, 2, 25, 6880578, 65536] ('seti', 17, 0, 3)
it= 30 ip=18 [356, 0, 2, 18, 6880578, 65536] ('addi', 2, 1, 1)
it= 31 ip=19 [356, 3, 2, 19, 6880578, 65536] ('muli', 1, 256, 1)
it= 32 ip=20 [356, 768, 2, 20, 6880578, 65536] ('gtrr', 1, 5, 1)
it= 33 ip=21 [356, 0, 2, 21, 6880578, 65536] ('addr', 1, 3, 3)
it= 34 ip=22 [356, 0, 2, 22, 6880578, 65536] ('addi', 3, 1, 3)
it= 35 ip=24 [356, 0, 2, 24, 6880578, 65536] ('addi', 2, 1, 2)
it= 36 ip=25 [356, 0, 3, 25, 6880578, 65536] ('seti', 17, 0, 3)
it= 37 ip=18 [356, 0, 3, 18, 6880578, 65536] ('addi', 2, 1, 1)
it= 38 ip=19 [356, 4, 3, 19, 6880578, 65536] ('muli', 1, 256, 1)
it= 39 ip=20 [356, 1024, 3, 20, 6880578, 65536] ('gtrr', 1, 5, 1)
it= 40 ip=21 [356, 0, 3, 21, 6880578, 65536] ('addr', 1, 3, 3)
it= 41 ip=22 [356, 0, 3, 22, 6880578, 65536] ('addi', 3, 1, 3)
it= 42 ip=24 [356, 0, 3, 24, 6880578, 65536] ('addi', 2, 1, 2)
....
it= 44 ip=18 [361, 0, 4, 18, 6880578, 65536] ('addi', 2, 1, 1)
it= 45 ip=19 [361, 5, 4, 19, 6880578, 65536] ('muli', 1, 256, 1)
it= 46 ip=20 [361, 1280, 4, 20, 6880578, 65536] ('gtrr', 1, 5, 1)
it= 47 ip=21 [361, 0, 4, 21, 6880578, 65536] ('addr', 1, 3, 3)
it= 48 ip=22 [361, 0, 4, 22, 6880578, 65536] ('addi', 3, 1, 3)
it= 49 ip=24 [361, 0, 4, 24, 6880578, 65536] ('addi', 2, 1, 2)
it= 50 ip=25 [361, 0, 5, 25, 6880578, 65536] ('seti', 17, 0, 3)
it= 51 ip=18 [361, 0, 5, 18, 6880578, 65536] ('addi', 2, 1, 1)
it= 52 ip=19 [361, 6, 5, 19, 6880578, 65536] ('muli', 1, 256, 1)
it= 53 ip=20 [361, 1536, 5, 20, 6880578, 65536] ('gtrr', 1, 5, 1)
it= 54 ip=21 [361, 0, 5, 21, 6880578, 65536] ('addr', 1, 3, 3)
it= 55 ip=22 [361, 0, 5, 22, 6880578, 65536] ('addi', 3, 1, 3)
it= 56 ip=24 [361, 0, 5, 24, 6880578, 65536] ('addi', 2, 1, 2)
it= 57 ip=25 [361, 0, 6, 25, 6880578, 65536] ('seti', 17, 0, 3)w

'''
import functools
import itertools
import time
import input_readers

def timeme(method):
    """Decorator that prints the elapsed time after a call"""
    @functools.wraps(method)
    def wrapper(*args, **kw):
        start_time = time.time()
        result = method(*args, **kw)
        end_time = time.time()
        print('timed', wrapper.__name__, end_time - start_time, 's')
        return result
    return wrapper


def main():
    'read input data, process it if necessary and call the part workers'
    #data = input_readers.read_input_str()
    # data = input_readers.read_input_int_list_multiline()
    # data = input_readers.read_input_str_list()
    # data = input_readers.read_input_table_tab()
    data = '''#ip 3
seti 123 0 4
bani 4 456 4
eqri 4 72 4
addr 4 3 3
seti 0 0 3
seti 0 6 4
bori 4 65536 5
seti 1855046 9 4
bani 5 255 2
addr 4 2 4
bani 4 16777215 4
muli 4 65899 4
bani 4 16777215 4
gtir 256 5 2
addr 2 3 3
addi 3 1 3
seti 27 0 3
seti 0 9 2
addi 2 1 1
muli 1 256 1
gtrr 1 5 1
addr 1 3 3
addi 3 1 3
seti 25 5 3
addi 2 1 2
seti 17 0 3
setr 2 7 5
seti 7 9 3
eqrr 4 0 2
addr 2 3 3
seti 5 3 3'''.splitlines()
    data = [row.split() for row in data]
    data = [(inst, *map(int, vals)) for (inst, *vals) in data]

    # assert part1('') ==
    print('part1:', part1(data))

    # assert part2('') ==
    print('part2:', part2(data))


def execute_until_pos_28(prog, reg0):
    'Do the part1 calculation'

    prog = list(prog)
    r = regs = [0] * 6  # registers
    r[0] = reg0

    ip_inst, instruction_reg = prog.pop(0) # what register is the instruction pointer
    assert ip_inst == '#ip'

    last_addr = len(prog)-2

    for iteration in itertools.count():
        inst = prog[regs[instruction_reg]]
        # if iteration % 1000000 < 100:
        #     print(f'it= {iteration} ip={regs[instruction_reg]} {regs} {inst} ') # , end='')
        if regs[instruction_reg] == 28:
            print(f'it= {iteration} ip={regs[instruction_reg]} {regs} {inst} ') # , end='')
            # what should r4 be for program to end
            return(r[4])


        opcode, a, b, c = inst

        if opcode == 'addr': r[c] = r[a] + r[b]
        elif opcode == 'addi': r[c] = r[a] + b
        elif opcode == 'mulr': r[c] = r[a] * r[b]
        elif opcode == 'muli': r[c] = r[a] * b
        elif opcode == 'banr': r[c] = r[a] & r[b]
        elif opcode == 'bani': r[c] = r[a] & b
        elif opcode == 'borr': r[c] = r[a] | r[b]
        elif opcode == 'bori': r[c] = r[a] | b
        elif opcode == 'setr': r[c] = r[a]
        elif opcode == 'seti': r[c] = a
        elif opcode == 'gtir': r[c] = a > r[b]
        elif opcode == 'gtri': r[c] = int(r[a] > b)
        elif opcode == 'gtrr': r[c] = int(r[a] > r[b])
        elif opcode == 'eqir': r[c] = int(a == r[b])
        elif opcode == 'eqri': r[c] = int(r[a] == b)
        elif opcode == 'eqrr': r[c] = int(r[a] == r[b])

        if regs[instruction_reg] < -1 or regs[instruction_reg] > last_addr:
            break  # program ended
        regs[instruction_reg] += 1
        # if iteration == 100000:
        #     break
    #print(f'ip={regs[instruction_reg]} {regs} {inst} ')
    return Null



@timeme
def part1(_data):
    'Do the part1 calculation'

    prog = list(_data)
    ip_inst, instruction_reg = prog.pop(0) # what register is the instruction pointer
    assert ip_inst == '#ip'
    print('program formatted for analysis')
    for (addr, inst) in enumerate(prog):
        print('{:>2} {} {:>9} {:>9} {:>2} : '.format(addr, *inst), end='')
        if inst[-1] == instruction_reg:
            print('Jump ', end='')
        else:
            print(f'r{inst[-1]}=', end='')
        print()
    print('end of program')

    res = execute_until_pos_28(_data, 0)
    print(res)
    return res

def execute_until_pos_28_or_hang(prog, reg0):
    'Do the part1 calculation'
    res = {}  # r0: iteration

    prog = list(prog)
    r = regs = [0] * 6  # registers
    r[0] = reg0

    ip_inst, instruction_reg = prog.pop(0) # what register is the instruction pointer
    assert ip_inst == '#ip'

    last_addr = len(prog)-2

    for iteration in itertools.count(1):
        inst = prog[regs[instruction_reg]]

        if regs[instruction_reg] == 28:
            # what should r0 be for program to end
            val = r[4]
            if val not in res:
                res[val] = iteration
                print(len(res), iteration, r[4])
            else:
                return

        opcode, a, b, c = inst

        if opcode == 'addr': r[c] = r[a] + r[b]
        elif opcode == 'addi': r[c] = r[a] + b
        elif opcode == 'mulr': r[c] = r[a] * r[b]
        elif opcode == 'muli': r[c] = r[a] * b
        elif opcode == 'banr': r[c] = r[a] & r[b]
        elif opcode == 'bani': r[c] = r[a] & b
        elif opcode == 'borr': r[c] = r[a] | r[b]
        elif opcode == 'bori': r[c] = r[a] | b
        elif opcode == 'setr': r[c] = r[a]
        elif opcode == 'seti': r[c] = a
        elif opcode == 'gtir': r[c] = a > r[b]
        elif opcode == 'gtri': r[c] = int(r[a] > b)
        elif opcode == 'gtrr': r[c] = int(r[a] > r[b])
        elif opcode == 'eqir': r[c] = int(a == r[b])
        elif opcode == 'eqri': r[c] = int(r[a] == b)
        elif opcode == 'eqrr': r[c] = int(r[a] == r[b])

        if regs[instruction_reg] < -1 or regs[instruction_reg] > last_addr:
            break  # program ended
        regs[instruction_reg] += 1
    #print(f'ip={regs[instruction_reg]} {regs} {inst} ')
    print('program exited, this is not expected to happen')
    return None

@timeme
def part2(_data):
    'Do the part2 calculation'
    prog = list(_data)
    ip_inst, instruction_reg = prog.pop(0) # what register is the instruction pointer
    assert ip_inst == '#ip'
    res = execute_until_pos_28_or_hang(_data, 0)
    return 'last value on line above'


if __name__ == '__main__':
    main()
