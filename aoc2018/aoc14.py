#!/bin/env python3

import functools
import itertools
import time
import input_readers

def timeme(method):
    """Decorator that prints the elapsed time after a call"""
    @functools.wraps(method)
    def wrapper(*args, **kw):
        start_time = time.time()
        result = method(*args, **kw)
        end_time = time.time()
        print('timed', wrapper.__name__, end_time - start_time, 's')
        return result
    return wrapper


def main():
    'read input data, process it if necessary and call the part workers'
    #data = input_readers.read_input_str()
    # data = input_readers.read_input_int_list_multiline()
    # data = input_readers.read_input_str_list()
    # data = input_readers.read_input_table_tab()

    assert part1(9) == '5158916779'
    assert part1(5) == '0124515891'
    assert part1(18) == '9251071085'
    assert part1(2018) == '5941429882'
    print('part1:', part1(894501))

    assert part2('51589') == 9
    assert part2('01245') == 5
    assert part2('92510') == 18
    assert part2('59414') == 2018
    print('part2:', part2('894501'))

@timeme
def part1(_data):
    'Do the part1 calculation'
    input = _data  # number of recipie
    dat = [3, 7]
    pos1 = 0
    pos2 = 1
    while len(dat) < input+10:
        #print(pos1, pos2, dat)
        sum_ = dat[pos1] + dat[pos2]
        new_recipies = list(map(int, str(sum_)))
        dat.extend(new_recipies)
        pos1 = (pos1 + 1 + dat[pos1]) % len(dat)
        pos2 = (pos2 + 1 + dat[pos2]) % len(dat)

    res = ''.join(map(str,dat[input:][:10]))
    print(res)
    return res


@timeme
def part2(_data):
    'Do the part2 calculation'
    input = _data  # str
    dat = [3, 7]
    pos1 = 0
    pos2 = 1
    start_next_search_on = 0
    for rep in itertools.count():
        if rep % 100000 == 0:
            #print(rep)
            res = ''.join(map(str,dat[start_next_search_on:]))
            #print('search', start_next_search_on, res)
            res = res.find(_data)
            if res != -1:
                res =  res + start_next_search_on
                print(res)
                return res
            start_next_search_on = max(len(dat)-len(input)-1, 0)

        #print(pos1, pos2, dat)
        sum_ = dat[pos1] + dat[pos2]
        new_recipies = list(map(int, str(sum_)))
        dat.extend(new_recipies)
        pos1 = (pos1 + 1 + dat[pos1]) % len(dat)
        pos2 = (pos2 + 1 + dat[pos2]) % len(dat)



if __name__ == '__main__':
    main()
