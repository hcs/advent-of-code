#!/bin/env python3

import collections
import functools
import time
import itertools

import numpy
import sortedcontainers

import input_readers

def timeme(method):
    """Decorator that prints the elapsed time after a call"""
    @functools.wraps(method)
    def wrapper(*args, **kw):
        start_time = time.time()
        result = method(*args, **kw)
        end_time = time.time()
        print('timed', wrapper.__name__, end_time - start_time, 's')
        return result
    return wrapper


def main():
    'read input data, process it if necessary and call the part workers'
    data = input_readers.read_input_str_no_strip()
    data = data.splitlines()
    # data = input_readers.read_input_int_list_multiline()
    #data = input_readers.read_input_str_list()
    # data = input_readers.read_input_table_tab()

    tdata = (r'/->-\        ',
             '|   |  /----\\',
             r'| /-+--+-\  |',
             r'| | |  | v  |',
             r'\-+-/  \-+--/',
             r'  \------/  ')

    assert part1(tdata) == (7, 3)
    print('part1:', part1(data))

    tdata= r'''/>-<\  
|   |  
| /<+-\
| | | v
\>+</ |
  |   ^
  \<->/'''.splitlines()

    assert part2(tdata) == (6, 4)
    print('part2:', part2(data))


Cart = collections.namedtuple('Cart', ['y', 'x', 'direction', 'turns'])

def print_map(map_, carts = {}):
    for y, row in enumerate(map_):
        for x, state in enumerate(row):
            cart = carts.get((y, x))
            if cart:
                state = cart.direction
            print(state or ' ', end='')

        print()


def find_carts(map_with_carts):
    carts = sortedcontainers.SortedDict()
    rows = len(map_with_carts)
    cols = max([len(row) for row in map_with_carts])
    map_ = numpy.full((rows, cols), ' ')
    for y, row in enumerate(map_with_carts):
        for x, state in enumerate(row):
            #print(x, y, state)
            if state in '><v^':
                carts[(y, x)] = Cart(y, x, state, 0)
                map_[y, x] = '-' if state in '><' else '|'
            else:
                map_[y, x] = state
    #print_map(map_)


    return carts, map_

def move_cart(map_, cart, carts):
    choices = ('left', 'straight', 'right')  # choices[turns%3] -> what to do
    x = cart.x
    y = cart.y
    turns = cart.turns
    if cart.direction == '>':
        x += 1
    elif cart.direction == '<':
        x -= 1
    elif cart.direction == '^':
        y -= 1
    elif cart.direction == 'v':
        y += 1
    else:
        raise Exception('Unknown direction: ' + cart.direction)
    new_map_sym = map_[y, x]
    #print(x,y,new_map_sym)
    if new_map_sym != '+':
        new_dir = {
            '>/': '^',
            '>\\': 'v',
            '>-': '>',
            '</': 'v',
            '<\\': '^',
            '<-': '<',
            'v/': '<',
            'v\\': '>',
            'v|': 'v',
            '^/': '>',
            '^\\': '<',
            '^|': '^',
        }[f'{cart.direction}{new_map_sym}']
    else:
        new_dir = {
            ('>', 'right'): 'v',
            ('<', 'right'): '^',
            ('v', 'right'): '<',
            ('^', 'right'): '>',
            ('>', 'left'): '^',
            ('<', 'left'): 'v',
            ('v', 'left'): '>',
            ('^', 'left'): '<',
            ('>', 'straight'): '>',
            ('<', 'straight'): '<',
            ('v', 'straight'): 'v',
            ('^', 'straight'): '^',
        }[(cart.direction, choices[turns%3])]
        turns += 1

    return Cart(y, x, new_dir, turns)

@timeme
def part1(_data):
    'Do the part1 calculation'
    carts, map_ = find_carts(_data)
    for tick in itertools.count():
        for cart in list(carts.values()):
            #print(cart)
            carts.pop((cart.y, cart.x))  # remove old state
            cart2 = move_cart(map_, cart, carts)
            if (cart2.y, cart2.x) in carts.keys():
                # collision
                print('collision at', cart2.x, cart2.y)
                return cart2.x, cart2.y
            carts[(cart2.y, cart2.x)] = cart2


@timeme
def part2(_data):
    'Do the part2 calculation'
    print('PART2')
    carts, map_ = find_carts(_data)
    for tick in itertools.count():
        #print('tick', tick)
        #print_map(map_, carts)
        #print(carts)
        for cart in list(carts.values()):
            if cart not in carts.values():
                continue  # has been crashed into
            #print_map(map_, carts)
            carts.pop((cart.y, cart.x))  # remove old state
            cart2 = move_cart(map_, cart, carts)
            yx = (cart2.y, cart2.x)
            if yx in carts.keys():
                # collision
                #print(tick, 'collision at yx', yx, yx in carts.keys())
                carts.pop(yx)
            else:
                carts[(cart2.y, cart2.x)] = cart2
        if len(carts) == 1:
            res = carts.values()[0]
            #print(res)
            return res.x, res.y

if __name__ == '__main__':
    main()
