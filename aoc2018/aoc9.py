#!/bin/env python3

import itertools
import input_readers

def main():
    'read input data, process it if necessary and call the part workers'
    #data = input_readers.read_input_str()
    # data = input_readers.read_input_int_list_multiline()
    # data = input_readers.read_input_str_list()
    # data = input_readers.read_input_table_tab()
    #411 players; last marble is worth 72059 points
    data = (411, 72059)

    assert part1((9, 25)) == 32
    assert part1((10, 1618)) == 8317
    assert part1((13, 7999)) == 146373
    assert part1((17, 1104)) == 2764
    assert part1((21, 6111)) == 54718
    assert part1((30, 5807)) == 37305
    print('part1:', part1(data))

    # assert part2('') ==
    print('part2:', part1((411, 72059*100)))

class Node():
    '''circular linked list'''
    def __init__(self, value):
        self.value = value
        self.next = self
        self.prev = self

    def get_relative(self, positions):
        '''Get node positions after this one'''
        res = self
        if positions >= 0:
            for _idx in range(positions):
                res = res.next
        else:
            for _idx in range(positions, 0):
                res = res.prev
        return res

    def remove(self):
        '''remove current node, returns the node after'''
        self.prev.next = self.next
        self.next.prev = self.prev
        return self.next

    def insert_after(self, value):
        new = Node(value)
        new.prev = self
        new.next = self.next
        self.next.prev = new
        self.next = new

    def values(self):
        '''return list with all values in circle, current first'''
        current = self
        while True:
            yield current.value
            current = current.next
            if current == self:
                break


def part1(_data):
    'Do the part1 calculation'
    players, last_marble = _data
    print('START', players, last_marble)
    remaining_marbles = list(range(last_marble, -1, -1))  # reverse order for quick pop
    player_score = {player: 0 for player in range(1, players+1)}
    start = current = Node(remaining_marbles.pop())

    for player in itertools.cycle(range(1, players+1)):
        next_marble = remaining_marbles.pop()
        if next_marble % 23 == 0:
            player_score[player] += next_marble
            current = current.get_relative(-7)
            player_score[player] += current.value
            current = current.remove()
        else:
            current = current.get_relative(1)
            current.insert_after(next_marble)
            current = current.get_relative(1)

        #print(player, list(start.values()))

        if not remaining_marbles:
            break

        if len(remaining_marbles) % (last_marble//10) == 0:
            print(round(100 * len(remaining_marbles)/last_marble), '%')

    return max(player_score.values())


def part1_listbased(_data):
    'Do the part1 calculation'
    players, last_marble = _data
    print('xxx', players, last_marble)
    remaining_marbles = list(range(last_marble, -1, -1))  # reverse order for quick pop
    player_score = {player: 0 for player in range(1, players+1)}
    circle = [remaining_marbles.pop()]
    current_pos = 0

    def change_pos(change):
        new = current_pos + change
        if new >= len(circle):
            new = new % len(circle)
        elif new < 0:
            new = len(circle) + new
        return new

    for player in itertools.cycle(range(1, players+1)):
        next_marble = remaining_marbles.pop()
        if next_marble % 23 == 0:
            player_score[player] += next_marble
            current_pos = change_pos(-7)
            player_score[player] += circle.pop(current_pos)
            current_pos = change_pos(0)
        else:
            current_pos = change_pos(1)
            circle.insert(current_pos + 1, next_marble)
            current_pos = change_pos(1)

        #print(player, current_pos, next_marble, circle)

        if not remaining_marbles:
            return max(player_score.values())

        if len(remaining_marbles) % (last_marble//10) == 0:
            print(round(100 * len(remaining_marbles)/last_marble), '%')



if __name__ == '__main__':
    main()
