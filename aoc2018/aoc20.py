#!/bin/env python3.7

import functools
import itertools
import time
import input_readers

def timeme(method):
    """Decorator that prints the elapsed time after a call"""
    @functools.wraps(method)
    def wrapper(*args, **kw):
        start_time = time.time()
        result = method(*args, **kw)
        end_time = time.time()
        print('timed', wrapper.__name__, end_time - start_time, 's')
        return result
    return wrapper


def main():
    'read input data, process it if necessary and call the part workers'
    data = input_readers.read_input_str()
    # data = input_readers.read_input_int_list_multiline()
    # data = input_readers.read_input_str_list()
    # data = input_readers.read_input_table_tab()

    assert part1('^WNE$') == 3
    assert part1('^ENWWW(NEEE|SSE(EE|N))$') == 10
    assert part1('^ENNWSWW(NEWS|)SSSEEN(WNSE|)EE(SWEN|)NNN$') == 18
    assert part1('^ESSWWN(E|NNENN(EESS(WNSE|)SSS|WWWSSSSE(SW|NNNE)))$') == 23
    assert part1('^WSSEESWWWNW(S|NENNEEEENN(ESSSSW(NWSW|SSEN)|WSWWN(E|WWS(E|SS))))$') == 31
    print('part1:', part1(data))

    # assert part2('') ==
    print('part2:', part2(data))


def print_map(map_):
    x_vals = [pos.real for pos in map_.keys()]
    y_vals = [pos.imag for pos in map_.keys()]
    minx = int(min(x_vals))-1
    maxx = int(max(x_vals))+1
    miny = int(min(y_vals))-1
    maxy = int(max(y_vals))+1

    for row in range(minx, maxx+1):
        for col in range(miny, maxy+1):
            print(map_.get(complex(col, row), '#'), end='')
        print()


# def move(pos, motion):
#     '''1d matrix addition'''
#     return tuple([v1+v2 for v1, v2 in zip(pos, motion)])


def find_furthest(map_):
    map_ = dict(map_)
    last_positions = [0j]
    map_[0j] = 0
    for steps in itertools.count(1):
        new_positions = set()
        for pos in last_positions:
            for motion in (-1, -1j, 1j, 1):
                pos1 = pos + motion  # door / wall
                pos2 = pos1 + motion  # other side of door/wall
                if (map_.get(pos1, '#') in ('-', '|') and
                        map_.get(pos2, '#') == '.'):
                    map_[pos2] = steps
                    new_positions.add(pos2)
            last_positions = new_positions
        if not new_positions:
            break

    #print_map(map_)
    print(steps-1)
    return steps-1, map_

def split_branches(path):
    branches = []
    common_suffix = None
    nestlevel = 0
    branch_start = 0
    for i, c in enumerate(path):
        if c == '(':
            nestlevel += 1
        elif c == ')':
            nestlevel -= 1
            if nestlevel < 0: # fouth the end of the branching () we are splitting
                branches.append(path[branch_start:i])
                common_suffix = path[i+1:]
                break
        elif nestlevel == 0 and c == '|':
            branches.append(path[branch_start:i])
            branch_start = i+1
    branches = [branch + common_suffix for branch in branches]
    #print(path, 'split into', [''.join(branch) for branch in branches])
    return branches


def create_map(path):
    map_ = {}  # (y,x): c
    history = set()
    stack = set()
    stack.add((0+0j, path))
    while stack:
        #print(len(stack))
        pos, path = stack.pop()
        history.add((pos, path))
        #print('walk', pos, path)
        path_pos = 0
        while True:
            #print_map(map_)
            c = path[path_pos]
            path_pos += 1
            #print(c, path[path_pos:])
            if c == '^':
                map_[pos] = 'X'
            elif c in '$':
                break
            elif c == '(':
                for path in split_branches(path[path_pos:]):
                    if (pos, path) not in history:
                        stack.add((pos, path))
                break
            elif c == ')':
                raise Exception('EEK )')
            elif c == '|':
                raise Exception('EEK |')
            elif c == 'N':
                pos += -1j
                map_[pos] = '-'
                pos += -1j
                map_[pos] = '.'
            elif c == 'S':
                pos += 1j
                map_[pos] = '-'
                pos += 1j
                map_[pos] = '.'
            elif c == 'W':
                pos += -1
                map_[pos] = '|'
                pos += -1
                map_[pos] = '.'
            elif c == 'E':
                pos += 1
                map_[pos] = '|'
                pos += 1
                map_[pos] = '.'
            else:
                raise Exception('EEK ' + c)

            #re_pos+=1
    #breakpoint()
    return map_

@timeme
def part1(_data):
    'Do the part1 calculation'
    map_ = create_map(_data)
    #print_map(map_)
    return find_furthest(map_)[0]


@timeme
def part2(_data):
    'Do the part2 calculation'
    map_ = create_map(_data)
    _, dist_map = find_furthest(map_)
    #print_map(dist_map)
    counter = 0
    for val in dist_map.values():
        if isinstance(val, int) and val >= 1000:
            counter += 1
    return counter


if __name__ == '__main__':
    main()
