#!/bin/env python3

import functools
import time
import input_readers
import numpy
import itertools

def timeme(method):
    """Decorator that prints the elapsed time after a call"""
    @functools.wraps(method)
    def wrapper(*args, **kw):
        start_time = time.time()
        result = method(*args, **kw)
        end_time = time.time()
        print('timed', wrapper.__name__, end_time - start_time, 's')
        return result
    return wrapper


def main():
    'read input data, process it if necessary and call the part workers'
    #data = input_readers.read_input_str()
    # data = input_readers.read_input_int_list_multiline()
    #data = input_readers.read_input_str_list()
    # data = input_readers.read_input_table_tab()
    data = input_readers.read_input_str_no_strip()
    data = data.splitlines()
    data = list(map(tuple, data))
    data = numpy.array(data)

    tdata= '''
.#.#...|#.
.....#|##|
.|..|...#.
..|#.....#
#.#|||#|#|
...#.||...
.|....|...
||...#|.#|
|.||||..|.
...#.|..|.'''.strip()
    tdata = tdata.splitlines()
    tdata = list(map(tuple, tdata))
    tdata = numpy.array(tdata)

    assert part1(tdata) == 1147
    print('part1:', part1(data))

    # assert part2('') ==
    print('part2:', part2(data))

def print_map(map_):
    '''only usable for testdata'''
    #print(map_.shape)
    print()
    for y, row in enumerate(map_):
        for x, state in enumerate(row):
            # if x >= 494:
            print(state, end='')
        print()

def analyse(map_, pos):
    #print(pos, map_[pos] == '.')
    ysize, xsize = map_.shape
    adjacent = []
    for apos in itertools.product(range(pos[0]-1, pos[0]+2), range(pos[1]-1, pos[1]+2)):
        if apos == pos:
            continue
        if (apos[0] == -1 or apos[1] == -1 or
            apos[0] >= ysize or apos[1] >= xsize):
            continue
        #print('a', apos)
        adjacent.append(map_[apos])
    #print(adjacent)

    if map_[pos] == '.':
        res = '|' if adjacent.count('|') > 2 else '.'
    elif map_[pos] == '|':
        res = '#' if adjacent.count('#') > 2 else '|'
    elif map_[pos] == '#':
        res = '#' if adjacent.count('#') > 0 and adjacent.count('|') > 0 else '.'
    #print(res)
    return res

@timeme
def part1(_data):
    'Do the part1 calculation'
    map_ = _data.copy()
    #print_map(map_)

    for min_ in range(1, 11):
        nmap = _data.copy()
        for pos in itertools.product(range(map_.shape[0]), range(map_.shape[1])):
            nmap[pos] = analyse(map_, pos)
        map_ = nmap
        #print(min_)
        #print_map(map_)

    wooded = 0
    lumberyards = 0
    for val in map_.flat:
        if val == '|':
            wooded += 1
        elif val == '#':
            lumberyards += 1
    return wooded * lumberyards




@timeme
def part2(_data):
    'Do the part2 calculation'
    map_ = _data.copy()
    #print_map(map_)

    def value():
        wooded = 0
        lumberyards = 0
        for val in map_.flat:
            if val == '|':
                wooded += 1
            elif val == '#':
                lumberyards += 1
        return wooded * lumberyards

    lval = 0
    for min_ in range(1, 600):
        nmap = _data.copy()
        for pos in itertools.product(range(map_.shape[0]), range(map_.shape[1])):
            nmap[pos] = analyse(map_, pos)
        map_ = nmap
        #print_map(map_)
        val = value()
        print(min_, val, val-lval)
        lval = val


if __name__ == '__main__':
    main()

print(''''
# Size repeats on base 28 from under 600 min
tmin = 1000000000
reps = (tmin-600)//28
pos with same value as target = tmin-28*reps = 602 -> 212176
''')
