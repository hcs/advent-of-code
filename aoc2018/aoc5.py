#!/bin/env python3

import string
import input_readers

def main():
    'read input data, process it if necessary and call the part workers'
    data = input_readers.read_input_str()
    # data = input_readers.read_input_int_list_multiline()
    # data = input_readers.read_input_str_list()
    # data = input_readers.read_input_table_tab()

    assert part1('dabAcCaCBAcCcaDA') == 10
    print('part1:', part1(data))

    assert part2('dabAcCaCBAcCcaDA') == 4
    print('part2:', part2(data))

def part1(_data):
    'Do the part1 calculation'
    data = list(_data)
    pos = 0
    table = str.maketrans(string.ascii_lowercase + string.ascii_uppercase,
                          string.ascii_uppercase + string.ascii_lowercase)
    while pos < len(data)-2:
        first = data[pos]
        second = data[pos+1]
        if first == second.translate(table):
            data.pop(pos)
            data.pop(pos)
            if pos > 0:
                pos += -1
        else:
            pos += 1
    return len(data)



def part2(_data):
    'Do the part2 calculation'
    res = []  # list of (length, removed letter)
    for type_ in string.ascii_lowercase:
        remove = (type_, type_.upper())
        data = [val for val in _data if val not in remove]
        pos = 0
        table = str.maketrans(string.ascii_lowercase + string.ascii_uppercase,
                              string.ascii_uppercase + string.ascii_lowercase)
        while pos < len(data)-2:
            first = data[pos]
            second = data[pos+1]
            if first == second.translate(table):
                data.pop(pos)
                data.pop(pos)
                if pos > 0:
                    pos += -1
            else:
                pos += 1
            res.append((len(data), type_))


    return sorted(res)[0][0]


if __name__ == '__main__':
    main()
