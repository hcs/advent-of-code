#!/bin/env python3

import itertools
import input_readers

def main():
    'read input data, process it if necessary and call the part workers'
    #data = input_readers.read_input_str()
    # data = input_readers.read_input_int_list_multiline()
    # data = input_readers.read_input_str_list()
    # data = input_readers.read_input_table_tab()

    data = 9221

    assert part1(18) == (33, 45)
    assert part1(42) == (21, 61)
    print('part1:', part1(data))

    # assert part2('') ==
    print('part2:', part2(data))

def power_level(x, y, serial):
    rack_id = x + 10
    power = (rack_id * y + serial) * rack_id
    power = 0 if power < 100 else int(str(power)[-3])
    power -= 5
    return power

def sq_power(x, y, size, serial):
    res = 0
    for y2 in range(y, y+size):
        for x2 in range(x, x+size):
            res += power_level(x2, y2, serial)
    return res

def part1(_data):
    'Do the part1 calculation'
    serial = _data
    assert power_level(3, 5, 8) == 4
    assert power_level(122, 79, 57) == -5
    assert power_level(217, 196, 39) == 0
    assert power_level(101, 153, 71) == 4

    res = []
    size = 3
    for y in range(1,302-size):
        for x in range(1,302-size):
            res.append((sq_power(x, y, size, serial), (x, y)))

    res.sort()
    largest = res.pop()
    print(serial, largest)
    return largest[1]


def part2(_data):
    'Do the part2 calculation'
    # NOTE this takes a couple of hours to run
    serial = _data
    grid = {}
    for y in range(1, 301):
        for x in range(1, 301):
            grid[(x, y)] = power_level(x, y, serial)

    def sq_power(x, y, size):
        return sum(map(grid.get, itertools.product(range(x, x+size), range(y, y+size))))

    res = []
    largest = (0, (0, 0, 0))
    for size in range(1, 301):
        print(size)
        for (x, y) in itertools.product(range(1, 302-size), range(1, 302-size)):
            largest = max(largest, (sq_power(x, y, size), (x, y, size)))

    print(serial, largest)
    return largest[1]

if __name__ == '__main__':
    main()
