#!/bin/env python3

import re
import functools
import time
import itertools
import sys

import numpy

import input_readers

sys.setrecursionlimit(10000)

def timeme(method):
    """Decorator that prints the elapsed time after a call"""
    @functools.wraps(method)
    def wrapper(*args, **kw):
        start_time = time.time()
        result = method(*args, **kw)
        end_time = time.time()
        print('timed', wrapper.__name__, end_time - start_time, 's')
        return result
    return wrapper

def parse(lines):
    '''
    y=55, x=464..466
    x=525, y=673..675'''
    clay = [] # (y, x)
    for line in lines:
        x_range = re.match(r'y=(.*), x=(.*)\.\.(.*)$', line)
        y_range = re.match(r'x=(.*), y=(.*)\.\.(.*)$', line)
        if x_range:
            y, x1, x2 = map(int, x_range.groups())
            for x in range(x1, x2+1):
                clay.append((y, x))
        elif y_range:
            x, y1, y2 = map(int, y_range.groups())
            for y in range(y1, y2+1):
                clay.append((y, x))
        else:
            raise Exception('Failed to parse line: ' + line)

    # convert map_dict to array:
    min_y = sorted(clay)[0][0]
    max_y = sorted(clay)[-1][0]
    min_x = sorted(clay, key=lambda yx: yx[1])[0][1]
    max_x = sorted(clay, key=lambda yx: yx[1])[-1][1]
    x_move = min_x - 1  # keep left column free
    y_move = min_y
    print(min_y, max_y, min_x, max_x)
    map_ = numpy.full((max_y-y_move+1, max_x-x_move+2), '.')
    for pos in clay:
        map_[pos[0]-y_move, pos[1]-x_move] = '#'

    #map_[-1, 500-x_move] = '+'  # spring
    return 500-x_move, map_


def print_map(map_, spring_col=None):
    '''only usable for testdata'''
    #print(map_.shape)
    if spring_col:
        print(' ' * spring_col + '+')
    else:
        print()
    for y, row in enumerate(map_):
        for x, state in enumerate(row):
            # if x >= 494:
            print(state, end='')
        print()


def main():
    'read input data, process it if necessary and call the part workers'
    #data = input_readers.read_input_str()
    # data = input_readers.read_input_int_list_multiline()
    data = input_readers.read_input_str_list()
    # data = input_readers.read_input_table_tab()

    tdata = parse('''\
x=495, y=2..7
y=7, x=495..501
x=501, y=3..7
x=498, y=2..4
x=506, y=1..2
x=498, y=10..13
x=504, y=10..13
y=13, x=498..504'''.splitlines())


    data = parse(data)
    assert part1(tdata) == 57
    print('part1:', part1(data))

    # assert part2('') ==
    print('part2:', part2(data))


def fill(map_, pos, dir_='down'):
    '''return True if there is a leak that way'''
    y, x = pos
    if map_[y, x] == '#':
        return False  # No leak inside walls
    if map_[y, x] == '|':
        return True  # Already desided to be a leak
    if map_[y, x] == '.':
        map_[y, x] = '?'
    #print_map(map_)

    # Try to flow down
    #print(y, map_.shape)
    if y+1 >= map_.shape[0]:
        map_[y, x] = '|'
        return True  # leaking downward
    if map_[y+1, x] in '.|':
        dleak = fill(map_, (y+1, x), dir_='down') # flow down
        if dleak:
            map_[y, x] = '|'
            return True  # leaking downward

    # we cant flow down so..

    if x-1 < 0:
        map_[y, x] = '|'
    if x >= map_.shape[1]:
        map_[y, x] = '|'
        return True  # leaking to the side

    # Try to flow left
    lleak = False
    if dir_ != 'right' and map_[y, x-1] in ['|', '.']:
        lleak = fill(map_, (y, x-1), dir_='left') # flow left

    # Try to flow right
    rleak = False
    if dir_ != 'left' and map_[y, x+1] in ['|', '.']:
        rleak = fill(map_, (y, x+1), dir_='right') # flow right

    if dir_ == 'down':
        if lleak or rleak:
            # fill with |
            map_[y, x] = '|'
            for step in itertools.count(1):
                if map_[y, x-step] == '?':
                    map_[y, x-step] = '|'
                else:
                    break
            for step in itertools.count(1):
                if map_[y, x+step] == '?':
                    map_[y, x+step] = '|'
                else:
                    break
            return True  # inherited leak
        else:
            # fill with ~
            map_[y, x] = '~'
            for step in itertools.count(1):
                if map_[y, x-step] == '?':
                    map_[y, x-step] = '~'
                else:
                    break
            for step in itertools.count(1):
                if map_[y, x+step] == '?':
                    map_[y, x+step] = '~'
                else:
                    break
            return False
    else:
        return lleak or rleak


@timeme
def part1(_data):
    'Do the part1 calculation'
    spring_col, map_ = _data
    map_ = map_.copy()
    #print_map(map_, spring_col)
    try:
        fill(map_, (0, spring_col))
    finally:
        print_map(map_, spring_col)

    count = 0
    for val in map_.flat:
        if val in ('~', '|'):
            count += 1
    return count


@timeme
def part2(_data):
    'Do the part2 calculation'
    spring_col, map_ = _data
    map_ = map_.copy()
    #print_map(map_, spring_col)
    try:
        fill(map_, (0, spring_col))
    finally:
        print_map(map_, spring_col)

    count = 0
    for val in map_.flat:
        if val in ('~',):
            count += 1
    return count


if __name__ == '__main__':
    main()
