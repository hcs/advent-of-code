#!/bin/env python3

from collections import Counter
import input_readers

def main():
    'read input data, process it if necessary and call the part workers'
    #data = input_readers.read_input_str()
    # data = input_readers.read_input_int_list_multiline()
    data = input_readers.read_input_str_list()
    # data = input_readers.read_input_table_tab()

    # assert part1('') ==
    assert part1(('abcdef', 'bababc', 'abbcde', 'abcccd', 'aabcdd', 'abcdee', 'ababab',)) == 12
    print('part1:', part1(data))

    # assert part2('') ==
    assert part2(('abcde', 'fghij', 'klmno', 'pqrst', 'fguij', 'axcye', 'wvxyz', )) == 'fgij'
    print('part2:', part2(data))

def part1(_data):
    'Do the part1 calculation'
    twos = 0
    threes = 0

    def calc(id_):
        counts = Counter(id_)
        # print(counts)
        # print(2 in counts.values(), 3 in counts.values())
        return 2 in counts.values(), 3 in counts.values()

    for id_ in _data:
        two, thr = calc(id_)
        twos += two
        threes += thr


    return twos * threes


def part2(_data):
    'Do the part2 calculation'
    def diff(str1, str2):
        diff_count = 0
        for pos, letter in enumerate(str1):
            if letter != str2[pos]:
                diff_count += 1
                if diff_count == 2:
                    break  # no reason to continue
        return diff_count
    def search(id_, ids):
        for id2 in ids:
            if diff(id_, id2) == 1:
                # return chars common to both
                return ''.join([a for a, b in zip(id_, id2) if a == b])
        return None
    for pos, id_ in enumerate(_data):
        res = search(id_, _data[pos+1:])
        if res:
            return res




if __name__ == '__main__':
    main()
