#!/bin/env python3
import re
import input_readers

def main():
    'read input data, process it if necessary and call the part workers'
    #data = input_readers.read_input_str()
    #data = input_readers.read_input_int_list_multiline()
    data = input_readers.read_input_str_list()
    # data = input_readers.read_input_table_tab()


    # [1518-10-19 00:39] wakes up
    # [1518-10-14 00:05] falls asleep
    # [1518-05-05 00:23] falls asleep
    # [1518-09-12 23:59] Guard #3541 begins shift
    data = sorted(data)

    def parse(line):
        res = re.match(r'\[.* ..:(.*)\] (.*)$', line)
        min_, msg = res.groups()
        return int(min_), msg
    data = tuple(map(parse, data))  # (no, left, top, with, height


    # assert part1('') ==
    print('part1:', part1(data))

    # assert part2('') ==
    print('part2:', part2(data))

def part1(data):
    'Do the part1 calculation'
    guards = {}
    guard = None
    since = 0  # min
    mins = None  # we are using pos 60 for the guards total
    for min_, msg in data:
        if msg.startswith('Guard'): # begins shift
            res = re.match(r'.*#([0-9]*) .*', msg)
            guard = res.group(1)
            mins = guards.setdefault(guard, [0]*61)
            since = 0
        elif msg == 'falls asleep':
            since = min_
        elif msg == 'wakes up':
            for min2 in range(since, min_):
                mins[min2] += 1
                mins[60] += 1
        else:
            print('ERROR', msg)
            exit(1)

    # find sleepy
    sleepy = sorted([ lis + [guard] for (guard, lis) in guards.items()], key=lambda x: x[60])[-1][61]
    mins = guards[sleepy][:-1] # remove total
    when = mins.index(max(mins))
    return int(sleepy) * when


def part2(data):
    'Do the part2 calculation'
    guards = {}
    guard = None
    since = 0  # min
    mins = None
    for min_, msg in data:
        if msg.startswith('Guard'): # begins shift
            res = re.match(r'.*#([0-9]*) .*', msg)
            guard = res.group(1)
            mins = guards.setdefault(guard, [0]*60)
            since = 0
        elif msg == 'falls asleep':
            since = min_
        elif msg == 'wakes up':
            for min2 in range(since, min_):
                mins[min2] += 1
        else:
            print('ERROR', msg)
            exit(1)

    # find sleepy

    sorted_guards = sorted([(sorted(lis, reverse=True), guard) for (guard, lis) in guards.items()], reverse=True)
    sleepy = sorted_guards[0][1]
    mins = guards[sleepy]
    when = mins.index(max(mins))
    return int(sleepy) * when


if __name__ == '__main__':
    main()
