#!/bin/env python3

import re
import input_readers

def main():
    'read input data, process it if necessary and call the part workers'
    # data = input_readers.read_input_str()
    # data = input_readers.read_input_int_list_multiline()
    data = input_readers.read_input_str_list()
    # data = input_readers.read_input_table_tab()

    #1233 @ 923,156: 16x21
    def parse(line):
        res = re.match(r'#(.*) @ (.*),(.*): (.*)x(.*)$', line)
        return tuple(map(int, res.groups()))

    data = tuple(map(parse, data))  # (no, left, top, with, height
    #print(data)
    # assert part1('') ==
    print('part1:', part1(data))

    # assert part2('') ==
    print('part2:', part2(data))

def part1(_data):
    'Do the part1 calculation'
    map_ = {}  # (x,y): count
    for (no, left, top, with_, height) in _data:
        #print((no, left, top, with_, height))
        for x in range(left, left+with_):
            for y in range(top, top+height):
                map_[(x, y)] = map_.get((x, y), 0) + 1
        # print(map_)
        # exit()

    return len([1 for count in map_.values() if count > 1])


def part2(_data):
    'Do the part2 calculation'
    map_ = {}  # (x,y): count
    for (no, left, top, with_, height) in _data:
        #print((no, left, top, with_, height))
        for x in range(left, left+with_):
            for y in range(top, top+height):
                map_[(x, y)] = map_.get((x, y), 0) + 1

    for (no, left, top, with_, height) in _data:
        #print((no, left, top, with_, height))
        whole = True
        for x in range(left, left+with_):
            for y in range(top, top+height):
                if map_[(x, y)] > 1:
                    whole = False
        if whole:
            return no




if __name__ == '__main__':
    main()
