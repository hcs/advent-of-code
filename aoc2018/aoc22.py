#!/bin/env python3

import functools
import time
import input_readers
import networkx

def timeme(method):
    """Decorator that prints the elapsed time after a call"""
    @functools.wraps(method)
    def wrapper(*args, **kw):
        start_time = time.time()
        result = method(*args, **kw)
        end_time = time.time()
        print('timed', wrapper.__name__, end_time - start_time, 's')
        return result
    return wrapper


def main():
    'read input data, process it if necessary and call the part workers'
    #data = input_readers.read_input_str()
    # data = input_readers.read_input_int_list_multiline()
    # data = input_readers.read_input_str_list()
    # data = input_readers.read_input_table_tab()

    data = 11541, 14+778j
    tdata = 510, 10+10j

    assert part1(*tdata) == 114
    print('part1:', part1(*data))

    # assert part2('') ==
    assert part2(*tdata) == 45
    print('part2:', part2(*data))


@timeme
def part1(depth, target):
    'Do the part1 calculation'

    def geo_index(pos):
        if pos in (0j, target):
            return 0
        if pos.imag == 0:
            return int(pos.real) * 16807
        if pos.real == 0:
            return int(pos.imag) * 48271
        return ero_level(pos-1) * ero_level(pos-1j)

    @functools.lru_cache(maxsize=None)
    def ero_level(pos):
        return (geo_index(pos) + depth) % 20183

    def risk(pos):
        return ero_level(pos) % 3

    def type_(pos):
        return ('.', '=', '|')[ero_level(pos) % 3]

    if depth == 510:
        assert ero_level(0+0j) == 510
        assert ero_level(1+0j) == 17317
        assert ero_level(0+1j) == 8415
        assert ero_level(1+1j) == 1805
        assert ero_level(10+10j) == 510

        assert geo_index(0+0j) == 0
        assert geo_index(1+0j) == 16807
        assert geo_index(0+1j) == 48271
        assert geo_index(1+1j) == 145722555
        assert geo_index(10+10j) == 0

        assert type_(0+0j) == '.'
        assert type_(1+0j) == '=', type_(1+0j)
        assert type_(0+1j) == '.'
        assert type_(1+1j) == '|'
        assert type_(10+10j) == '.'

    sum_ = 0
    for x in range(0, int(target.real)+1):
        for y in range(0, int(target.imag)+1):
            sum_ += risk(x+1j*y)
    print(sum_)
    return sum_



@timeme
def part2(depth, target):
    'Do the part2 calculation'
    def geo_index(pos):
        if pos in (0j, target):
            return 0
        if pos.imag == 0:
            return int(pos.real) * 16807
        if pos.real == 0:
            return int(pos.imag) * 48271
        return ero_level(pos-1) * ero_level(pos-1j)

    @functools.lru_cache(maxsize=None)
    def ero_level(pos):
        return (geo_index(pos) + depth) % 20183

    def type_(pos):
        return ('.', '=', '|')[ero_level(pos) % 3]


    valid_eq = {
        '.': set(('c', 't')),
        '=': set(('c', 'n')),
        '|': set(('t', 'n')),
    }
    def eq_to_use(terrain_here, terrain_there, equip_here):
        if terrain_here == terrain_there:
            return equip_here, 1
        new_equip = (valid_eq[terrain_here] & valid_eq[terrain_there]).pop()
        return new_equip, 8

    # build graph
    graph = networkx.Graph()
    for x in range(0, int(target.real)+1+100):
        for y in range(0, int(target.imag)+1+100):
            here = x+1j*y
            terrain_here = type_(here)
            valid_here = {'t'} if here in (0, target) else valid_eq[terrain_here]
            for motion in (-1, 1, -1j, 1j):
                there = here + motion
                if int(there.real) < 0 or int(there.imag) < 0:
                    continue
                terrain_there = type_(there)
                valid_there = {'t'} if there in (0, target) else valid_eq[terrain_there]
                for equip_here in valid_here:
                    for equip_there in valid_there & valid_here:
                        if equip_here == equip_there:
                            #print((here, equip_here), (there, equip_there), 1)
                            graph.add_edge((here, equip_here), (there, equip_there), weight=1)
                        else:
                            #print((here, equip_here), (there, equip_there), 8)
                            graph.add_edge((here, equip_here), (there, equip_there), weight=8)

    print(depth, target)
    mins = networkx.algorithms.shortest_paths.weighted.dijkstra_path_length(graph, (0j, 't'), (target, 't'))
    #path = networkx.algorithms.shortest_paths.weighted.dijkstra_path(graph, (0j, 't'), (target, 't'))
    #print('\n'.join(map(str, path)))
    print(mins)
    return mins


if __name__ == '__main__':
    main()
