#!/bin/env python3

import input_readers

def main():
    'read input data, process it if necessary and call the part workers'
    #data = input_readers.read_input_str()
    # data = input_readers.read_input_int_list_multiline()
    # data = input_readers.read_input_str_list()
    data = input_readers.read_input_table_tab(sep=', ')

    assert part1(((1, 1), (1, 6), (8, 3), (3, 4), (5, 5), (8, 9))) == 17
    #print('part1:', part1(data))

    # assert part2('') ==
    assert part2(((1, 1), (1, 6), (8, 3), (3, 4), (5, 5), (8, 9)), 32) == 16
    print('part2:', part2(data, 10000))

def part1(_data):
    'Do the part1 calculation'

    xvals = [x for x,y in _data]
    yvals = [y for x,y in _data]
    minx = min(xvals)
    miny = min(yvals)
    maxx = max(xvals)
    maxy = max(yvals)
    # print(xvals)
    # print(yvals)
    # print(minx, miny, maxx, maxy)

    board = {}  # (x, y): code
    board[(5,5)] = 'E'

    res = []  # sizes
    # for pos
    for center in _data:
        #print('center', center)
        size = 1
        for radius in range(1, max(maxx,maxy)):  # it will never get this big
            #print("radius", radius)
            size_on_this_radius = 0
            for pos in manhattan_circle(center, radius):

                #print('pos', pos)
                for other in _data:
                    if center == other:
                        continue
                    #print('other', other)
                    if manhattan_distance(center, pos) >= manhattan_distance(other, pos):
                        #print('mdr', manhattan_distance(center, other), 2*radius)
                        # if center == (5,5):
                        #     print(f'{radius} {pos} no')
                        break # someone else is close or same
                else:  # we were closest
                    if pos[0] < minx or pos[0] > maxx or pos[1] < miny or pos[1] > maxy:
                        #print(center, 'in infinite')
                        size = -1  # infinite size
                        break
                    #print(f'{radius} {pos} inc')
                    board[pos] = 'e'
                    size += 1
                    size_on_this_radius += 1
                if size == -1:
                    break
            if size == -1:
                break
            if not size_on_this_radius:
                break   # time to give up
        #print(center, size)
        res.append(size)


    def print_board(board):
        for y in range(miny, maxy+1):
            for x in range(minx, maxx+1):
                print(f'{board.get((x, y), ".")}', end='')
            print('')
    #print_board(board)

    return max(res)




    # print(list(manhattan_circle((10,10),2)))
    # print(manhattan_distance((1,1),(3,2)))




def manhattan_distance(pos1, pos2):

    res =  abs(abs(pos1[0]) - abs(pos2[0])) + abs(abs(pos1[1]) - abs(pos2[1]))
    # if pos1 == (5,5):
    #     print(f'md({pos1}, {pos2}) -> {res}')
    return res
    #return abs(abs(pos1[0]) - abs(pos2[0])) + abs(abs(pos1[1]) - abs(pos2[1]))

def manhattan_circle(center, radius):
    x, y = center
    x -= radius
    for _i in range(radius):
        x, y = x+1, y+1
        yield x, y
    for _i in range(radius):
        x, y = x+1, y-1
        yield x, y
    for _i in range(radius):
        x, y = x-1, y-1
        yield x, y
    for _i in range(radius):
        x, y = x-1, y+1
        yield x, y

def part2(_data, limit):
    'Do the part2 calculation'
    pass
    xvals = [x for x,y in _data]
    yvals = [y for x,y in _data]
    minx = min(xvals)
    miny = min(yvals)
    maxx = max(xvals)
    maxy = max(yvals)
    # print(xvals)
    # print(yvals)
    # print(minx, miny, maxx, maxy)


    res = []  # sizes
    # for pos

    size = 0
    for y in range(miny, maxy+1):
        for x in range(minx, maxx+1):
            s = sum(map(lambda other: manhattan_distance((x, y), other), _data))
            if s < limit:
                size += 1

    print(size)
    return size

    for center in _data:
        #print('center', center)
        size = 1
        for radius in range(1, max(maxx,maxy)):  # it will never get this big
            #print("radius", radius)
            size_on_this_radius = 0
            for pos in manhattan_circle(center, radius):

                #print('pos', pos)
                for other in _data:
                    if center == other:
                        continue
                    #print('other', other)
                    if manhattan_distance(center, pos) >= manhattan_distance(other, pos):
                        #print('mdr', manhattan_distance(center, other), 2*radius)
                        # if center == (5,5):
                        #     print(f'{radius} {pos} no')
                        break # someone else is close or same
                else:  # we were closest
                    if pos[0] < minx or pos[0] > maxx or pos[1] < miny or pos[1] > maxy:
                        #print(center, 'in infinite')
                        size = -1  # infinite size
                        break
                    #print(f'{radius} {pos} inc')
                    board[pos] = 'e'
                    size += 1
                    size_on_this_radius += 1
                if size == -1:
                    break
            if size == -1:
                break
            if not size_on_this_radius:
                break   # time to give up
        #print(center, size)
        res.append(size)


    return max(res)


if __name__ == '__main__':
    main()

