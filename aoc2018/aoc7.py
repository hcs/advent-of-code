#!/bin/env python3

import sortedcontainers
import input_readers

def main():
    'read input data, process it if necessary and call the part workers'
    data = input_readers.read_input_str()
    # data = input_readers.read_input_int_list_multiline()
    data = [(line.split()[1], line.split()[-3]) for line in input_readers.read_input_str_list()]
    # data = input_readers.read_input_table_tab()

    # print(data)
    assert part1((('C', 'A'), ('C', 'F'), ('A', 'B'), ('A', 'D'), ('B', 'E'), ('D', 'E'), ('F', 'E'))) == 'CABDFE'
    print('part1:', part1(data))

    # assert part2('') ==
    assert part2((('C', 'A'), ('C', 'F'), ('A', 'B'), ('A', 'D'), ('B', 'E'), ('D', 'E'), ('F', 'E')), testmode=True) == 15
    print('part2:', part2(data))

def part1(_data):
    'Do the part1 calculation'
    res = []
    done = set()

    steps = {}  # step: [then]
    requirements = {}  # step: {do_before}
    for step, before in _data:
        steps.setdefault(step, set()).add(before)
        for bef in before:
            requirements.setdefault(bef, set()).add(step)
    # print(steps)
    # print(requirements)

    not_end = set(map(lambda pair: pair[0], _data))
    not_start = set(map(lambda pair: pair[1], _data))
    starting_steps = not_end - not_start

    available_steps = sortedcontainers.SortedSet(starting_steps)
    while available_steps:
        step = available_steps.pop(0)
        res.append(step)
        done.add(step)

        potential_new_steps = steps.get(step, set()) - done
        for nstep in potential_new_steps:
            if not requirements.get(nstep, set()) - done:
                available_steps.add(nstep)

        # print(step, available_steps)
        # print('res', res)

    # print(''.join(res))
    return ''.join(res)


def part2(_data, testmode=False):
    'Do the part2 calculation'
    res = []
    done = set()
    num_workers = 2 if testmode else 5

    steps = {}  # step: [then]
    requirements = {}  # step: {do_before}
    for step, before in _data:
        steps.setdefault(step, set()).add(before)
        for bef in before:
            requirements.setdefault(bef, set()).add(step)

    not_end = set(map(lambda pair: pair[0], _data))
    not_start = set(map(lambda pair: pair[1], _data))
    all_steps = not_end.union(not_start)
    starting_steps = not_end - not_start

    available_steps = sortedcontainers.SortedSet(starting_steps)
    second = -1
    workers = sortedcontainers.SortedSet()  # [finished, what]
    while available_steps or workers:
        second += 1
        in_progress = set([step for _fin, step in workers])
        for (finished, step) in list(workers):
            if second ==  finished:
                workers.remove((finished, step))
                res.append(step)
                done.add(step)
                for nstep in all_steps - done - in_progress:  # check potential new steps
                    if not requirements.get(nstep, set()) - done:
                        available_steps.add(nstep)
        while len(workers) < num_workers and available_steps:
            step = available_steps.pop(0)
            when = second + ord(step) - ord('A') + 1 + (0 if testmode else 60)
            workers.add((when, step))

        #print(second, workers, res)

    return second


if __name__ == '__main__':
    main()

