#!/bin/env python3
import collections
import dataclasses
import functools
import itertools
import time
import input_readers
import numpy
import sortedcontainers


def timeme(method):
    """Decorator that prints the elapsed time after a call"""
    @functools.wraps(method)
    def wrapper(*args, **kw):
        start_time = time.time()
        result = method(*args, **kw)
        end_time = time.time()
        print('timed', wrapper.__name__, end_time - start_time, 's')
        return result
    return wrapper


def main():
    'read input data, process it if necessary and call the part workers'
    #data = input_readers.read_input_str()
    # data = input_readers.read_input_int_list_multiline()
    data = input_readers.read_input_str_list()
    # data = input_readers.read_input_table_tab()

    assert part1(
        ['#######',
         '#.G...#',
         '#...EG#',
         '#.#.#G#',
         '#..G#E#',
         '#.....#',
         '#######']
    ) == 27730


#     assert part1('''\
# #######
# #G..#E#
# #E#E.E#
# #G.##.#
# #...#E#
# #...E.#
# #######'''.splitlines()) == 36334

    assert part1('''\
#######
#E..EG#
#.#G.E#
#E.##E#
#G..#.#
#..E#.#
#######'''.splitlines()) == 39514

    assert part1('''\
#######
#E.G#.#
#.#G..#
#G.#.G#
#G..#.#
#...E.#
#######'''.splitlines()) == 27755

    assert part1('''\
#######
#.E...#
#.#..G#
#.###.#
#E#G#G#
#...#G#
#######'''.splitlines()) == 28944

    assert part1('''\
#########
#G......#
#.E.#...#
#..##..G#
#...##..#
#...#...#
#.G...G.#
#.....G.#
#########'''.splitlines()) == 18740




    print('part1:', part1(data))

    # assert part2('') ==
    print('part2:', part2(data))

WALL = '#'
GOBLIN = 'G'
ELF = 'E'
FLOOR = '.'

@dataclasses.dataclass
class Fighter:
    type: str  # E/G
    hit_points: int = 200
    attack_power = 3

    def is_dead(self):
        return self.hit_points < 1

def take_turn(map_, fighters, currentfpos, currentf):
    '''return True if a target was found'''
    #print(currentfpos, currentf)
    #found_target = False
    tmap = map_.copy()  # turn_map
    # add fighters to tmap
    tcount = 0
    for fpos, fig in fighters.items():
        # treat friends like walls
        tmap[fpos] = '#' if fig.type == currentf.type else 'T'
        if fig.type != currentf.type:
            tcount += 1
    if tcount == 0:
        return False  # No targets at all

    tmap[currentfpos] = 'X'
    #print_map(tmap)


    def walk(start_pos):
        wmap = tmap.copy()  # walk map
        #current = sortedcontainers.SortedSet([currentfpos])
        current = set([start_pos])
        for dist in itertools.count(1):
            last = sorted(current)
            current = set()
            for y, x in last:
                trail = wmap[y, x] if isinstance(wmap[y, x], tuple) else tuple()
                for pos in ((y-1, x), (y, x-1), (y, x+1), (y+1, x)):
                    elem = wmap[pos]
                    if elem in ('.', 'T'): # .#Tn
                        if elem == '.':
                            wmap[pos] = trail + (pos,)
                            current.add(pos)
                        #print_map(wmap)
                        yield dist, pos, elem, trail
            if not current:
                break
            #print(dist)
            #print_map(wmap)

    def select_target(start_pos):
        for dist, pos, elem, path in walk(start_pos):
            if elem == 'T':
                #found_target = True
                #print('selected target:', dist, pos, path)
                return pos, dist, path
        return None, None, None

    def attack(from_pos):
        y, x = from_pos
        targets = []
        for pos in ((y-1, x), (y, x-1), (y, x+1), (y+1, x)):
            if tmap[pos] == 'T':
                targets.append((pos, fighters[pos]))
        # target the weakest, or first in reading order
        tpos, tfig = sorted(targets, key=lambda e: (e[1].hit_points, e[0]))[0]
        tfig.hit_points -= 3
        if tfig.is_dead():
            del fighters[tpos]

    #def search_enemy(pos, fighter):

        #target_type):

        #fighters.get(pos) or

    # select target
    target_pos, target_dist, target_path = select_target(currentfpos)
    if target_pos is None:
        return False  # no target to be found

    # attack if in range
    if target_dist == 1:
        attack(currentfpos)
        return True


    # move one step on target_path
    newfpos = currentfpos
    newfpos = target_path[0]
    tmap[currentfpos] = '.'
    tmap[newfpos] = 'X'
    fighters[newfpos] = fighters.pop(currentfpos)


    # select target
    #target_pos, target_dist, target_path = select_target(newfpos)
    # attack if in range
    if target_dist == 2:  # was 2 should now be 1
        attack(newfpos)

    return True


def print_map(map_, fighters={}):
    for y, row in enumerate(map_):
        figs = []
        for x, state in enumerate(row):
            fig = fighters.get((y, x))
            if fig:
                figs.append(fig)
                state = fig.type
            if isinstance(state, tuple):
                state = len(state)
            print(state or 'E', end='')
        #print(figs)
        #print('  ', ', '.join([fig for fig in figs]))
        print('  ', ', '.join([f'{fig.type}({fig.hit_points})' for fig in figs]))

@timeme
def part1(_data):
    'Do the part1 calculation'
    map_ = numpy.array([tuple(row) for row in  _data], dtype=object)
    numpy.set_printoptions(threshold=numpy.nan)


    # remove pieces from the board
    fighters = {}  # (y,x): Fighter
    for y, row in enumerate(map_):
        for x, elem in enumerate(row):
            if elem in ('E', 'G'):
                fighters[(y, x)] = Fighter(elem)
                map_[y, x] = '.'
    #print('START')
    #print_map(map_, fighters)

    full_rounds = 0
    for round in itertools.count(1):
        #print('START', round, full_rounds)
        #print_map(map_, fighters)
        combat_ends = False
        found_target = False
        for pos, fighter in sorted(fighters.items()):
            if fighter.is_dead():
                continue
            #print(pos, fighter)
            found_target2 = take_turn(map_, fighters, pos, fighter)
            #print('found_target', found_target2)
            found_target = found_target or found_target2
            #print_map(map_, fighters)
            if not found_target2:
                #print(fighter, fighters.values(), len([fig for fig in fighters.values() if fig.type != fighter.type]))
                if not [fig for fig in fighters.values() if fig.type != fighter.type]:
                    #print('combat_ends')
                    # no more targets
                    combat_ends = True
                    break
        else:
            full_rounds += 1
            #print('ended round')
        if combat_ends:
            break
    #print_map(map_, fighters)
    #print('END')
    hit_point_sum = sum([fig.hit_points for fig in fighters.values()])
    print(full_rounds, hit_point_sum, full_rounds * hit_point_sum)
    return full_rounds * hit_point_sum




@timeme
def part2(_data):
    'Do the part2 calculation'
    pass


if __name__ == '__main__':
    main()
