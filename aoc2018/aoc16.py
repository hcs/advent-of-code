#!/bin/env python3

import functools
import time
import input_readers

def timeme(method):
    """Decorator that prints the elapsed time after a call"""
    @functools.wraps(method)
    def wrapper(*args, **kw):
        start_time = time.time()
        result = method(*args, **kw)
        end_time = time.time()
        print('timed', wrapper.__name__, end_time - start_time, 's')
        return result
    return wrapper


def main():
    'read input data, process it if necessary and call the part workers'
    data = input_readers.read_input_str()
    # data = input_readers.read_input_int_list_multiline()
    #data = input_readers.read_input_str_list()
    # data = input_readers.read_input_table_tab()

    last_row = None
    before = None
    inst = None
    after = None
    section1 = []
    section2 = []
    section2f = False
    for row in data.splitlines():
        if section2f:
            if row == '':
                pass
            else:
                section2.append(tuple(map(int, row.split())))
        else:
            if row.startswith('Before: '):
                before = tuple(map(int, row[9:-1].split(', ')))
            elif row.startswith('After: '):
                after = tuple(map(int, row[9:-1].split(', ')))
                section1.append((before, inst, after))
            elif row == '':
                if last_row == '':
                    section2f = True
            else:
                inst = tuple(map(int, row.split()))
            last_row = row
    # print(section1)
    # print(section2)


    # assert part1('') ==
    print('part1:', part1(section1))

    # assert part2('') ==
    print('part2:', part2(section1, section2))


# use like this:
# regs[inst[3]] = OPCODE['addr'](regs, inst[1], inst[2])
OPCODE = {
    'addr': lambda r, a, b: r[a] + r[b],
    'addi': lambda r, a, b: r[a] + b,
    'mulr': lambda r, a, b: r[a] * r[b],
    'muli': lambda r, a, b: r[a] * b,
    'banr': lambda r, a, b: r[a] & r[b],
    'bani': lambda r, a, b: r[a] & b,
    'borr': lambda r, a, b: r[a] | r[b],
    'bori': lambda r, a, b: r[a] | b,
    'setr': lambda r, a, b: r[a],
    'seti': lambda r, a, b: a,
    'gtir': lambda r, a, b: a > r[b],
    'gtri': lambda r, a, b: r[a] > b,
    'gtrr': lambda r, a, b: r[a] > r[b],
    'eqir': lambda r, a, b: a == r[b],
    'eqri': lambda r, a, b: r[a] == b,
    'eqrr': lambda r, a, b: r[a] == r[b],

}

def exec_inst(inst, regs):
    regs = list(regs)
    regs[inst[3]] = ops['addr'](inst[1],inst[2])
    return regs

@timeme
def part1(_data):
    'Do the part1 calculation'

    res_counter = 0  # how many samples in your puzzle input behave like three or more opcodes?


    for sample in _data:
        #print(sample)
        before, inst, after = sample
        matches = []
        for (code, op) in OPCODE.items():
            regs = list(before)
            regs[inst[3]] = op(regs, inst[1], inst[2])
            #if tuple(regs) == before:
                #print('ERRORRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRr')
            if tuple(regs) == after:
                matches.append(code)
        #print(matches)
        if len(matches)>=3:
            res_counter += 1
    return res_counter



@timeme
def part2(samples, program):
    'Do the part2 calculation'
    possibilities = { opcoden: set(OPCODE.keys()) for opcoden in range(16) }
    #print('\n'.join(map(str, possibilities.items())))
    for sample in samples:
        #print(sample)
        before, inst, after = sample
        matches = []
        for (code, op) in OPCODE.items():
            regs = list(before)
            regs[inst[3]] = op(regs, inst[1], inst[2])
            #if tuple(regs) == before:
                #print('ERRORRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRr')
            if tuple(regs) == after:
                matches.append(code)
        possibilities[inst[0]].intersection_update(matches)
        #print(matches)
    #print('\n'.join(map(str, possibilities.items())))


    # now use the exclusion method to boil down the possibilities into a codemap
    codemap = {opcoden: None for opcoden in range(16)}
    while possibilities:
        for opcoden, posi in possibilities.items():
            if len(posi) == 1:
                opcode = posi.pop()
                codemap[opcoden] = opcode
                del possibilities[opcoden]
                for opcoden in possibilities:
                    possibilities[opcoden].discard(opcode)
                break  # we have modified  possibilities so  we need to restart the inner loop
        else:
            print('cleanup failed')
            print(codemap)
            print('\n'.join(map(str, possibilities.items())))
            break

    print(codemap)

    regs = [0, 0, 0, 0]
    for inst in program:
        regs[inst[3]] = OPCODE[codemap[inst[0]]](regs, inst[1], inst[2])
    return regs[0]
if __name__ == '__main__':
    main()
