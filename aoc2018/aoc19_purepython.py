#!python3
'''
This is a manual reverse engineering of the input program
first a python rewrite then a optimized version.


it= 204000000 ip=6 [1, 4397251, 3, 10551373, 0, 6] ('addi', 5, 1, 5)
it= 205000000 ip=6 [1, 4522251, 3, 10551373, 0, 6] ('addi', 5, 1, 5)
it= 206000000 ip=6 [1, 4647251, 3, 10551373, 0, 6] ('addi', 5, 1, 5)
it= 207000000 ip=6 [1, 4772251, 3, 10551373, 0, 6] ('addi', 5, 1, 5)
it= 208000000 ip=6 [1, 4897251, 3, 10551373, 0, 6] ('addi', 5, 1, 5)
it= 209000000 ip=6 [1, 5022251, 3, 10551373, 0, 6] ('addi', 5, 1, 5)
it= 210000000 ip=6 [1, 5147251, 3, 10551373, 0, 6] ('addi', 5, 1, 5)
it= 211000000 ip=6 [1, 5272251, 3, 10551373, 0, 6] ('addi', 5, 1, 5)
it= 212000000 ip=6 [1, 5397251, 3, 10551373, 0, 6] ('addi', 5, 1, 5)
it= 213000000 ip=6 [1, 5522251, 3, 10551373, 0, 6] ('addi', 5, 1, 5)
it= 214000000 ip=6 [1, 5647251, 3, 10551373, 0, 6] ('addi', 5, 1, 5)
it= 215000000 ip=6 [1, 5772251, 3, 10551373, 0, 6] ('addi', 5, 1, 5)
it= 216000000 ip=6 [1, 5897251, 3, 10551373, 0, 6] ('addi', 5, 1, 5)
it= 217000000 ip=6 [1, 6022251, 3, 10551373, 0, 6] ('addi', 5, 1, 5)
it= 218000000 ip=6 [1, 6147251, 3, 10551373, 0, 6] ('addi', 5, 1, 5)
it= 219000000 ip=6 [1, 6272251, 3, 10551373, 0, 6] ('addi', 5, 1, 5)
it= 220000000 ip=6 [1, 6397251, 3, 10551373, 0, 6] ('addi', 5, 1, 5)
it= 221000000 ip=6 [1, 6522251, 3, 10551373, 0, 6] ('addi', 5, 1, 5)
it= 222000000 ip=6 [1, 6647251, 3, 10551373, 0, 6] ('addi', 5, 1, 5)
it= 223000000 ip=6 [1, 6772251, 3, 10551373, 0, 6] ('addi', 5, 1, 5)

r0 result, a bit slower than slow counter (sum of factors?)
r1 fast counter
r2 slow counter
r3 const
r4 temp
r5 pointer


 0 addi  5 16  5 : Jump to 17

JT if not slow>const
 1 seti  1  8  2 : r2=1
 2 seti  1  1  1 : r1=1

JT if not fast>const
if stuff
 3 mulr  2  1  4 : r4=r2*r1
 4 eqrr  4  3  4 : r4=r3==r1*r2
 5 addr  4  5  5 : Jump skip next if true
 6 addi  5  1  5 : Jump if false to endif
IF r3==r1*r2==fast*slow
 7 addr  2  0  0 : r0+=r2   # inc res with r2
endif
 8 addi  1  1  1 : r1+=1 # inc fast

if stuff
 9 gtrr  1  3  4 : r4=r1>r3 fast>const
10 addr  5  4  5 : Jump skip next if true
11 seti  2  8  5 : Jump if false to 3
IF fast>const continue

12 addi  2  1  2 : r2+=1

if stuff
13 gtrr  2  3  4 : r4=r2>r3 slow>const
14 addr  4  5  5 : Jump skip next if true
15 seti  1  7  5 : Jump goto 1 if false
IF slow>const:
16 mulr  5  5  5 : END PROGRAM

JT from 0, program start below sets const to x and r0 to 0 and then jumps to 1
17 addi  3  2  3 : r3+=2
18 mulr  3  3  3 : r3=r3*r3
19 mulr  5  3  3 : r3=19*r3
20 muli  3 11  3 : r3=11*r3
21 addi  4  6  4 : r4+=6
22 mulr  4  5  4 : r4=r4*22
23 addi  4  5  4 : r4+=5
24 addr  3  4  3 : r3=r3+r4
25 addr  5  0  5 : Jump skip next if part2
26 seti  0  0  5 : Jump to 1 (if part 1)
27 setr  5  3  4 : r4=27
28 mulr  4  5  4 : r4=r4*28
29 addr  5  4  4 : r4+=29
30 mulr  5  4  4 : r4=30*r4
31 muli  4 14  4 : r4=r4*14
32 mulr  4  5  4 : r4=r4*32
33 addr  3  4  3 : r3+=r4
34 seti  0  3  0 : r0=0
35 seti  0  0  5 : Jump to 1
'''

p1_const = 2*2*19*11+6*22+5
p2_const = p1_const + 30*14*32*(27*28+29)  #  10551373

def prog(const):
    slow = 0
    fast = 0
    res = 0  # r0
    for slow in range(const+1):  # inst 1 to 16
        for fast in range(const+1):  # inst 3 to 11
            if fast*slow == const:
                res += slow  # inst 7
        # 12
        slow += 1
    print(res)
    return res

def prog_v2(const):
    sum_ = 0
    for val in range(1, const+1):
        if const % val == 0:
            sum_ += val
    print(sum_)
    return sum_


assert prog(p1_const) == 1120
assert prog_v2(p1_const) == 1120
print(prog_v2(p2_const))


