#!/bin/env python3

import collections
import functools
import time
import input_readers

def timeme(method):
    """Decorator that prints the elapsed time after a call"""
    @functools.wraps(method)
    def wrapper(*args, **kw):
        start_time = time.time()
        result = method(*args, **kw)
        end_time = time.time()
        print('timed', wrapper.__name__, end_time - start_time, 's')
        return result
    return wrapper


def main():
    'read input data, process it if necessary and call the part workers'

    initial_state = '#...#...##..####..##.####.#...#...#.#.#.#......##....#....######.####.##..#..#..##.##..##....#######'
    state_map = {
        '.####': '.',
        '...#.': '.',
        '.##..': '#',
        '#.##.': '.',
        '#..##': '.',
        '#####': '#',
        '####.': '#',
        '.##.#': '#',
        '#.###': '.',
        '...##': '#',
        '.#.##': '#',
        '#..#.': '#',
        '#.#..': '#',
        '.###.': '#',
        '##.##': '#',
        '##..#': '.',
        '.#...': '#',
        '###.#': '.',
        '..##.': '.',
        '.....': '.',
        '###..': '#',
        '..#.#': '.',
        '.#..#': '#',
        '##...': '#',
        '#....': '.',
        '##.#.': '.',
        '..#..': '#',
        '....#': '.',
        '#...#': '.',
        '#.#.#': '#',
        '..###': '.',
        '.#.#.': '#',
    }

    t_initial_state = '#..#.#..##......###...###'
    t_state_map = {
        '...##': '#',
        '..#..': '#',
        '.#...': '#',
        '.#.#.': '#',
        '.#.##': '#',
        '.##..': '#',
        '.####': '#',
        '#.#.#': '#',
        '#.###': '#',
        '##.#.': '#',
        '##.##': '#',
        '###..': '#',
        '###.#': '#',
        '####.': '#',
    }


    data = (initial_state, state_map)
    tdata = (t_initial_state, t_state_map)
    #data = input_readers.read_input_str()
    # data = input_readers.read_input_int_list_multiline()
    # data = input_readers.read_input_str_list()
    # data = input_readers.read_input_table_tab()

    assert part1(tdata, 20) == 325
    print('part1:', part1(data, 20))

    # assert part2('') ==
    print('part2:', part1(data, 50000000000))



@timeme
def part1(_data, gens):
    'Do the part1 calculation'
    initial_state, state_map = _data
    living = {idx for (idx, val) in enumerate(initial_state) if val == '#'}
    print('gens', gens)
    def evolve(living):
        #print(living)
        for pos in range(min(living)-2, max(living)+3):
            key = ''.join([('#' if pos2 in living else '.') for pos2 in range(pos-2, pos+3)])
            if state_map.get(key, '.') == '#':
                yield pos

    diffs = collections.deque([], maxlen=10)
    lsum = sum(living)
    for gen in range(1, min(200, gens)+1):  # sum diff stabil before 200
        living = set(evolve(living))
        csum = sum(living)
        diff = csum - lsum
        lsum = csum
        # print(diffs, list(filter(lambda d2: d2 != diff, diffs)))
        if len(diffs) == 10 and not list(filter(lambda d2: d2 != diff, diffs)):
            #print(gen, sum(living), diff)
            break
        diffs.append(diff)

    if gen == gens:  # if we iterated over all gens
        return sum(living)

    # if we found a stable diff and are able to calculate the rest
    print(gen,gens,lsum, diff)
    return lsum+(gens-gen)*diff



@timeme
def part1_1(_data, gens):
    initial_state, state_map = _data
    state = '.' * 40 + initial_state + '.' * 40  # make space for higher and negative positions
    zero_at=40

    def evolve(state):
        yield '.'
        yield '.'
        for pos in range(2, len(state)-2):
            #print(state[pos-2:pos+3], state_map[state[pos-2:pos+3]])
            yield state_map.get(state[pos-2:pos+3], '.')
        yield '.'
        yield '.'
    print('.'*40)
    print(state)
    for gen in range(1, gens+1):
        state = ''.join(evolve(state))
        print(gen, state)
    res = 0
    for pos, val in enumerate(state, -40):
        if val == '#':
            res += pos
    return res


if __name__ == '__main__':
    main()
