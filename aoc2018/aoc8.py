#!/bin/env python3

import input_readers

def main():
    'read input data, process it if necessary and call the part workers'
    data = tuple(map(int, input_readers.read_input_str().split()))
    # data = input_readers.read_input_int_list_multiline()
    # data = input_readers.read_input_str_list()
    # data = input_readers.read_input_table_tab()

    assert part1(tuple(map(int, '2 3 0 3 10 11 12 1 1 0 1 99 2 1 1 2'.split()))) == 138
    print('part1:', part1(data))

    # assert part2('') ==
    assert part2(tuple(map(int, '2 3 0 3 10 11 12 1 1 0 1 99 2 1 1 2'.split()))) == 66
    print('part2:', part2(data))

def part1(_data):
    'Do the part1 calculation'
    metadata, _data = parse_tree(*_data)
    return sum(metadata)

def parse_tree(child_count, meta_count, *_data):
    #print(child_count, meta_count, _data)
    metadata = []
    for idx in range(child_count):
        child_metadata, _data = parse_tree(*_data)
        metadata.extend(child_metadata)

    metadata[0:0] = _data[:meta_count]
    _data = _data[meta_count:]

    return metadata, _data


def part2(_data):
    'Do the part2 calculation'
    value, _data = parse_tree2(*_data)
    return value

def parse_tree2(child_count, meta_count, *_data):
    #print(child_count, meta_count, _data)
    child_values = {}
    for idx in range(1, child_count+1):
        value, _data = parse_tree2(*_data)
        child_values[idx] = value

    metadata = _data[:meta_count]
    _data = _data[meta_count:]

    node_value = 0
    if not child_count:
        node_value = sum(metadata)
    else:
        for idx in metadata:
            node_value += child_values.get(idx, 0)

    return node_value, _data


if __name__ == '__main__':
    main()

