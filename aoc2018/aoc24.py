#!/bin/env python3

import functools
import itertools
import re
import queue
import dataclasses
import time
import typing
import input_readers

INPUT = '''Immune System:
457 units each with 4941 hit points with an attack that does 98 slashing damage at initiative 14
35 units each with 3755 hit points (weak to cold; immune to fire, bludgeoning) with an attack that does 1021 bludgeoning damage at initiative 1
6563 units each with 7155 hit points (immune to radiation, bludgeoning) with an attack that does 10 cold damage at initiative 11
5937 units each with 1868 hit points (weak to slashing) with an attack that does 2 fire damage at initiative 2
422 units each with 7279 hit points (immune to radiation, cold) with an attack that does 170 bludgeoning damage at initiative 16
279 units each with 9375 hit points (weak to cold) with an attack that does 333 slashing damage at initiative 10
4346 units each with 3734 hit points (weak to cold; immune to fire, slashing) with an attack that does 8 bludgeoning damage at initiative 9
1564 units each with 1596 hit points with an attack that does 8 bludgeoning damage at initiative 19
361 units each with 1862 hit points (weak to bludgeoning, slashing) with an attack that does 40 bludgeoning damage at initiative 3
427 units each with 8025 hit points (immune to fire, slashing; weak to cold) with an attack that does 139 bludgeoning damage at initiative 7

Infection:
27 units each with 24408 hit points (weak to bludgeoning, fire) with an attack that does 1505 cold damage at initiative 5
137 units each with 29784 hit points (immune to slashing) with an attack that does 423 cold damage at initiative 12
1646 units each with 15822 hit points (weak to slashing, cold; immune to fire) with an attack that does 16 fire damage at initiative 6
1803 units each with 10386 hit points (immune to cold, radiation) with an attack that does 11 bludgeoning damage at initiative 17
2216 units each with 39081 hit points with an attack that does 28 bludgeoning damage at initiative 20
3192 units each with 40190 hit points (weak to radiation, cold; immune to slashing) with an attack that does 22 radiation damage at initiative 15
1578 units each with 69776 hit points with an attack that does 63 cold damage at initiative 4
2950 units each with 40891 hit points (weak to fire) with an attack that does 25 slashing damage at initiative 8
70 units each with 56156 hit points (weak to radiation) with an attack that does 1386 radiation damage at initiative 13
1149 units each with 48840 hit points (weak to slashing) with an attack that does 63 slashing damage at initiative 18'''

TINPUT = '''Immune System:
17 units each with 5390 hit points (weak to radiation, bludgeoning) with an attack that does 4507 fire damage at initiative 2
989 units each with 1274 hit points (immune to fire; weak to bludgeoning, slashing) with an attack that does 25 slashing damage at initiative 3

Infection:
801 units each with 4706 hit points (weak to radiation) with an attack that does 116 bludgeoning damage at initiative 1
4485 units each with 2961 hit points (immune to radiation; weak to fire, cold) with an attack that does 12 slashing damage at initiative 4'''

def timeme(method):
    """Decorator that prints the elapsed time after a call"""
    @functools.wraps(method)
    def wrapper(*args, **kw):
        start_time = time.time()
        result = method(*args, **kw)
        end_time = time.time()
        print('timed', wrapper.__name__, end_time - start_time, 's')
        return result
    return wrapper



@dataclasses.dataclass
class Group:
    name: str
    units: int
    hit_points: int
    damage: int
    attack_type: str
    initiative: int
    weaknesses: typing.Tuple[str]
    immunities: typing.Tuple[str]

    def effective_power(self):
        return self.units * self.damage


    def calc_damage_from(self, attacker):
        damage = attacker.effective_power()
        if attacker.attack_type in self.immunities:
            damage = 0
        elif attacker.attack_type in self.weaknesses:
            damage *= 2
        return damage

    def take_damage_from(self, attacker):
        damage = self.calc_damage_from(attacker)
        killed_units = min(damage // self.hit_points, self.units)
        self.units = self.units - killed_units
        #print(attacker.name, 'deal', self.name, damage, killed_units)
        #print('killed', killed_units)

    # def select_target(self, enemies):
    #     # TODO
    #     return enemies[0], enemies[1:]


def main():
    'read input data, process it if necessary and call the part workers'
    #data = input_readers.read_input_str()
    # data = input_readers.read_input_int_list_multiline()
    # data = input_readers.read_input_str_list()
    # data = input_readers.read_input_table_tab()

    def parse(input_):
        side='immune'  # or infection
        immune_system = []
        infection = []
        for line in input_.splitlines():
            if line == 'Immune System:':
                side = 'immune'
                num = 0
            elif line == 'Infection:':
                side = 'infection'
                num = 0
            elif line == '':
                pass
            else:
                #print(line)
                match = re.match(
                    r'(.*) units each with (.*) hit points (\(.*\) |)with an attack that does (.*) (.*) damage at initiative (.*)',
                    line)
                units, hitp, properties, damage, attack_type, initiative = match.groups()
                properties = properties.strip().strip(' ()').split('; ')
                weaknesses = []
                immunities = []
                for prop in properties:
                    if not prop:
                        continue
                    what, lst = re.match(r'(weak|immune) to (.*)', prop).groups()
                    if what == 'weak':
                        weaknesses = lst.split(', ')
                    elif what == 'immune':
                        immunities = lst.split(', ')
                    else:
                        raise Exception('Unkonwn ' + prop)
                num += 1
                name = side[:2] + str(num)
                group = Group(name, int(units), int(hitp), int(damage), attack_type,
                            int(initiative), weaknesses, immunities)
                if side == 'immune':
                    immune_system.append(group)
                else:
                    infection.append(group)
        return immune_system, infection

    data = parse(INPUT)  # immune_system, infection
    tdata = parse(TINPUT)

    assert part1(tdata) == 5216
    #return
    ans1 = part1(data)
    print('part1:', ans1)
    assert ans1 == 14000

    assert part2(tdata) == 51
    print('part2:', part2(data))

def fight(immune_system, infection):
    #print('\n'.join(map(str, immune_system)))
    #print('\n'.join(map(str, infection)))
    while immune_system and infection:
        tot_units = (sum((group.units for group in immune_system)) +
                     sum((group.units for group in infection)))
        # print('Immune System:\n', '\n'.join(map(str, immune_system)))
        # print('Infection\n', '\n'.join(map(str, infection)))
        fight_queue = queue.PriorityQueue() #  (initiative, attecker, defender)
        # target selection
        for att_army, def_army in ((immune_system, infection), (infection, immune_system)):
            available_targets = list(def_army)
            for attacker in sorted(att_army,
                                   key=lambda a: (a.effective_power(), a.initiative),
                                   reverse=True):
                #print(attacker.name, attacker.units)
                available_targets.sort(key=lambda t, a=attacker: (
                    t.calc_damage_from(a),
                    t.effective_power(),
                    t.initiative))
                if available_targets[-1].calc_damage_from(attacker) == 0:
                    continue
                target = available_targets.pop()
                #target, available_targets = attacker.select_target(available_targets)
                fight_queue.put((-attacker.initiative, attacker, target))
                if not available_targets:
                    #print(attacker.name, attacker.units)
                    break

        # attacking
        while not fight_queue.empty():
            _initiative, attacker, target = fight_queue.get()
            #print(initiative, attacker.name, target.name)
            target.take_damage_from(attacker)

        # remove dead groups
        immune_system = [group for group in immune_system if group.units > 0]
        infection = [group for group in infection if group.units > 0]

        tot_units_after = (sum((group.units for group in immune_system)) +
                           sum((group.units for group in infection)))
        if tot_units == tot_units_after:
            # war is a tie
            #print('TIE')
            break

    return immune_system, infection

@timeme
def part1(_data):
    'Do the part1 calculation'
    immune_system, infection = _data
    immune_system = list(map(dataclasses.replace, immune_system))
    infection = list(map(dataclasses.replace, infection))

    immune_system, infection = fight(immune_system, infection)

    winner = immune_system or infection  # ignore possibility of a tie
    units = sum((group.units for group in winner))
    print(units)
    return units


@timeme
def part2(_data):
    'Do the part2 calculation'
    immune_system, infection = _data
    #print('\n'.join(map(str, immune_system)))
    #print('\n'.join(map(str, infection)))
    for boost in itertools.count(1):
        #print('boost:', boost)
        immune_system, infection = _data
        infection = list(map(dataclasses.replace, infection))
        immune_system = [dataclasses.replace(group, damage=group.damage+boost)
                         for group in immune_system]
        immune_system, infection = fight(immune_system, infection)
        if immune_system and not infection:
            units = sum((group.units for group in immune_system))
            print(units)
            # print('\n'.join(map(str, immune_system)))
            # print('\n'.join(map(str, infection)))
            return units



if __name__ == '__main__':
    main()
