#!/bin/env python3

import input_readers

def main():
    'read input data, process it if necessary and call the part workers'
    #data = input_readers.read_input_str()
    data = input_readers.read_input_int_list_multiline()
    # data = input_readers.read_input_str_list()
    # data = input_readers.read_input_table_tab()

    assert part1([1, -2, 3, 1]) == 3
    assert part1([1, 1, 1]) == 3
    assert part1([1, 1, -2]) == 0
    assert part1([-1, -2, -3]) == -6
    print('part1:', part1(data))

    # assert part2('') ==
    assert part2([1, -2, 3, 1]) == 2
    assert part2([1, -1]) == 0
    assert part2([3, 3, 4, -2, -4]) == 10
    assert part2([-6, 3, 8, 5, -6]) == 5
    assert part2([7, 7, -2, -7, -4]) == 14
    print('part2:', part2(data))

def part1(_data):
    'Do the part1 calculation'
    freq = 0
    for change in _data:
        freq += change

    return freq


def part2(_data):
    'Do the part2 calculation'
    freq = 0
    freq_history = set([0])
    while True:
        for change in _data:
            freq += change
            if freq in freq_history:
                return freq
            freq_history.add(freq)


if __name__ == '__main__':
    main()
