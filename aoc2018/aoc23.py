#!/bin/env python3

import functools
import re
import time
import z3
import input_readers

def timeme(method):
    """Decorator that prints the elapsed time after a call"""
    @functools.wraps(method)
    def wrapper(*args, **kw):
        start_time = time.time()
        result = method(*args, **kw)
        end_time = time.time()
        print('timed', wrapper.__name__, end_time - start_time, 's')
        return result
    return wrapper


def main():
    'read input data, process it if necessary and call the part workers'


    #data = input_readers.read_input_str()
    # data = input_readers.read_input_int_list_multiline()
    data = input_readers.read_input_str_list()
    # data = input_readers.read_input_table_tab()

    # pos=<-17480970,29380821,40124518>, r=86529590
    map_ = {}  # (x, y, z) = r
    for row in data:
        m = re.match(r'pos=<(.*),(.*),(.*)>, r=(.*)$', row)
        x, y, z, r = map(int, m.groups())
        map_[(x, y, z)] = r
    data = map_

    # assert part1('') ==
    print('part1:', part1(data))


    tdata = '''pos=<10,12,12>, r=2
pos=<12,14,12>, r=2
pos=<16,12,12>, r=4
pos=<14,14,14>, r=6
pos=<50,50,50>, r=200
pos=<10,10,10>, r=5'''.splitlines()
    map_ = {}  # (x, y, z) = r
    for row in tdata:
        m = re.match(r'pos=<(.*),(.*),(.*)>, r=(.*)$', row)
        x, y, z, r = map(int, m.groups())
        map_[(x, y, z)] = r
    tdata = map_

    assert part2(tdata) == 36
    print('part2:', part2(data))


# def do_overlap(bot1, bot2):
#     dist = sum(map(abs, ((c1-c2) for (c1, c2) in zip(bot1[0], bot2[0]))))
#     res = dist <= bot1[1] + bot2[1]
#     #print('do', bot1, bot2, dist, res)
#     #return dist <= bot1[1] + bot2[1]
#     return res

def do_overlap(bot1, bot2):
    ((x1, y1, z1), r1) = bot1
    ((x2, y2, z2), r2) = bot2
    #print(abs(x1-x2) + abs(y1-y2) + abs(z1-z2) <= r1 + r2)
    return abs(x1-x2) + abs(y1-y2) + abs(z1-z2) <= r1 + r2


def dist(pos1, pos2):
    return sum(map(abs, ((c1-c2) for (c1, c2) in zip(pos1, pos2))))

@timeme
def part1(_data):
    'Do the part1 calculation'
    map_ = _data
    #print(map_)
    max_r , max_pos = max(((r, pos) for (pos, r) in map_.items()))
    #print(max_pos, max_r)
    #print(dist((0,0,-1),(1,1,1)))
    in_range = {pos2 for pos2 in map_.keys() if dist(max_pos, pos2) <= max_r}
    return len(in_range)

# def volume(items):
#     minx = maxx = items[0][0][0]
#     miny = maxy = items[0][0][1]
#     minz = maxz = items[0][0][2]
#     for (x, y, z), r in items:
#         minx = min(minx, x)
#         miny = min(miny, y)
#         minz = min(minz, z)
#         maxx = max(maxx, x)
#         maxy = max(maxy, y)
#         maxz = max(maxz, z)
#     return (minx, miny, minz), (maxx, maxy, maxz)

# def find_most_overlaps(bots):
#     overlaps = set([frozenset((bot,)) for bot in bots])  # each bot overlaps itself   set(bot)
#     #print(overlaps)

#     layers = 2 # number of overlapping bots

#     while True:
#         print('overlaps', len(overlaps))
#         #print('\n'.join(map(str, overlaps)))
#         new_overlaps = set()
#         for overlap in overlaps:
#             for bot in bots - overlap:
#                 if all((do_overlap(bot, bot2) for bot2 in overlap)):
#                     new_overlaps.add(overlap | {bot})
#         if not new_overlaps:
#             break # overlaps contains largest overlaps
#         overlaps = set(new_overlaps)  # lets throw away the small overlays
#     return overlaps


# @timeme
# def part2(_data):
#     'Do the part2 calculation'
#     map_ = _data
#     bots = set(map_.items())  # a bot: (pos, r)

#     overlaps = find_most_overlaps(bots)
#     # overlaps is a list of the largest overlapping areas
#     print('Number of areas', len(overlaps))
#     print('Number of layers', len(overlaps[0]))

#     furthest_bot = sorted(overlaps, key=lambda bot: dist((0, 0, 0), bot[0]))[-1]

#     return



def z3abs(n):
    return z3.If(n >= 0, n, -n)

def z3dist(pos1, pos2):
    (x1, y1, z1) = pos1
    (x2, y2, z2) = pos2
    return z3abs(x1-x2) + z3abs(y1-y2) + z3abs(z1-z2)

def dist_to_target(bots):
    x, y, z, overlaps = z3.Ints("x y z overlaps")
    overlaps = overlaps * 0
    for pos, r in bots:
        overlaps += z3.If(z3dist((x, y, z), pos) <= r, 1, 0)
    o = z3.Optimize()
    o.maximize(overlaps)  # Find x,y,z:s with maximum number of overlapping bots
    o.minimize(z3dist((0, 0, 0), (x, y, z)))  # Find position in above closest to home
    o.check()
    model = o.model()
    return dist((0, 0, 0), (model[x].as_long(), model[y].as_long(), model[z].as_long()))


@timeme
def part2(_data):
    'Do the part2 calculation'
    map_ = _data
    bots = set(map_.items())  # a bot: (pos, r)

    return dist_to_target(bots)


""" IDEA
Try to build as large as possible and put misses in a miss queue...


"""


    # for idx, bot1 in enumerate(bots):
    #     (pos1, r1) = bot1
    #     for bot2 in bots[idx+1:]:
    #         (pos2, r2) = bot2
    #         if pos1 is pos2:
    #             continue
    #         if do_overlap(bot1, bot2):
    #             overlaps.append(set((pos1, pos2)))
    #print(len(overlaps))

    # for overlap in overlaps:
    #     for pos1, r1 in bots:
    #         if pos1 in overlap:
    #             continue
    #         for pos2 in overlap:
    #             if do_overlap(bot1, bot2):
    #                 break
    #         else:
    #             overlap.add(pos2)
    # print(len(overlaps))
    # print(list(map(len, overlaps)))





if __name__ == '__main__':
    main()
