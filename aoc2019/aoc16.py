#!/bin/env python3

import functools
import time

import input_readers


def timeme(method):
    """Decorator that prints the elapsed time after a call"""

    @functools.wraps(method)
    def wrapper(*args, **kw):
        start_time = time.time()
        result = method(*args, **kw)
        end_time = time.time()
        print("timed", wrapper.__name__, end_time - start_time, "s")
        return result

    return wrapper


def main():
    "read input data, process it if necessary and call the part workers"
    data = input_readers.read_input_str()
    # data = input_readers.read_input_int_list_multiline()
    # data = input_readers.read_input_str_list()
    # data = input_readers.read_input_table_tab()

    # assert part1('12345678') == None # used for manual tests using scaffolding
    # return
    assert part1("80871224585914546619083218645595") == "24176176"
    assert part1("19617804207202209144916044189917") == "73745418"
    assert part1("69317163492948606335995924319873") == "52432133"
    print("part1:", part1(data))

    assert part2("03036732577212944063491565474664" * 10000) == "84462026"
    assert part2("02935109699940807407585447034323" * 10000) == "78725270"
    assert part2("03081770884921959731165446850517" * 10000) == "53553731"
    print("part2:", part2(data * 10000))


def phase_1(input_):
    base_pattern = (0, 1, 0, -1)

    def pattern_valf(reps, pos):
        return base_pattern[((pos + 1) // reps) % 4]

    res = []
    for pos in range(len(input_)):
        sum_ = 0
        for pos2, val in enumerate(input_):
            pattern_val = pattern_valf(pos + 1, pos2)
            sum_ += val * pattern_val
        res.append(abs(sum_) % 10)
    return res


def phase(input_, message_offset):
    """I looked at reddit for help with part2 """

    input_len = len(input_)
    res = [0] * input_len
    for i in range(message_offset, input_len):
        sum_ = 0
        if i == message_offset:
            for j in range(i, input_len):
                sum_ += input_[j]
            res[i] = sum_ % 10
        else:
            res[i] = abs(10 + res[i - 1] - input_[i - 1]) % 10
    return res


@timeme
def part1(data):
    "Do the part1 calculation"
    input_ = [int(char) for char in data]
    # print(input_)

    for _phase_no in range(1, 100 + 1):
        input_ = phase_1(input_)
    res = "".join(map(str, input_[:8]))
    print(res)
    return res


@timeme
def part2(data):
    "Do the part2 calculation"
    message_offset = int(data[:7])
    input_ = [int(char) for char in data]
    # print(input_)

    for _phase_no in range(1, 100 + 1):
        input_ = phase(input_, message_offset)
    res = "".join(map(str, input_[message_offset : message_offset + 8]))
    print(res)
    return res


if __name__ == "__main__":
    main()
