#!/bin/env python3

import functools
import itertools
import math
import time

import input_readers
import utils


def timeme(method):
    """Decorator that prints the elapsed time after a call"""

    @functools.wraps(method)
    def wrapper(*args, **kw):
        start_time = time.time()
        result = method(*args, **kw)
        end_time = time.time()
        print("timed", wrapper.__name__, end_time - start_time, "s")
        return result

    return wrapper


def main():
    "read input data, process it if necessary and call the part workers"
    # data = input_readers.read_input_str()
    # data = input_readers.read_input_int_list_multiline()
    data = input_readers.read_input_str_list()
    # data = input_readers.read_input_table_tab()

    # assert part1('') ==
    assert (
        part1(
            """.#..#
.....
#####
....#
...##""".splitlines()
        )
        == ((3, 4), 8)
    )
    part1_res = part1(data)
    print("part1:", part1_res)

    assert (
        part2(
            """.#..##.###...#######
##.############..##.
.#.######.########.#
.###.#######.####.#.
#####.##.#.##.###.##
..#####..#.#########
####################
#.####....###.#.#.##
##.#################
#####.##.###..####..
..######..##.#######
####.##.####...##..#
.#####..#.######.###
##...#.##########...
#.##########.#######
.####.#.###.###.#.##
....##.##.###..#####
.#.#.###########.###
#.#.#.#####.####.###
###.##.####.##.#..##""".splitlines(),
            (11, 13),
        )
        == ((8, 2), 802)
    )

    print("part2:", part2(data, part1_res[0]))


@timeme
def part1(_data):
    "Do the part1 calculation"
    maxx = len(_data[0]) - 1
    maxy = len(_data) - 1
    # print(maxx, maxy)
    map_ = {}
    for y, line in enumerate(_data):
        map_.update({(x, y): val for x, val in enumerate(line)})
    astroid_count = len(list(filter(lambda val: val == "#", map_.values())))
    # print("astroid_count", astroid_count)

    def can_see_from(pos):
        # print("CSF", pos)
        (x, y) = pos
        hidden = 0
        checked = set()  # of pos

        def astroids_in_dir(pos, xdir, ydir):
            """how many astroids in given direction (can only see first)"""
            # nonlocal checked
            (x, y) = pos
            (x, y) = pos = (x + xdir, y + ydir)
            if x > maxx or x < 0 or y > maxy or y < 0 or pos in checked:
                return 0
            checked.add(pos)
            return astroids_in_dir(pos, xdir, ydir) + (1 if map_[pos] == "#" else 0)

        for xdir, ydir in all_directions():
            if not (xdir or ydir):
                continue
            aid = astroids_in_dir(pos, xdir, ydir)
            # print("AID", pos, xdir, ydir, aid)
            if aid > 1:
                hidden += aid - 1  # all except the first is hidden
        # print("CSF", pos, " ->",  hidden, astroid_count - hidden)
        return astroid_count - hidden - 1  # one is where we are

    def all_directions():
        directions = itertools.product(range(-maxx // 2 - 1, maxx // 2 + 3), repeat=2)
        directions = sorted(directions, key=max)
        for x, y in directions:
            if math.gcd(x, y) == 1:
                yield x, y

    observatory_positions = {}

    utils.print_board(map_)

    for (x, y), val in map_.items():
        if val == ".":
            continue
        seen_astroids = can_see_from((x, y))
        observatory_positions[(x, y)] = seen_astroids
    # utils.print_board(observatory_positions)
    # print(sorted(observatory_positions.items(), key=lambda val: val[-1]))
    best = sorted(observatory_positions.items(), key=lambda val: val[-1])[-1]
    # print(best)
    return best


@timeme
def part2(_data, pos):
    "Do the part2 calculation"
    maxx = len(_data[0]) - 1
    maxy = len(_data) - 1
    # print(maxx, maxy)
    map_ = {}
    for y, line in enumerate(_data):
        for x, val in enumerate(line):
            if val == "#":
                map_[(x, y)] = True

    # astroid_count = len(list(filter(lambda val: val == "#", map_.values())))
    # print("astroid_count", astroid_count)

    def zap_from(pos):
        # print("CSF", pos)
        (x, y) = pos

        def zap_astroid_in_dir(pos, xdir, ydir):
            """how many astroids in given direction (can only see first)"""
            # nonlocal checked
            (x, y) = pos
            (x, y) = pos = (x + xdir, y + ydir)
            if pos in checked:
                raise Exception("checked twice")
            if x > maxx or x < 0 or y > maxy or y < 0 or pos in checked:
                return None
            checked.add(pos)
            print("check", pos, compass_dir(*pos))
            if map_.get(pos, False):
                del map_[pos]
                return pos
            return zap_astroid_in_dir(pos, xdir, ydir)

        zap_count = 0
        for rotation in itertools.count(1):
            print("rotation", rotation)
            checked = set()  # of pos
            for xdir, ydir in all_directions():
                if not (xdir or ydir):
                    continue
                zapped = zap_astroid_in_dir(pos, xdir, ydir)
                if zapped:
                    zap_count += 1
                    print("zapped", zap_count, zapped)
                    if zap_count == 200:
                        return zapped

    def all_directions():
        """start up and sweep clockwise"""
        directions = itertools.product(range(-maxx - 1, maxx + 2), repeat=2)
        # directions = sorted(directions, key=max)
        directions = filter(lambda pos: math.gcd(*pos) == 1, directions)
        directions = sorted(directions, key=lambda pos: compass_dir(*pos) % 360,)
        for pos in directions:
            yield pos

    def compass_dir(xdir, ydir):
        res = math.degrees(math.atan2(xdir, -ydir))
        if res >= 0:
            return res
        else:
            return 360 + res

    # observatory_positions = {}

    for dir_ in [
        [0, -1],
        [1, 0],
        [1, 1],
        [0, 1],
        [-1, 0],
        [-1, -1],
    ]:
        print(compass_dir(*dir_))
    utils.print_board(map_)
    no_200 = zap_from(pos)
    return no_200, no_200[0] * 100 + no_200[1]  # < 2719

    # for (x, y), val in map_.items():
    #     if val == ".":
    #         continue
    #     seen_astroids = can_see_from((x, y))
    #     observatory_positions[(x, y)] = seen_astroids
    # utils.print_board(observatory_positions)
    # print(sorted(observatory_positions.items(), key=lambda val: val[-1]))
    # best = sorted(observatory_positions.items(), key=lambda val: val[-1])[-1]
    # print(best)
    # return best


if __name__ == "__main__":
    main()
