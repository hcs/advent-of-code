#!/bin/env python3

import functools
import time

import input_readers


def timeme(method):
    """Decorator that prints the elapsed time after a call"""

    @functools.wraps(method)
    def wrapper(*args, **kw):
        start_time = time.time()
        result = method(*args, **kw)
        end_time = time.time()
        print("timed", wrapper.__name__, end_time - start_time, "s")
        return result

    return wrapper


def main():
    "read input data, process it if necessary and call the part workers"
    data = input_readers.read_input_str()
    # data = input_readers.read_input_int_list_multiline()
    # data = input_readers.read_input_str_list()
    # data = input_readers.read_input_table_tab()

    # assert part1('') ==
    print("part1:", part1(25, 6, data))

    # assert part2('') ==
    assert part2(2, 2, "0222112222120000") == ["01", "10"]
    print("part2:", part2(25, 6, data))


@timeme
def part1(width, height, data):
    "Do the part1 calculation"
    assert len(data) % (width * height) == 0
    layer_size = width * height
    layer_count = len(data) // (width * height)
    layers = []

    zero_counts = []
    for i in range(layer_count):
        layer = data[i * layer_size : (i + 1) * layer_size]
        layers.append(layer)
        zeroes = layer.count("0")
        zero_counts.append((zeroes, i))

    # print(zero_counts)
    # print(min(zero_counts))
    layer = layers[min(zero_counts)[1]]
    return layer.count("1") * layer.count("2")


@timeme
def part2(width, height, data):
    "Do the part2 calculation"
    assert len(data) % (width * height) == 0

    layer_size = width * height
    layer_count = len(data) // (width * height)
    layers = []

    for i in range(layer_count):
        layer = data[i * layer_size : (i + 1) * layer_size]
        layers.append(layer)

    # merge layers
    image = []
    for pixel_layers in zip(*layers):
        # 0 is black, 1 is white, and 2 is transparent.
        res = "2"
        for val in pixel_layers:
            if val == "0" or val == "1":
                res = val
                break
        image.append(res)
    lines = []
    for row in range(height):
        lines.append("".join(image[row * width : (row + 1) * width]))

    for line in lines:
        print(line.replace("0", " ").replace("1", "*"))
    return lines


if __name__ == "__main__":
    main()
