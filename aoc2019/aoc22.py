#!/bin/env python3

import functools
import time

import input_readers


def timeme(method):
    """Decorator that prints the elapsed time after a call"""

    @functools.wraps(method)
    def wrapper(*args, **kw):
        start_time = time.time()
        result = method(*args, **kw)
        end_time = time.time()
        print("timed", wrapper.__name__, end_time - start_time, "s")
        return result

    return wrapper


def main():
    "read input data, process it if necessary and call the part workers"
    # data = input_readers.read_input_str()
    # data = input_readers.read_input_int_list_multiline()
    data = input_readers.read_input_str_list()
    # data = input_readers.read_input_table_tab()

    # assert part1('') ==
    assert part1("""deal into new stack""".splitlines(), 10) == list(
        map(int, "9 8 7 6 5 4 3 2 1 0".split(" "))
    )
    assert part1("""cut 3""".splitlines(), 10) == list(
        map(int, "3 4 5 6 7 8 9 0 1 2".split(" "))
    )
    assert part1("""deal with increment 3""".splitlines(), 10) == list(
        map(int, "0 7 4 1 8 5 2 9 6 3".split(" "))
    )

    assert part1("""cut 3""".splitlines(), 10) == list(
        map(int, "3 4 5 6 7 8 9 0 1 2".split(" "))
    )
    assert part1("""cut -4""".splitlines(), 10) == list(
        map(int, "6 7 8 9 0 1 2 3 4 5".split(" "))
    )

    assert (
        part1(
            """deal into new stack
cut -2
deal with increment 7
cut 8
cut -4
deal with increment 7
cut 3
deal with increment 9
deal with increment 3
cut -1""".splitlines(),
            10,
        )
        == list(map(int, "9 2 5 8 1 4 7 0 3 6".split(" ")))
    )

    print("part1:", part1(data))

    # assert part2('') ==
    print("part2:", part2(data))


@timeme
def part1(_data, deck_size=10007):
    "Do the part1 calculation"

    deck = list(range(deck_size))
    for op in _data:
        if op.startswith("deal into new stack"):
            deck.reverse()
        elif op.startswith("deal with increment"):
            inc = int(op.split(" ")[-1])
            deck2 = [None] * deck_size
            pos = 0
            for card in deck:
                deck2[pos] = card
                pos = (pos + inc) % deck_size
            deck = deck2
        elif op.startswith("cut"):
            size = int(op.split(" ")[-1])
            if size > 0:
                tmp = deck[:size]
                del deck[:size]
                deck.extend(tmp)
            if size < 0:
                size = abs(size)
                tmp = deck[-size:]
                del deck[-size:]
                deck = tmp + deck
        else:
            raise Exception("Unknown op", op)
    if deck_size == 10007:
        return deck.index(2019)
    else:
        print(deck)
        return deck


@timeme
def part2(_data):
    "Do the part2 calculation"


if __name__ == "__main__":
    main()
