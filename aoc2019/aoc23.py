#!/bin/env python3

import collections
import functools
import time

import input_readers


def timeme(method):
    """Decorator that prints the elapsed time after a call"""

    @functools.wraps(method)
    def wrapper(*args, **kw):
        start_time = time.time()
        result = method(*args, **kw)
        end_time = time.time()
        print("timed", wrapper.__name__, end_time - start_time, "s")
        return result

    return wrapper


def main():
    "read input data, process it if necessary and call the part workers"
    data = input_readers.read_input_str()
    data = list(map(int, data.split(",")))
    # data = input_readers.read_input_int_list_multiline()
    # data = input_readers.read_input_str_list()
    # data = input_readers.read_input_table_tab()

    # assert part1('') ==
    print("part1:", part1(data))
    print("part1_impl2:", part1_impl2(data))

    # assert part2('') ==
    print("part2:", part2(data))


class Computer:
    def __init__(
        self,
        mem,
        input_=None,
        input_callback=None,
        output_callback=None,
        default_input=None,
    ):
        # self.mem = list(mem)

        self.mem = collections.defaultdict(lambda: 0, enumerate(mem))

        self.pos = 0
        self.halt = False
        self.relative_base = 0
        self.input = []
        if input_:
            self.input.extend(input_)
        # function to call instead of using self.input
        self.input_callback = input_callback
        # function to call instead of using self.output
        self.output_callback = output_callback
        self.output = []
        self.default_input = default_input  # use value in self.input is empty

    def exec_prog(self, input_=None):
        if input_ is not None:
            assert not self.input  # possible but not used
            self.input.extend(input_)
        while not self.halt:
            self.exec_op()
        # print("Halted, output:", self.output)

    def exec_for_input(self, input_=None):
        """pause when input is empty"""
        assert not self.input_callback  # possible but not used
        if input_ is not None:
            assert not self.input
            self.input.extend(input_)
        while not self.halt and self.input:
            self.exec_op()
        # print("Halted, output:", self.output)
        return None  # Halted

    def exec_to_output(self, input_=None):
        """return first output value found and pause"""
        assert not self.output_callback
        if input_ is not None:
            assert not self.input  # possible but not used
            self.input.extend(input_)
        while not self.halt:
            if self.output:
                return self.output.pop(0)
            self.exec_op()
        # print("Halted, output:", self.output)
        return None  # Halted

    def exec_op(self):
        mem = self.mem
        pos = self.pos

        # parse opcode position
        opcode = mem[pos] % 100
        parameter_mode = list(
            reversed(list(map(int, str(mem[pos] // 100))))
        )  # 0 position mode, 1 immediate, 2 relative, default is 0
        # print("paramodes", parameter_mode)
        pos += 1  # we have eaten tho opcode

        def get_data():
            nonlocal pos
            val = mem[pos]
            pos += 1  # we have eaten a data field
            pm = parameter_mode.pop(0) if parameter_mode else 0

            if pm == 0:
                # print("pm", val, mem[val])
                # position mode
                return mem[val]
            elif pm == 1:
                # immediate mode
                # print("im", val)
                return val
            elif pm == 2:
                # relative mode
                return mem[self.relative_base + val]
            else:
                raise Exception("Unknown parameter_mode", pm)

        def write_data(data):
            nonlocal pos
            val = mem[pos]
            pos += 1  # we have eaten a data field
            pm = parameter_mode.pop(0) if parameter_mode else 0

            if pm == 0:
                # print("pm", val, mem[val])
                # position mode
                mem[val] = data
            elif pm == 1:
                # immediate mode
                Exception("target parameter in immediate mode")
            elif pm == 2:
                # relative mode
                mem[self.relative_base + val] = data
            else:
                raise Exception("Unknown parameter_mode", pm)

        if opcode == 1:
            write_data(get_data() + get_data())
        elif opcode == 2:
            write_data(get_data() * get_data())
        elif opcode == 3:
            # Opcode 3 takes a single integer as input and saves it to the address given by its only parameter.
            # For example, the instruction 3,50 would take an input value and store it at address 50.
            # print("read_input from", self.input)
            if self.input:
                val = self.input.pop(0)
                # print("in1", val)
            elif self.input_callback:
                val = self.input_callback()
            elif self.default_input is not None:
                val = self.default_input
            else:
                raise Exception("no input available")
            write_data(val)
        elif opcode == 4:
            # Opcode 4 outputs the value of its only parameter. For example, the instruction 4,50 would output the value at address 50.
            val = get_data()
            if self.output_callback is not None:
                # print("OUT", val)
                self.output_callback(val)
            else:
                # print("OUT2", val)
                self.output.append(val)
            # print("output:", val)
        elif opcode == 5:
            # Opcode 5 is jump-if-true: if the first parameter is non-zero,
            # it sets the instruction pointer to the value from the second parameter. Otherwise, it does nothing.
            if get_data():
                pos = get_data()
            else:
                get_data()
        elif opcode == 6:
            # Opcode 6 is jump-if-false: if the first parameter is zero, it sets the instruction
            # pointer to the value from the second parameter. Otherwise, it does nothing.
            if not get_data():
                pos = get_data()
            else:
                get_data()
        elif opcode == 7:
            # Opcode 7 is less than: if the first parameter is less than the second parameter,
            # it stores 1 in the position given by the third parameter. Otherwise, it stores 0.
            if get_data() < get_data():
                write_data(1)
            else:
                write_data(0)
        elif opcode == 8:
            # Opcode 8 is equals: if the first parameter is equal to the second parameter,
            # it stores 1 in the position given by the third parameter. Otherwise, it stores 0.
            if get_data() == get_data():
                write_data(1)
            else:
                write_data(0)
        elif opcode == 9:
            # Opcode 9 adjusts the relative base by the value of its only parameter.
            # The relative base increases (or decreases, if the value is negative) by the value of the parameter.
            self.relative_base += get_data()

        elif opcode == 99:
            self.halt = True
        else:
            raise Exception("Unknown opcode", opcode)

        self.pos = pos


@timeme
def part1(_data):
    "Do the part1 calculation"
    computers = [Computer(_data, [naddr], default_input=-1) for naddr in range(50)]

    while True:
        for cmp in computers:
            cmp.exec_op()
            if cmp.halt:
                raise Exception("cmp halted")
            if len(cmp.output) >= 3:
                dest = cmp.output.pop(0)
                x = cmp.output.pop(0)
                y = cmp.output.pop(0)
                if dest == 255:
                    return y
                computers[dest].input.extend([x, y])


@timeme
def part1_impl2(_data):
    "Do the part2 calculation"
    in_queues = [[] for _ in range(50)]
    out_queues = [[] for _ in range(50)]
    nat_x = nat_y = None

    def get_inp(naddr):
        # print("get_inp", naddr)
        if in_queues[naddr]:
            val = in_queues[naddr].pop(0)
            # print("get_inp", naddr, val)
            return val
        else:
            return -1

    def net_out(fr_naddr, val):
        oq = out_queues[fr_naddr]
        oq.append(val)
        # if we have a complete packet
        if len(oq) == 3:
            dest = oq.pop(0)
            x = oq.pop(0)
            y = oq.pop(0)
            if dest == 255:
                nonlocal nat_x
                nonlocal nat_y
                nat_x = x
                nat_y = y
                # print("to_nat", nat_x, nat_y)
            else:
                in_queues[dest].extend([x, y])

    computers = [
        Computer(
            _data,
            [naddr],
            input_callback=lambda naddr=naddr: get_inp(naddr),
            output_callback=lambda val, naddr=naddr: net_out(naddr, val),
        )
        for naddr in range(50)
    ]

    import itertools

    for cmp in itertools.cycle(computers):
        cmp.exec_op()
        if nat_y is not None:
            return nat_y


@timeme
def part2(_data):
    "Do the part2 calculation"
    in_queues = [[naddr] for naddr in range(50)]
    out_queues = [[] for _ in range(50)]
    nat_x = nat_y = None
    delivered_y = None
    res = None
    idle = [False] * 50
    # idle2 = [False]*50
    idle_counter = 0

    def get_inp(naddr):
        nonlocal idle_counter
        if in_queues[naddr]:
            idle_counter = 0
            idle[naddr] = False
            val = in_queues[naddr].pop(0)
            # print("get_inp", naddr, val)
            return val
        else:
            idle[naddr] = True
            idle_counter += 1
            # print("get_inp", naddr, -1)
            return -1

    def net_out(fr_naddr, val):
        nonlocal idle_counter
        nonlocal nat_x
        nonlocal nat_y
        oq = out_queues[fr_naddr]
        oq.append(val)
        # print("out", fr_naddr, oq)
        idle_counter = 0
        idle[fr_naddr] = False
        # if we have a complete packet
        if len(oq) == 3:
            # print("out from", fr_naddr, oq)
            dest = oq.pop(0)
            x = oq.pop(0)
            y = oq.pop(0)
            if dest == 255:
                nat_x = x
                nat_y = y
                # print("to_NAT", fr_naddr,  nat_x, nat_y)
            else:
                idle[dest] = False
                # print("out", fr_naddr,  x, y)
                in_queues[dest].extend([x, y])

    """res should be < 25110"""

    computers = [
        Computer(
            _data,
            # [naddr],
            input_callback=lambda naddr=naddr: get_inp(naddr),
            output_callback=lambda val, naddr=naddr: net_out(naddr, val),
        )
        for naddr in range(50)
    ]

    import itertools

    for _i, cmp in enumerate(itertools.cycle(computers)):
        # print("op", _i)
        cmp.exec_op()
        if res is not None:
            return res
        # if idle_counter == 100:
        if nat_y is not None and False not in idle:
            # print(idle)
            print("idle", nat_y)
            idle_counter = 0
            idle = [False] * 50
            in_queues[0].extend([nat_x, nat_y])
            if nat_y == delivered_y:
                return nat_y
            delivered_y = nat_y
        # print(idle)

    # nat_x = nat_y = None
    # while True:
    #     idle = True
    #     for cmp in computers:
    #         cmp.exec_op()
    #         if cmp.halt:
    #             raise Exception("cmp halted")
    #         if len(cmp.output) >= 3:
    #             idle = False
    #             dest = cmp.output.pop(0)
    #             x = cmp.output.pop(0)
    #             y = cmp.output.pop(0)
    #             if dest == 255:
    #                 nat_x = x
    #                 nat_y = y


if __name__ == "__main__":
    main()
