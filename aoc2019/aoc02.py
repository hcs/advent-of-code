#!/bin/env python3

import functools
import time

import input_readers


def timeme(method):
    """Decorator that prints the elapsed time after a call"""

    @functools.wraps(method)
    def wrapper(*args, **kw):
        start_time = time.time()
        result = method(*args, **kw)
        end_time = time.time()
        print("timed", wrapper.__name__, end_time - start_time, "s")
        return result

    return wrapper


def main():
    "read input data, process it if necessary and call the part workers"
    data = input_readers.read_input_str()
    data = list(map(int, data.split(",")))
    # data = input_readers.read_input_int_list_multiline()
    # data = input_readers.read_input_str_list()
    # data = input_readers.read_input_table_tab()

    assert exec1([1, 0, 0, 0, 99]) == [2, 0, 0, 0, 99]
    assert exec1([2, 3, 0, 3, 99]) == [2, 3, 0, 6, 99]
    assert exec1([2, 4, 4, 5, 99, 0]) == [2, 4, 4, 5, 99, 9801]
    assert exec1([1, 1, 1, 4, 99, 5, 6, 0, 99]) == [30, 1, 1, 4, 2, 5, 6, 0, 99]
    print("part1:", part1(data))

    # assert part2('') ==
    print("part2:", part2(data))


def exec_op(opcode, data1, data2):
    if opcode == 1:
        return data1 + data2
    elif opcode == 2:
        return data1 * data2
    else:
        raise Exception("Unknown opcode", opcode)


def exec1(_data):
    pos = 0
    # print(_data)
    while True:
        opcode = _data[pos]
        if opcode == 99:
            break
        data1 = _data[_data[pos + 1]]
        data2 = _data[_data[pos + 2]]
        # print(pos, opcode, data1, data2)
        _data[_data[pos + 3]] = exec_op(opcode, data1, data2)
        # print(_data)
        pos += 4

    return _data


@timeme
def part1(_data):
    "Do the part1 calculation"
    _data = list(_data)
    _data[1] = 12
    _data[2] = 2
    exec1(_data)
    return _data[0]


@timeme
def part2(_data):
    "Do the part2 calculation"
    org_data = _data
    for noun in range(100):
        for verb in range(100):
            _data = list(org_data)
            # _data[1] = 12  # noun
            # _data[2] = 2   # verb
            _data[1] = noun  # noun
            _data[2] = verb  # verb
            try:
                exec1(_data)
            except Exception as err:
                print(err)
                continue
            res = _data[0]
            print(noun, verb, res)
            if res == 19690720:
                return 100 * noun + verb
    return None


if __name__ == "__main__":
    main()
