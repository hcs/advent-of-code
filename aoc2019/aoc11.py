#!/bin/env python3

import collections
import functools
import time

import input_readers
import utils


def timeme(method):
    """Decorator that prints the elapsed time after a call"""

    @functools.wraps(method)
    def wrapper(*args, **kw):
        start_time = time.time()
        result = method(*args, **kw)
        end_time = time.time()
        print("timed", wrapper.__name__, end_time - start_time, "s")
        return result

    return wrapper


def main():
    "read input data, process it if necessary and call the part workers"
    data = input_readers.read_input_str()
    data = list(map(int, data.split(",")))
    # data = input_readers.read_input_int_list_multiline()
    # data = input_readers.read_input_str_list()
    # data = input_readers.read_input_table_tab()

    test_computer()

    # assert part1('') ==
    print("part1:", part1(data))

    # assert part2('') ==
    print("part2:", part2(data))


class Computer:
    def __init__(self, mem, input_=None):
        # self.mem = list(mem)

        self.mem = collections.defaultdict(lambda: 0, enumerate(mem))

        self.pos = 0
        self.halt = False
        self.relative_base = 0
        if not input:
            self.input = []
        else:
            self.input = input_
        self.output = []

    def exec_prog(self, input_=None):
        if input_ is not None:
            self.input = input_
        while not self.halt:
            self.exec_op()
        # print("Halted, output:", self.output)

    def exec_for_input(self, input_=None):
        """pause when input is empty"""
        if input_ is not None:
            self.input = input_
        while not self.halt and self.input:
            self.exec_op()
        # print("Halted, output:", self.output)
        return None  # Halted

    def exec_to_output(self, input_=None):
        """return first output value found and pause"""
        if input_ is not None:
            self.input = input_
        while not self.halt:
            if self.output:
                return self.output.pop(0)
            self.exec_op()
        # print("Halted, output:", self.output)
        return None  # Halted

    def exec_op(self):
        mem = self.mem
        pos = self.pos

        # parse opcode position
        opcode = mem[pos] % 100
        parameter_mode = list(
            reversed(list(map(int, str(mem[pos] // 100))))
        )  # 0 position mode, 1 immediate, 2 relative, default is 0
        # print("paramodes", parameter_mode)
        pos += 1  # we have eaten tho opcode

        def get_data():
            nonlocal pos
            val = mem[pos]
            pos += 1  # we have eaten a data field
            pm = parameter_mode.pop(0) if parameter_mode else 0

            if pm == 0:
                # print("pm", val, mem[val])
                # position mode
                return mem[val]
            elif pm == 1:
                # immediate mode
                # print("im", val)
                return val
            elif pm == 2:
                # relative mode
                return mem[self.relative_base + val]
            else:
                raise Exception("Unknown parameter_mode", pm)

        def write_data(data):
            nonlocal pos
            val = mem[pos]
            pos += 1  # we have eaten a data field
            pm = parameter_mode.pop(0) if parameter_mode else 0

            if pm == 0:
                # print("pm", val, mem[val])
                # position mode
                mem[val] = data
            elif pm == 1:
                # immediate mode
                Exception("target parameter in immediate mode")
            elif pm == 2:
                # relative mode
                mem[self.relative_base + val] = data
            else:
                raise Exception("Unknown parameter_mode", pm)

        if opcode == 1:
            write_data(get_data() + get_data())
        elif opcode == 2:
            write_data(get_data() * get_data())
        elif opcode == 3:
            # Opcode 3 takes a single integer as input and saves it to the address given by its only parameter.
            # For example, the instruction 3,50 would take an input value and store it at address 50.
            # print("read_input from", self.input)
            write_data(self.input.pop(0))
        elif opcode == 4:
            # Opcode 4 outputs the value of its only parameter. For example, the instruction 4,50 would output the value at address 50.
            val = get_data()
            self.output.append(val)
            # print("output:", val)
        elif opcode == 5:
            # Opcode 5 is jump-if-true: if the first parameter is non-zero,
            # it sets the instruction pointer to the value from the second parameter. Otherwise, it does nothing.
            if get_data():
                pos = get_data()
            else:
                get_data()
        elif opcode == 6:
            # Opcode 6 is jump-if-false: if the first parameter is zero, it sets the instruction
            # pointer to the value from the second parameter. Otherwise, it does nothing.
            if not get_data():
                pos = get_data()
            else:
                get_data()
        elif opcode == 7:
            # Opcode 7 is less than: if the first parameter is less than the second parameter,
            # it stores 1 in the position given by the third parameter. Otherwise, it stores 0.
            if get_data() < get_data():
                write_data(1)
            else:
                write_data(0)
        elif opcode == 8:
            # Opcode 8 is equals: if the first parameter is equal to the second parameter,
            # it stores 1 in the position given by the third parameter. Otherwise, it stores 0.
            if get_data() == get_data():
                write_data(1)
            else:
                write_data(0)
        elif opcode == 9:
            # Opcode 9 adjusts the relative base by the value of its only parameter.
            # The relative base increases (or decreases, if the value is negative) by the value of the parameter.
            self.relative_base += get_data()

        elif opcode == 99:
            self.halt = True
        else:
            raise Exception("Unknown opcode", opcode)

        self.pos = pos


def test_computer():
    cmp = Computer(
        [109, 1, 204, -1, 1001, 100, 1, 100, 1008, 100, 16, 101, 1006, 101, 0, 99]
    )
    cmp.exec_prog()
    assert cmp.output == [
        109,
        1,
        204,
        -1,
        1001,
        100,
        1,
        100,
        1008,
        100,
        16,
        101,
        1006,
        101,
        0,
        99,
    ]

    cmp = Computer([1102, 34915192, 34915192, 7, 4, 7, 99, 0])
    cmp.exec_prog()
    assert len(cmp.output) == 1
    assert len(str(cmp.output[0])) == 16

    cmp = Computer([104, 1125899906842624, 99])
    cmp.exec_prog()
    assert cmp.output == [1125899906842624]


@timeme
def part1(_data):
    "Do the part1 calculation"
    cmp = Computer(_data, [1])

    # dirs = "^>v<"
    dir_ = 0
    hull = {}  # collections.defaultdict(lambda: 0)  # starts black
    pos = (0, 0)

    def left():
        nonlocal dir_
        dir_ -= 1
        if dir_ < 0:
            dir_ = 3

    def right():
        nonlocal dir_
        dir_ += 1
        if dir_ > 3:
            dir_ = 0

    def walk_one_forward():
        nonlocal pos
        x, y = pos
        if dir_ == 0:
            pos = x, y - 1
        elif dir_ == 1:
            pos = x + 1, y
        elif dir_ == 2:
            pos = x, y + 1
        elif dir_ == 3:
            pos = x - 1, y
        else:
            Exception("Unknown dir")

    while True:
        color = cmp.exec_to_output([hull.get(pos, 0)])
        if color is None:
            break
        hull[pos] = color
        turn = cmp.exec_to_output()
        if turn is None:
            break
        if turn == 0:
            left()
        else:
            right()
        walk_one_forward()

    utils.print_board(
        hull, get_char=lambda board, pos: "#" if board.get(pos, 0) else " "
    )

    return len(hull)


@timeme
def part2(_data):
    "Do the part2 calculation"
    cmp = Computer(_data, [1])

    # dirs = "^>v<"
    dir_ = 0
    hull = {(0, 0): 1}  # collections.defaultdict(lambda: 0)  # starts black
    pos = (0, 0)

    def left():
        nonlocal dir_
        dir_ -= 1
        if dir_ < 0:
            dir_ = 3

    def right():
        nonlocal dir_
        dir_ += 1
        if dir_ > 3:
            dir_ = 0

    def walk_one_forward():
        nonlocal pos
        x, y = pos
        if dir_ == 0:
            pos = x, y - 1
        elif dir_ == 1:
            pos = x + 1, y
        elif dir_ == 2:
            pos = x, y + 1
        elif dir_ == 3:
            pos = x - 1, y
        else:
            Exception("Unknown dir")

    while True:
        color = cmp.exec_to_output([hull.get(pos, 0)])
        if color is None:
            break
        hull[pos] = color
        turn = cmp.exec_to_output()
        if turn is None:
            break
        if turn == 0:
            left()
        else:
            right()
        walk_one_forward()

    utils.print_board(
        hull, get_char=lambda board, pos: "#" if board.get(pos, 0) else " "
    )

    return len(hull)


if __name__ == "__main__":
    main()
