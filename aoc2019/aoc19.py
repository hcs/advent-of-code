#!/bin/env python3

import collections
import functools
import itertools
import time

import input_readers


def timeme(method):
    """Decorator that prints the elapsed time after a call"""

    @functools.wraps(method)
    def wrapper(*args, **kw):
        start_time = time.time()
        result = method(*args, **kw)
        end_time = time.time()
        print("timed", wrapper.__name__, end_time - start_time, "s")
        return result

    return wrapper


def main():
    "read input data, process it if necessary and call the part workers"
    data = input_readers.read_input_str()
    data = list(map(int, data.split(",")))
    # data = input_readers.read_input_int_list_multiline()
    # data = input_readers.read_input_str_list()
    # data = input_readers.read_input_table_tab()

    # assert part1('') ==
    print("part1:", part1(data))

    # assert part2('') ==
    print("part2:", part2(data))


class Computer:
    def __init__(self, mem, input_=None):
        # self.mem = list(mem)

        self.mem = collections.defaultdict(lambda: 0, enumerate(mem))

        self.pos = 0
        self.halt = False
        self.relative_base = 0
        if not input:
            self.input = []
        else:
            self.input = input_
        self.input_callback = None  # function to call instead of using self.input
        self.output = []

    def print_state(self):
        print("memsize", len(self.mem))
        print("pos", self.pos)
        print("halt", self.halt)
        print("input", self.input)
        print("output", self.output)

    def exec_prog(self, input_=None):
        if input_ is not None:
            self.input = input_
        while not self.halt:
            self.exec_op()
        # print("Halted, output:", self.output)

    def exec_for_input(self, input_=None):
        """pause when input is empty"""
        if input_ is not None:
            self.input = input_
        while not self.halt and self.input:
            self.exec_op()
        # print("Halted, output:", self.output)
        return None  # Halted

    def exec_to_output(self, input_=None):
        """return first output value found and pause"""
        if input_ is not None:
            self.input = input_
        while not self.halt:
            if self.output:
                return self.output.pop(0)
            self.exec_op()
        # print("Halted, output:", self.output)
        return None  # Halted

    def exec_op(self):
        mem = self.mem
        pos = self.pos

        # parse opcode position
        opcode = mem[pos] % 100
        parameter_mode = list(
            reversed(list(map(int, str(mem[pos] // 100))))
        )  # 0 position mode, 1 immediate, 2 relative, default is 0
        # print("paramodes", parameter_mode)
        pos += 1  # we have eaten tho opcode

        def get_data():
            nonlocal pos
            val = mem[pos]
            pos += 1  # we have eaten a data field
            pm = parameter_mode.pop(0) if parameter_mode else 0

            if pm == 0:
                # print("pm", val, mem[val])
                # position mode
                return mem[val]
            elif pm == 1:
                # immediate mode
                # print("im", val)
                return val
            elif pm == 2:
                # relative mode
                return mem[self.relative_base + val]
            else:
                raise Exception("Unknown parameter_mode", pm)

        def write_data(data):
            nonlocal pos
            val = mem[pos]
            pos += 1  # we have eaten a data field
            pm = parameter_mode.pop(0) if parameter_mode else 0

            if pm == 0:
                # print("pm", val, mem[val])
                # position mode
                mem[val] = data
            elif pm == 1:
                # immediate mode
                Exception("target parameter in immediate mode")
            elif pm == 2:
                # relative mode
                mem[self.relative_base + val] = data
            else:
                raise Exception("Unknown parameter_mode", pm)

        if opcode == 1:
            write_data(get_data() + get_data())
        elif opcode == 2:
            write_data(get_data() * get_data())
        elif opcode == 3:
            # Opcode 3 takes a single integer as input and saves it to the address given by its only parameter.
            # For example, the instruction 3,50 would take an input value and store it at address 50.
            # print("read_input from", self.input)
            if self.input_callback:
                write_data(self.input_callback())
            else:
                write_data(self.input.pop(0))
        elif opcode == 4:
            # Opcode 4 outputs the value of its only parameter. For example, the instruction 4,50 would output the value at address 50.
            val = get_data()
            self.output.append(val)
            # print("output:", val)
        elif opcode == 5:
            # Opcode 5 is jump-if-true: if the first parameter is non-zero,
            # it sets the instruction pointer to the value from the second parameter. Otherwise, it does nothing.
            if get_data():
                pos = get_data()
            else:
                get_data()
        elif opcode == 6:
            # Opcode 6 is jump-if-false: if the first parameter is zero, it sets the instruction
            # pointer to the value from the second parameter. Otherwise, it does nothing.
            if not get_data():
                pos = get_data()
            else:
                get_data()
        elif opcode == 7:
            # Opcode 7 is less than: if the first parameter is less than the second parameter,
            # it stores 1 in the position given by the third parameter. Otherwise, it stores 0.
            if get_data() < get_data():
                write_data(1)
            else:
                write_data(0)
        elif opcode == 8:
            # Opcode 8 is equals: if the first parameter is equal to the second parameter,
            # it stores 1 in the position given by the third parameter. Otherwise, it stores 0.
            if get_data() == get_data():
                write_data(1)
            else:
                write_data(0)
        elif opcode == 9:
            # Opcode 9 adjusts the relative base by the value of its only parameter.
            # The relative base increases (or decreases, if the value is negative) by the value of the parameter.
            self.relative_base += get_data()

        elif opcode == 99:
            self.halt = True
        else:
            raise Exception("Unknown opcode", opcode)

        self.pos = pos


@timeme
def part1(_data):
    "Do the part1 calculation"
    import itertools

    map_ = {}
    inp = list(itertools.product(range(50), repeat=2))
    affected_points = 0
    for x, y in inp:
        cmp = Computer(_data)
        cmp.exec_prog([x, y])
        assert len(cmp.output) == 1
        affected_points += cmp.output[0]
        map_[(x, y)] = "#" if cmp.output[0] else "."
    import utils

    utils.print_board(map_)
    return affected_points

    # import itertools

    # cmp = Computer(_data)
    # inp = list(itertools.product(range(50), repeat=2))
    # affected_points = 0
    # for x, y in inp:
    #     if x == y == 0:
    #         continue
    #     print(x, y)
    #     cmp.print_state()
    #     res = cmp.exec_to_output([x, y])
    #     print("res", res)
    #     affected_points += res
    # return affected_points

    # # def input_callback():

    # #     print(cmp.output)
    # #     cmp.output = []
    # inp = list(itertools.product(range(50), repeat=2))
    # inp = itertools.chain(inp)
    # print(inp)
    # cmp = Computer(_data, inp)
    # cmp.exec_prog()
    # return cmp.output
    # # cmp.input_callback = input_callback

    # # while not cmp.halt:
    # return

    # cmp.exec_prog(list(map(ord, springscript)))
    # # cmp.exec_prog(robot_input)
    # print(cmp.output)
    # print("".join(map(chr, cmp.output[:-1])))
    # return cmp.output[-1]


@timeme
def part2(_data):
    "Do the part2 calculation"

    map_ = {}

    def affected(x, y):
        cmp = Computer(_data)
        cmp.exec_prog([x, y])
        assert len(cmp.output) == 1
        res = bool(cmp.output[0])
        map_[(x, y)] = "#" if res else "."
        return res

    def test_fit_with_upper_right_at(x, y):
        return affected(x - 99, y) and affected(x - 99, y + 99)

    # walk top of beam
    y = 0
    for x in itertools.count(100):
        # print("A", x,y)
        while not affected(x, y):
            y += 1
            # print(x,y)
        if test_fit_with_upper_right_at(x, y):
            return (x - 99) * 10000 + y

    pass


if __name__ == "__main__":
    main()
