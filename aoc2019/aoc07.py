#!/bin/env python3

import functools
import time

import input_readers


def timeme(method):
    """Decorator that prints the elapsed time after a call"""

    @functools.wraps(method)
    def wrapper(*args, **kw):
        start_time = time.time()
        result = method(*args, **kw)
        end_time = time.time()
        print("timed", wrapper.__name__, end_time - start_time, "s")
        return result

    return wrapper


def main():
    "read input data, process it if necessary and call the part workers"
    data = input_readers.read_input_str()
    data = list(map(int, data.split(",")))
    # data = input_readers.read_input_int_list_multiline()
    # data = input_readers.read_input_str_list()
    # data = input_readers.read_input_table_tab()

    # assert part1('') ==
    assert (
        do1(
            [3, 15, 3, 16, 1002, 16, 10, 16, 1, 16, 15, 15, 4, 15, 99, 0, 0],
            [4, 3, 2, 1, 0],
        )
        == 43210
    )
    assert (
        do1(
            [
                3,
                23,
                3,
                24,
                1002,
                24,
                10,
                24,
                1002,
                23,
                -1,
                23,
                101,
                5,
                23,
                23,
                1,
                24,
                23,
                23,
                4,
                23,
                99,
                0,
                0,
            ],
            [0, 1, 2, 3, 4],
        )
        == 54321
    )
    assert (
        do1(
            [
                3,
                31,
                3,
                32,
                1002,
                32,
                10,
                32,
                1001,
                31,
                -2,
                31,
                1007,
                31,
                0,
                33,
                1002,
                33,
                7,
                33,
                1,
                33,
                31,
                31,
                1,
                32,
                31,
                31,
                4,
                31,
                99,
                0,
                0,
                0,
            ],
            [1, 0, 4, 3, 2],
        )
        == 65210
    )
    print("part1:", part1(data))

    # assert part2('') ==
    assert (
        do2(
            [
                3,
                26,
                1001,
                26,
                -4,
                26,
                3,
                27,
                1002,
                27,
                2,
                27,
                1,
                27,
                26,
                27,
                4,
                27,
                1001,
                28,
                -1,
                28,
                1005,
                28,
                6,
                99,
                0,
                0,
                5,
            ],
            [9, 8, 7, 6, 5],
        )
        == 139629729
    )
    # assert do2([], []) ==
    # assert do2([], []) ==
    print("part2:", part2(data))


class Computer:
    def __init__(self, mem, input_=None):
        self.mem = list(mem)
        self.pos = 0
        self.halt = False
        if not input:
            self.input = []
        else:
            self.input = input_
        self.output = []

    def exec_prog(self, input_=None):
        if input_ is not None:
            self.input = input_
        while not self.halt:
            self.exec_op()
        # print("Halted, output:", self.output)

    def exec_for_input(self, input_=None):
        """pause when input is empty"""
        if input_ is not None:
            self.input = input_
        while not self.halt and self.input:
            self.exec_op()
        # print("Halted, output:", self.output)
        return None  # Halted

    def exec_to_output(self, input_=None):
        """return first output value found and pause"""
        if input_ is not None:
            self.input = input_
        while not self.halt:
            if self.output:
                return self.output.pop(0)
            self.exec_op()
        # print("Halted, output:", self.output)
        return None  # Halted

    def exec_op(self):
        mem = self.mem
        pos = self.pos

        # parse opcode position
        opcode = mem[pos] % 100
        parameter_mode = list(
            reversed(list(map(int, str(mem[pos] // 100))))
        )  # 0 position mode 1 immediate, default is 0
        # print("paramodes", parameter_mode)
        pos += 1  # we have eaten tho opcode

        def get_data():
            nonlocal pos
            val = mem[pos]
            pos += 1  # we have eaten a data field
            if parameter_mode and parameter_mode.pop(0):
                # immediate mode
                # print("im", val)
                return val
            else:
                # print("pm", val, mem[val])
                # position mode
                return mem[val]

        def write_data(data):
            nonlocal pos
            val = mem[pos]
            pos += 1  # we have eaten a data field
            if parameter_mode and parameter_mode.pop(0):
                # immediate mode
                Exception("target parameter in immediate mode")
            else:
                # position mode
                mem[val] = data

        if opcode == 1:
            write_data(get_data() + get_data())
        elif opcode == 2:
            write_data(get_data() * get_data())
        elif opcode == 3:
            # Opcode 3 takes a single integer as input and saves it to the address given by its only parameter.
            # For example, the instruction 3,50 would take an input value and store it at address 50.
            # print("read_input from", self.input)
            write_data(self.input.pop(0))
        elif opcode == 4:
            # Opcode 4 outputs the value of its only parameter. For example, the instruction 4,50 would output the value at address 50.
            val = get_data()
            self.output.append(val)
            # print("output:", val)
        elif opcode == 5:
            # Opcode 5 is jump-if-true: if the first parameter is non-zero,
            # it sets the instruction pointer to the value from the second parameter. Otherwise, it does nothing.
            if get_data():
                pos = get_data()
            else:
                get_data()
        elif opcode == 6:
            # Opcode 6 is jump-if-false: if the first parameter is zero, it sets the instruction
            # pointer to the value from the second parameter. Otherwise, it does nothing.
            if not get_data():
                pos = get_data()
            else:
                get_data()
        elif opcode == 7:
            # Opcode 7 is less than: if the first parameter is less than the second parameter,
            # it stores 1 in the position given by the third parameter. Otherwise, it stores 0.
            if get_data() < get_data():
                write_data(1)
            else:
                write_data(0)
        elif opcode == 8:
            # Opcode 8 is equals: if the first parameter is equal to the second parameter,
            # it stores 1 in the position given by the third parameter. Otherwise, it stores 0.
            if get_data() == get_data():
                write_data(1)
            else:
                write_data(0)
        elif opcode == 99:
            self.halt = True
        else:
            raise Exception("Unknown opcode", opcode)

        self.pos = pos


@timeme
def part1(_data):
    "Do the part1 calculation"
    signals = []
    import itertools

    for phases in itertools.permutations(range(5), 5):
        signal = do1(_data, phases)
        signals.append(signal)

    return max(signals)


def do1(_data, phases):
    signal = 0
    for ampnum, _amp_name in enumerate("ABCDE"):
        cmp = Computer(_data, input_=[phases[ampnum], signal])
        cmp.exec_prog()
        # print(phases[ampnum], signal, "->", cmp.output)
        signal = cmp.output[0]
        # print(signal)
    return signal


@timeme
def part2(_data):
    "Do the part2 calculation"
    signals = []
    import itertools

    for phases in itertools.permutations(range(5, 10)):
        signal = do2(_data, phases)
        signals.append(signal)

    return max(signals)


def do2(_data, phases):
    signal = 0
    amps = []
    for ampnum, _amp_name in enumerate("ABCDE"):
        amp = Computer(_data)
        amp.exec_for_input([phases[ampnum]])
        amps.append(amp)

    while 1:  # until amp1 halts
        for _ampnum, amp in enumerate(amps):
            # print("ampnum", _ampnum, signal)
            res = amp.exec_to_output([signal])
            if res is None:  # computer halted without output
                return signal
            else:
                # print("->", res)
                signal = res

            # print(phases[ampnum], signal, "->", cmp.output)
            # print(signal)
    return signal


if __name__ == "__main__":
    main()
