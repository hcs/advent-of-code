#!/bin/env python3

import functools
import time

import input_readers


def timeme(method):
    """Decorator that prints the elapsed time after a call"""

    @functools.wraps(method)
    def wrapper(*args, **kw):
        start_time = time.time()
        result = method(*args, **kw)
        end_time = time.time()
        print("timed", wrapper.__name__, end_time - start_time, "s")
        return result

    return wrapper


def main():
    "read input data, process it if necessary and call the part workers"
    # data = input_readers.read_input_str()
    # data = input_readers.read_input_int_list_multiline()
    data = input_readers.read_input_str_list()
    # data = input_readers.read_input_table_tab()
    data = [line.split(")") for line in data]  # (a,b) b orbits a
    assert (
        part1(
            [
                ("COM", "B"),
                ("B", "C"),
                ("C", "D"),
                ("D", "E"),
                ("E", "F"),
                ("B", "G"),
                ("G", "H"),
                ("D", "I"),
                ("E", "J"),
                ("J", "K"),
                ("K", "L"),
            ]
        )
        == 42
    )
    print("part1:", part1(data))

    # assert part2('') ==
    assert (
        part2(
            [
                ("COM", "B"),
                ("B", "C"),
                ("C", "D"),
                ("D", "E"),
                ("E", "F"),
                ("B", "G"),
                ("G", "H"),
                ("D", "I"),
                ("E", "J"),
                ("J", "K"),
                ("K", "L"),
                ("K", "YOU"),
                ("I", "SAN"),
            ]
        )
        == 4
    )
    print("part2:", part2(data))


@timeme
def part1(_data):
    "Do the part1 calculation"
    # print(_data)
    data = {}  # parent: [children]
    for parent, child in _data:
        data.setdefault(parent, []).append(child)
    # print(data)

    def walk(parent="COM", level=0):
        osum = level
        for child in data.get(parent, []):
            osum += walk(child, level + 1)
        return osum

    return walk()


@timeme
def part2(_data):
    "Do the part2 calculation"
    data = {}  # parent: [children]
    for parent, child in _data:
        data.setdefault(parent, []).append(child)
    com_san = None
    com_you = None

    def walk(parent="COM", ppath=()):
        nonlocal com_san
        nonlocal com_you
        if com_san and com_you:
            return
        for child in data.get(parent, []):
            path = ppath + (child,)
            if child == "SAN":
                com_san = path
            if child == "YOU":
                com_you = path
            walk(child, path)

    walk()
    # print("com_san", com_san)
    # print("com_you", com_you)
    import itertools

    common_orbits = len(
        list(itertools.takewhile(lambda a: a[0] == a[1], zip(com_san, com_you)))
    )
    orbs = len(com_san) - common_orbits + len(com_you) - common_orbits - 2
    return orbs


if __name__ == "__main__":
    main()
