#!/bin/env python3

import functools
import time

# import input_readers


def timeme(method):
    """Decorator that prints the elapsed time after a call"""

    @functools.wraps(method)
    def wrapper(*args, **kw):
        start_time = time.time()
        result = method(*args, **kw)
        end_time = time.time()
        print("timed", wrapper.__name__, end_time - start_time, "s")
        return result

    return wrapper


def main():
    "read input data, process it if necessary and call the part workers"
    # data = input_readers.read_input_str()
    # data = input_readers.read_input_int_list_multiline()
    # data = input_readers.read_input_str_list()
    # data = input_readers.read_input_table_tab()
    data = 172930, 683082

    # assert part1('') ==
    print("part1:", part1(data))

    # assert part2('') ==
    print("part2:", part2(data))


@timeme
def part1(_data):
    "Do the part1 calculation"
    return len(list(enum_passwords(*_data)))


def enum_passwords(min_, max_):
    for passw in range(min_, max_ + 1):
        dig = list(str(passw))
        sdig = sorted(dig)
        if dig != sdig:  # failed never decreses rule
            continue
        repeats = 0
        last = None
        for d in dig:
            if d == last:
                repeats += 1
                break  # only need to find one
            last = d
        if not repeats:
            continue
        # print(passw)
        yield passw


@timeme
def part2(_data):
    "Do the part2 calculation"
    return len(list(enum_passwords2(*_data)))


def enum_passwords2(min_, max_):
    for passw in range(min_, max_ + 1):
        dig = list(str(passw))
        sdig = sorted(dig)
        if dig != sdig:  # failed never decreses rule
            continue
        repeats = 0
        last = None
        found_dubble = False
        for d in dig:
            if d == last:
                repeats += 1
                # break  # only need to find one
            else:  # found other char
                if repeats == 1:
                    found_dubble = True
                repeats = 0
            last = d
        if found_dubble or repeats == 1:
            # print(passw)
            yield passw


if __name__ == "__main__":
    main()
