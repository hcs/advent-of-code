#!/bin/env python3

import collections
import copy
import functools
import itertools
import math
import time

# import input_readers


def timeme(method):
    """Decorator that prints the elapsed time after a call"""

    @functools.wraps(method)
    def wrapper(*args, **kw):
        start_time = time.time()
        result = method(*args, **kw)
        end_time = time.time()
        print("timed", wrapper.__name__, end_time - start_time, "s")
        return result

    return wrapper


def main():
    "read input data, process it if necessary and call the part workers"
    # data = input_readers.read_input_str()
    # data = input_readers.read_input_int_list_multiline()
    # data = input_readers.read_input_str_list()
    # data = input_readers.read_input_table_tab()
    data = [  # moons_pos
        [3, 15, 8],
        [5, -1, -2],
        [-10, 8, 2],
        [8, 4, -5],
    ]
    testdata_1 = [
        [-1, 0, 2],
        [2, -10, -7],
        [4, -8, 8],
        [3, 5, -1],
    ]
    assert part1(testdata_1, 10) == 179
    print("part1:", part1(data, 1000))

    assert part2_t1(testdata_1) == 2772

    testdata_2 = [
        [-8, -10, 0],
        [5, 5, 10],
        [2, -7, 3],
        [9, -8, -3],
    ]

    assert part2(testdata_1) == 2772
    assert part2(testdata_2) == 4686774924
    print("GOT IT")
    print("part2:", part2(data))


@timeme
def part1(_data, reps):
    "Do the part1 calculation"
    moons = (0, 1, 2, 3)
    moons_pos = copy.deepcopy(_data)  # [[x,y,z],..]
    moons_vel = [
        [0, 0, 0],
        [0, 0, 0],
        [0, 0, 0],
        [0, 0, 0],
    ]  # [[dx,dy,dz],..]

    def update_vel():
        for m1, m2 in itertools.combinations(moons, r=2):
            for vector in [0, 1, 2]:
                m1p = moons_pos[m1][vector]
                m2p = moons_pos[m2][vector]
                if m1p < m2p:
                    moons_vel[m1][vector] += 1
                    moons_vel[m2][vector] -= 1
                elif m1p > m2p:
                    moons_vel[m1][vector] -= 1
                    moons_vel[m2][vector] += 1

    def update_pos():
        for m in moons:
            for vector in [0, 1, 2]:
                moons_pos[m][vector] += moons_vel[m][vector]

    def total_energy():
        res = 0
        for m in moons:
            potential_energy = sum(
                (abs(moons_pos[m][0]), abs(moons_pos[m][1]), abs(moons_pos[m][2]),)
            )
            kinetic_energy = sum(
                (abs(moons_vel[m][0]), abs(moons_vel[m][1]), abs(moons_vel[m][2]),)
            )
            res += potential_energy * kinetic_energy
        print(res)
        return res

    def print_state():
        for m in moons:
            print(moons_pos[m], moons_vel[m])

    print_state()
    for _i in range(1, reps + 1):
        update_vel()
        update_pos()
        # print(i)
        # print_state()
    return total_energy()


Moon = collections.namedtuple("Moon", "x y z dx dy dz")


@timeme
def part2_t1(_data):
    "Do the part2 calculation"
    moons = (0, 1, 2, 3)
    moons_pos = copy.deepcopy(_data)  # [[x,y,z],..]
    moons_vel = [
        [0, 0, 0],
        [0, 0, 0],
        [0, 0, 0],
        [0, 0, 0],
    ]  # [[dx,dy,dz],..]
    history = {}  # state: iter

    def update_vel():
        for m1, m2 in itertools.combinations(moons, r=2):
            for vector in [0, 1, 2]:
                m1p = moons_pos[m1][vector]
                m2p = moons_pos[m2][vector]
                if m1p < m2p:
                    moons_vel[m1][vector] += 1
                    moons_vel[m2][vector] -= 1
                elif m1p > m2p:
                    moons_vel[m1][vector] -= 1
                    moons_vel[m2][vector] += 1

    def update_pos():
        for m in moons:
            for vector in [0, 1, 2]:
                moons_pos[m][vector] += moons_vel[m][vector]

    def total_energy():
        res = 0
        for m in moons:
            potential_energy = sum(
                (abs(moons_pos[m][0]), abs(moons_pos[m][1]), abs(moons_pos[m][2]),)
            )
            kinetic_energy = sum(
                (abs(moons_vel[m][0]), abs(moons_vel[m][1]), abs(moons_vel[m][2]),)
            )
            res += potential_energy * kinetic_energy
        print(res)
        return res

    def immutable_state():
        return (
            Moon(*moons_pos[0], *moons_vel[0]),
            Moon(*moons_pos[1], *moons_vel[0]),
            Moon(*moons_pos[2], *moons_vel[0]),
            Moon(*moons_pos[3], *moons_vel[0]),
        )

    def print_state():
        for m in moons:
            print(moons_pos[m], moons_vel[m])

    # print_state()
    state = immutable_state()
    print(0, state)
    history[state] = 0

    for _i in itertools.count(1):
        update_vel()
        update_pos()
        # print(i)
        # print_state()
        state = immutable_state()
        if state in history:
            print(_i)
            return _i
        # print(0, state)
        history[state] = _i
    # return total_energy()


@timeme
def part2(_data):
    "Do the part2 calculation"
    moons = (0, 1, 2, 3)
    pairs = tuple(itertools.combinations(moons, r=2))
    vectors = (0, 1, 2)
    moons_pos = copy.deepcopy(_data)  # [[x,y,z],..]

    def find_repeat(positions):
        """takes a (int,int,int,int)"""
        positions = list(positions)
        velocities = [0, 0, 0, 0]
        history = {}  # state: iter
        history[tuple(positions + velocities)] = 0
        for _i in itertools.count(1):
            # update velocities
            for m1, m2 in pairs:
                m1p = positions[m1]
                m2p = positions[m2]
                if m1p < m2p:
                    velocities[m1] += 1
                    velocities[m2] -= 1
                elif m1p > m2p:
                    velocities[m1] -= 1
                    velocities[m2] += 1
            # update positions
            for m in moons:
                positions[m] += velocities[m]
            state = tuple(positions + velocities)
            if state in history:
                print("found repeat", _i, history[state], state)
                return _i
            history[state] = _i

    def lcm(a, b):
        """ LCM(a,b) = (a×b)/GCF(a,b) https://www.calculatorsoup.com/calculators/math/lcm.php"""
        return int((a * b) / math.gcd(a, b))

    vector_repeats_at = []
    for vector in vectors:
        positions = [moons_pos[m][vector] for m in moons]
        vector_repeats_at.append(find_repeat(positions))

    print(vector_repeats_at)
    res = lcm(lcm(*vector_repeats_at[:2]), vector_repeats_at[2])
    print(res)
    return res


if __name__ == "__main__":
    main()
