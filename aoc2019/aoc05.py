#!/bin/env python3

import functools
import time

import input_readers


def timeme(method):
    """Decorator that prints the elapsed time after a call"""

    @functools.wraps(method)
    def wrapper(*args, **kw):
        start_time = time.time()
        result = method(*args, **kw)
        end_time = time.time()
        print("timed", wrapper.__name__, end_time - start_time, "s")
        return result

    return wrapper


def main():
    "read input data, process it if necessary and call the part workers"
    data = input_readers.read_input_str()
    data = list(map(int, data.split(",")))
    # data = input_readers.read_input_int_list_multiline()
    # data = input_readers.read_input_str_list()
    # data = input_readers.read_input_table_tab()

    # assert part1('') ==
    assert exec1([1, 0, 0, 0, 99]) == [2, 0, 0, 0, 99]
    assert exec1([2, 3, 0, 3, 99]) == [2, 3, 0, 6, 99]
    assert exec1([2, 4, 4, 5, 99, 0]) == [2, 4, 4, 5, 99, 9801]
    assert exec1([1, 1, 1, 4, 99, 5, 6, 0, 99]) == [30, 1, 1, 4, 2, 5, 6, 0, 99]
    assert part1([3, 0, 4, 0, 99], 42) == [42]
    print("part1:", part1(data, 1))

    # assert part2('') ==
    print("part2:", part2(data, 5))


@timeme
def part1(_data, input_):
    "Do the part1 calculation"
    # print(_data)
    cmp = Computer(_data, input_=[input_])
    cmp.exec_prog()
    return cmp.output


def exec1(data):
    # This is to use aoc2p1 tests for the Computer
    cmp = Computer(data, input_=[])
    cmp.exec_prog()
    # print(cmp.mem)
    return cmp.mem


class Computer:
    def __init__(self, mem, input_):
        self.mem = list(mem)
        self.pos = 0
        self.halt = False
        self.input = input_
        self.output = []

    def exec_prog(self):
        while not self.halt:
            self.exec_op()
        # print("Halted, output:", self.output)

    def exec_op(self):
        mem = self.mem
        pos = self.pos

        # parse opcode position
        opcode = mem[pos] % 100
        parameter_mode = list(
            reversed(list(map(int, str(mem[pos] // 100))))
        )  # 0 position mode 1 immediate, default is 0
        # print("paramodes", parameter_mode)
        pos += 1  # we have eaten tho opcode

        def get_data():
            nonlocal pos
            val = mem[pos]
            pos += 1  # we have eaten a data field
            if parameter_mode and parameter_mode.pop(0):
                # immediate mode
                # print("im", val)
                return val
            else:
                # print("pm", val, mem[val])
                # position mode
                return mem[val]

        def write_data(data):
            nonlocal pos
            val = mem[pos]
            pos += 1  # we have eaten a data field
            if parameter_mode and parameter_mode.pop(0):
                # immediate mode
                Exception("target parameter in immediate mode")
            else:
                # position mode
                mem[val] = data

        if opcode == 1:
            write_data(get_data() + get_data())
        elif opcode == 2:
            write_data(get_data() * get_data())
        elif opcode == 3:
            # Opcode 3 takes a single integer as input and saves it to the address given by its only parameter.
            # For example, the instruction 3,50 would take an input value and store it at address 50.
            write_data(self.input.pop(0))
        elif opcode == 4:
            # Opcode 4 outputs the value of its only parameter. For example, the instruction 4,50 would output the value at address 50.
            val = get_data()
            self.output.append(val)
            # print("output:", val)
        elif opcode == 5:
            # Opcode 5 is jump-if-true: if the first parameter is non-zero,
            # it sets the instruction pointer to the value from the second parameter. Otherwise, it does nothing.
            if get_data():
                pos = get_data()
            else:
                get_data()
        elif opcode == 6:
            # Opcode 6 is jump-if-false: if the first parameter is zero,
            # it sets the instruction pointer to the value from the second parameter. Otherwise, it does nothing.
            if not get_data():
                pos = get_data()
            else:
                get_data()
        elif opcode == 7:
            # Opcode 7 is less than: if the first parameter is less than the second parameter,
            # it stores 1 in the position given by the third parameter. Otherwise, it stores 0.
            if get_data() < get_data():
                write_data(1)
            else:
                write_data(0)
        elif opcode == 8:
            # Opcode 8 is equals: if the first parameter is equal to the second parameter,
            # it stores 1 in the position given by the third parameter. Otherwise, it stores 0.
            if get_data() == get_data():
                write_data(1)
            else:
                write_data(0)
        elif opcode == 99:
            self.halt = True
        else:
            raise Exception("Unknown opcode", opcode)

        self.pos = pos


@timeme
def part2(_data, input_):
    "Do the part2 calculation"
    cmp = Computer(_data, input_=[input_])
    cmp.exec_prog()
    return cmp.output


if __name__ == "__main__":
    main()
