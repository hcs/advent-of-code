#!/bin/env python3
""" --- Day 13: Care Package ---

As you ponder the solitude of space and the ever-increasing three-hour roundtrip for messages between you and Earth, you
notice that the Space Mail Indicator Light is blinking. To help keep you sane, the Elves have sent you a care package.

It's a new game for the ship's arcade cabinet! Unfortunately, the arcade is all the way on the other end of the ship.
Surely, it won't be hard to build your own - the care package even comes with schematics.

The arcade cabinet runs Intcode software like the game the Elves sent (your puzzle input). It has a primitive screen
capable of drawing square tiles on a grid. The software draws tiles to the screen with output instructions: every three
output instructions specify the x position (distance from the left), y position (distance from the top), and tile id.
The tile id is interpreted as follows:

    0 is an empty tile. No game object appears in this tile.  1 is a wall tile. Walls are indestructible barriers.  2 is
    a block tile. Blocks can be broken by the ball.  3 is a horizontal paddle tile. The paddle is indestructible.  4 is
    a ball tile. The ball moves diagonally and bounces off objects.

For example, a sequence of output values like 1,2,3,6,5,4 would draw a horizontal paddle tile (1 tile from the left and
2 tiles from the top) and a ball tile (6 tiles from the left and 5 tiles from the top).

Start the game. How many block tiles are on the screen when the game exits?  """

import collections
import functools
import time

import input_readers
import utils


def timeme(method):
    """Decorator that prints the elapsed time after a call"""

    @functools.wraps(method)
    def wrapper(*args, **kw):
        start_time = time.time()
        result = method(*args, **kw)
        end_time = time.time()
        print("timed", wrapper.__name__, end_time - start_time, "s")
        return result

    return wrapper


def main():
    "read input data, process it if necessary and call the part workers"
    data = input_readers.read_input_str()
    data = list(map(int, data.split(",")))
    # data = input_readers.read_input_int_list_multiline()
    # data = input_readers.read_input_str_list()
    # data = input_readers.read_input_table_tab()

    # assert part1('') ==
    print("part1:", part1(data))

    # assert part2('') ==
    print("part2:", part2(data))


class Computer:
    def __init__(self, mem, input_=None):
        # self.mem = list(mem)

        self.mem = collections.defaultdict(lambda: 0, enumerate(mem))

        self.pos = 0
        self.halt = False
        self.relative_base = 0
        if not input:
            self.input = []
        else:
            self.input = input_
        self.input_callback = None  # function to call instead of using self.input
        self.output = []

    def exec_prog(self, input_=None):
        if input_ is not None:
            self.input = input_
        while not self.halt:
            self.exec_op()
        # print("Halted, output:", self.output)

    def exec_for_input(self, input_=None):
        """pause when input is empty"""
        if input_ is not None:
            self.input = input_
        while not self.halt and self.input:
            self.exec_op()
        # print("Halted, output:", self.output)
        return None  # Halted

    def exec_to_output(self, input_=None):
        """return first output value found and pause"""
        if input_ is not None:
            self.input = input_
        while not self.halt:
            if self.output:
                return self.output.pop(0)
            self.exec_op()
        # print("Halted, output:", self.output)
        return None  # Halted

    def exec_op(self):
        mem = self.mem
        pos = self.pos

        # parse opcode position
        opcode = mem[pos] % 100
        parameter_mode = list(
            reversed(list(map(int, str(mem[pos] // 100))))
        )  # 0 position mode, 1 immediate, 2 relative, default is 0
        # print("paramodes", parameter_mode)
        pos += 1  # we have eaten tho opcode

        def get_data():
            nonlocal pos
            val = mem[pos]
            pos += 1  # we have eaten a data field
            pm = parameter_mode.pop(0) if parameter_mode else 0

            if pm == 0:
                # print("pm", val, mem[val])
                # position mode
                return mem[val]
            elif pm == 1:
                # immediate mode
                # print("im", val)
                return val
            elif pm == 2:
                # relative mode
                return mem[self.relative_base + val]
            else:
                raise Exception("Unknown parameter_mode", pm)

        def write_data(data):
            nonlocal pos
            val = mem[pos]
            pos += 1  # we have eaten a data field
            pm = parameter_mode.pop(0) if parameter_mode else 0

            if pm == 0:
                # print("pm", val, mem[val])
                # position mode
                mem[val] = data
            elif pm == 1:
                # immediate mode
                Exception("target parameter in immediate mode")
            elif pm == 2:
                # relative mode
                mem[self.relative_base + val] = data
            else:
                raise Exception("Unknown parameter_mode", pm)

        if opcode == 1:
            write_data(get_data() + get_data())
        elif opcode == 2:
            write_data(get_data() * get_data())
        elif opcode == 3:
            # Opcode 3 takes a single integer as input and saves it to the address given by its only parameter.
            # For example, the instruction 3,50 would take an input value and store it at address 50.
            # print("read_input from", self.input)
            if self.input_callback:
                write_data(self.input_callback())
            else:
                write_data(self.input.pop(0))
        elif opcode == 4:
            # Opcode 4 outputs the value of its only parameter. For example, the instruction 4,50 would output the value at address 50.
            val = get_data()
            self.output.append(val)
            # print("output:", val)
        elif opcode == 5:
            # Opcode 5 is jump-if-true: if the first parameter is non-zero,
            # it sets the instruction pointer to the value from the second parameter. Otherwise, it does nothing.
            if get_data():
                pos = get_data()
            else:
                get_data()
        elif opcode == 6:
            # Opcode 6 is jump-if-false: if the first parameter is zero, it sets the instruction
            # pointer to the value from the second parameter. Otherwise, it does nothing.
            if not get_data():
                pos = get_data()
            else:
                get_data()
        elif opcode == 7:
            # Opcode 7 is less than: if the first parameter is less than the second parameter,
            # it stores 1 in the position given by the third parameter. Otherwise, it stores 0.
            if get_data() < get_data():
                write_data(1)
            else:
                write_data(0)
        elif opcode == 8:
            # Opcode 8 is equals: if the first parameter is equal to the second parameter,
            # it stores 1 in the position given by the third parameter. Otherwise, it stores 0.
            if get_data() == get_data():
                write_data(1)
            else:
                write_data(0)
        elif opcode == 9:
            # Opcode 9 adjusts the relative base by the value of its only parameter.
            # The relative base increases (or decreases, if the value is negative) by the value of the parameter.
            self.relative_base += get_data()

        elif opcode == 99:
            self.halt = True
        else:
            raise Exception("Unknown opcode", opcode)

        self.pos = pos


@timeme
def part1(_data):
    "Do the part1 calculation"
    cmp = Computer(_data)

    board = {}
    while True:
        x = cmp.exec_to_output()
        y = cmp.exec_to_output()
        tile_id = cmp.exec_to_output()
        if cmp.halt:
            break
        board[(x, y)] = tile_id
    utils.print_board(board)
    return collections.Counter(board.values())[2]
    # tile_ids
    # 0 is an empty tile. No game object appears in this tile.
    # 1 is a wall tile. Walls are indestructible barriers.
    # 2 is a block tile. Blocks can be broken by the ball.
    # 3 is a horizontal paddle tile. The paddle is indestructible.
    # 4 is a ball tile. The ball moves diagonally and bounces off objects.


@timeme
def part2(_data):
    "Do the part2 calculation"
    _data = list(_data)
    _data[0] = 2  # play for free
    score = 0
    cmp = Computer(_data)
    board = {}
    ball_x = 0  # ball_y = ball_dx = ball_dy = 0

    def input_callback():
        print("===================================================", score)
        utils.print_board(
            board,
            get_char=lambda board, pos: {0: " ", 1: "|", 2: "#", 3: "-", 4: "*"}[
                board.get(pos, 0)
            ],
        )

        paddle_dx = ball_x - paddle_x
        return paddle_dx

    cmp.input_callback = input_callback

    # while collections.Counter(board.values())[2] > 0:  # while there is tile blocks
    while True:
        x = cmp.exec_to_output()
        y = cmp.exec_to_output()
        tile_id = cmp.exec_to_output()
        if cmp.halt:
            break
        if x == -1 and y == 0:
            score = tile_id
        else:
            board[(x, y)] = tile_id
            if tile_id == 3:  # paddle
                paddle_x = x
                # paddle_y = y  # constant
            elif tile_id == 4:  # ball
                # ball_dx = x - ball_x  # ball direction
                # ball_dy = y - ball_y  # ball direction
                ball_x = x
                # ball_y = y
    return score

    # print("===================================================", score)
    # utils.print_board(
    #     board,
    #     get_char=lambda board, pos: {0: " ", 1: "|", 2: "#", 3: "-", 4: "*"}[
    #         board.get(pos, 0)
    #     ],
    # )
    # hull, get_char = lambda board, pos: "#" if board.get(pos, 0) else " "
    # tile_ids
    # 0 is an empty tile. No game object appears in this tile.
    # 1 is a wall tile. Walls are indestructible barriers.
    # 2 is a block tile. Blocks can be broken by the ball.
    # 3 is a horizontal paddle tile. The paddle is indestructible.
    # 4 is a ball tile. The ball moves diagonally and bounces off objects.


if __name__ == "__main__":
    main()
