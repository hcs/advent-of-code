#!/bin/env python3

import collections
import functools
import time

import input_readers
import utils


def timeme(method):
    """Decorator that prints the elapsed time after a call"""

    @functools.wraps(method)
    def wrapper(*args, **kw):
        start_time = time.time()
        result = method(*args, **kw)
        end_time = time.time()
        print("timed", wrapper.__name__, end_time - start_time, "s")
        return result

    return wrapper


def main():
    "read input data, process it if necessary and call the part workers"
    data = input_readers.read_input_str()
    data = list(map(int, data.split(",")))
    # data = input_readers.read_input_int_list_multiline()
    # data = input_readers.read_input_str_list()
    # data = input_readers.read_input_table_tab()

    # assert part1('') ==
    print("part1:", part1(data))

    # assert part2('') ==
    print("part2:", part2(data))


class Computer:
    def __init__(self, mem, input_=None):
        # self.mem = list(mem)

        self.mem = collections.defaultdict(lambda: 0, enumerate(mem))

        self.pos = 0
        self.halt = False
        self.relative_base = 0
        if not input:
            self.input = []
        else:
            self.input = input_
        self.input_callback = None  # function to call instead of using self.input
        self.output = []

    def exec_prog(self, input_=None):
        if input_ is not None:
            self.input = input_
        while not self.halt:
            self.exec_op()
        # print("Halted, output:", self.output)

    def exec_for_input(self, input_=None):
        """pause when input is empty"""
        if input_ is not None:
            self.input = input_
        while not self.halt and self.input:
            self.exec_op()
        # print("Halted, output:", self.output)
        return None  # Halted

    def exec_to_output(self, input_=None):
        """return first output value found and pause"""
        if input_ is not None:
            self.input = input_
        while not self.halt:
            if self.output:
                return self.output.pop(0)
            self.exec_op()
        # print("Halted, output:", self.output)
        return None  # Halted

    def exec_op(self):
        mem = self.mem
        pos = self.pos

        # parse opcode position
        opcode = mem[pos] % 100
        parameter_mode = list(
            reversed(list(map(int, str(mem[pos] // 100))))
        )  # 0 position mode, 1 immediate, 2 relative, default is 0
        # print("paramodes", parameter_mode)
        pos += 1  # we have eaten tho opcode

        def get_data():
            nonlocal pos
            val = mem[pos]
            pos += 1  # we have eaten a data field
            pm = parameter_mode.pop(0) if parameter_mode else 0

            if pm == 0:
                # print("pm", val, mem[val])
                # position mode
                return mem[val]
            elif pm == 1:
                # immediate mode
                # print("im", val)
                return val
            elif pm == 2:
                # relative mode
                return mem[self.relative_base + val]
            else:
                raise Exception("Unknown parameter_mode", pm)

        def write_data(data):
            nonlocal pos
            val = mem[pos]
            pos += 1  # we have eaten a data field
            pm = parameter_mode.pop(0) if parameter_mode else 0

            if pm == 0:
                # print("pm", val, mem[val])
                # position mode
                mem[val] = data
            elif pm == 1:
                # immediate mode
                Exception("target parameter in immediate mode")
            elif pm == 2:
                # relative mode
                mem[self.relative_base + val] = data
            else:
                raise Exception("Unknown parameter_mode", pm)

        if opcode == 1:
            write_data(get_data() + get_data())
        elif opcode == 2:
            write_data(get_data() * get_data())
        elif opcode == 3:
            # Opcode 3 takes a single integer as input and saves it to the address given by its only parameter.
            # For example, the instruction 3,50 would take an input value and store it at address 50.
            # print("read_input from", self.input)
            if self.input_callback:
                write_data(self.input_callback())
            else:
                write_data(self.input.pop(0))
        elif opcode == 4:
            # Opcode 4 outputs the value of its only parameter. For example, the instruction 4,50 would output the value at address 50.
            val = get_data()
            self.output.append(val)
            # print("output:", val)
        elif opcode == 5:
            # Opcode 5 is jump-if-true: if the first parameter is non-zero,
            # it sets the instruction pointer to the value from the second parameter. Otherwise, it does nothing.
            if get_data():
                pos = get_data()
            else:
                get_data()
        elif opcode == 6:
            # Opcode 6 is jump-if-false: if the first parameter is zero, it sets the instruction
            # pointer to the value from the second parameter. Otherwise, it does nothing.
            if not get_data():
                pos = get_data()
            else:
                get_data()
        elif opcode == 7:
            # Opcode 7 is less than: if the first parameter is less than the second parameter,
            # it stores 1 in the position given by the third parameter. Otherwise, it stores 0.
            if get_data() < get_data():
                write_data(1)
            else:
                write_data(0)
        elif opcode == 8:
            # Opcode 8 is equals: if the first parameter is equal to the second parameter,
            # it stores 1 in the position given by the third parameter. Otherwise, it stores 0.
            if get_data() == get_data():
                write_data(1)
            else:
                write_data(0)
        elif opcode == 9:
            # Opcode 9 adjusts the relative base by the value of its only parameter.
            # The relative base increases (or decreases, if the value is negative) by the value of the parameter.
            self.relative_base += get_data()

        elif opcode == 99:
            self.halt = True
        else:
            raise Exception("Unknown opcode", opcode)

        self.pos = pos


def get_hull(_data):
    cmp = Computer(_data)
    cmp.exec_prog()
    hull = collections.defaultdict(lambda: ".")
    board_str = "".join(map(chr, cmp.output))
    for lineno, line in enumerate(board_str.splitlines()):
        for col, val in enumerate(line):
            hull[(col + lineno * 1j)] = val
    return hull


@timeme
def part1(_data):
    "Do the part1 calculation"

    hull = get_hull(_data)

    utils.print_cboard(hull)

    alignment_parameter_sum = 0
    for pos, val in list(hull.items()):
        if val != "#":
            continue
        intersection = True
        for step in (-1, 1, 1j, -1j):
            pos2 = pos + step
            if hull[pos2] != "#":
                intersection = False
                break
        if intersection:
            # hull[pos] = "O"
            alignment_parameter_sum += pos.real * pos.imag

    return alignment_parameter_sum
    utils.print_cboard(hull)


def turn_right(dir_):
    dirs = (1, 1j, -1, -1j)
    return dirs[(dirs.index(dir_) + 1) % 4]


# def turn_left(dir_):
#     return -turn_right(dir_)


def calc_path(hull):
    start_pos = {v: k for k, v in hull.items()}["^"]
    pos = start_pos
    path = ["L"]  # to get in line with line
    dir_ = -1
    steps = 0

    while True:
        if hull[pos + dir_] == "#":
            # go forward to the end
            steps += 1
            pos = pos + dir_
            continue
        path.append(steps)
        steps = 0
        dir_right = turn_right(dir_)
        dir_left = -dir_right
        if hull[pos + dir_right] == "#":
            # pos = pos + dir_right
            dir_ = dir_right
            path.append("R")
        elif hull[pos + dir_left] == "#":
            # pos = pos + dir_left
            dir_ = dir_left
            path.append("L")
        else:
            return list(map(str, path))


@timeme
def part2(_data):
    "Do the part2 calculation"

    hull = get_hull(_data)
    utils.print_cboard(hull, axis=True)

    assert turn_right(1) == 1j

    path = calc_path(hull)
    path = ",".join(path)
    print("path", path)
    # L,10,R,12,R,12,R,6,R,10,L,10,L,10,R,12,R,12,R,10,L,10,L,12,R,6,R,6,R,10,L,10,R,10,L,10,L,12,R,6,R,6,R,10,L,10,R,10,L,10,L,12,R,6,L,10,R,12,R,12,R,10,L,10,L,12,R,6
    """
    A    L,10,R,12,R,12,
    B    R,6,R,10,L,10,
    A    L,10,R,12,R,12,
    C    R,10,L,10,L,12,R,6,
    B    R,6,R,10,L,10,
    C    R,10,L,10,L,12,R,6,
    B    R,6,R,10,L,10,
    C    R,10,L,10,L,12,R,6,
    A    L,10,R,12,R,12,
    C    R,10,L,10,L,12,R,6

    """
    main_ = "A,B,A,C,B,C,B,C,A,C\n"
    a = "L,10,R,12,R,12\n"
    b = "R,6,R,10,L,10\n"
    c = "R,10,L,10,L,12,R,6\n"

    robot_input = list(map(ord, main_ + a + b + c + "n\n"))

    # routines = []
    # while path:
    #     if len(path) <= 20:
    #         routines.append(path)
    #         break
    #     split_at = path.rindex(",", 0, 20)
    #     routines.append(path[0:split_at])
    #     path = path[split_at+1:]
    # print(routines)

    _data[0] = 2  # wake robot up

    cmp = Computer(_data)
    cmp.exec_prog(robot_input)
    # print(cmp.output)
    return cmp.output[-1]

    pass
    # cmp = Computer(_data)


if __name__ == "__main__":
    main()
