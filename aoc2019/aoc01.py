#!/bin/env python3

import functools
import time

import input_readers


def timeme(method):
    """Decorator that prints the elapsed time after a call"""

    @functools.wraps(method)
    def wrapper(*args, **kw):
        start_time = time.time()
        result = method(*args, **kw)
        end_time = time.time()
        print("timed", wrapper.__name__, end_time - start_time, "s")
        return result

    return wrapper


def main():
    "read input data, process it if necessary and call the part workers"
    # data = input_readers.read_input_str()
    data = input_readers.read_input_int_list_multiline()
    # data = input_readers.read_input_str_list()
    # data = input_readers.read_input_table_tab()

    assert fuel_req(12) == 2
    assert fuel_req(100756) == 33583
    print("part1:", part1(data))

    # assert part2('') ==
    assert part2((14,)) == 2
    assert part2((100756,)) == 50346
    print("part2:", part2(data))


def fuel_req(mass):
    return mass // 3 - 2


@timeme
def part1(_data):
    "Do the part1 calculation"
    return sum(map(fuel_req, _data))


def fuel_req_rec(mass):
    fuel_tot = 0
    while mass >= 0:
        mass = fuel_req(mass)
        if mass > 0:
            fuel_tot += mass
    return fuel_tot


@timeme
def part2(_data):
    "Do the part2 calculation"
    return sum(map(fuel_req_rec, _data))


if __name__ == "__main__":
    main()
