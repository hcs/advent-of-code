#!/bin/env python3

import collections
import functools
import time

import input_readers
import utils


def timeme(method):
    """Decorator that prints the elapsed time after a call"""

    @functools.wraps(method)
    def wrapper(*args, **kw):
        start_time = time.time()
        result = method(*args, **kw)
        end_time = time.time()
        print("timed", wrapper.__name__, end_time - start_time, "s")
        return result

    return wrapper


def main():
    "read input data, process it if necessary and call the part workers"
    data = input_readers.read_input_str()
    data = list(map(int, data.split(",")))
    # data = input_readers.read_input_int_list_multiline()
    # data = input_readers.read_input_str_list()
    # data = input_readers.read_input_table_tab()

    # assert part1('') ==
    print("part1:", part1(data))

    # assert part2('') ==
    print("part2:", part2(data))


class Computer:
    def __init__(
        self,
        mem,
        input_=None,
        input_callback=None,
        output_callback=None,
        default_input=None,
    ):
        # self.mem = list(mem)

        self.mem = collections.defaultdict(lambda: 0, enumerate(mem))

        self.pos = 0
        self.halt = False
        self.relative_base = 0
        self.input = []
        if input_:
            self.input.extend(input_)
        # function to call instead of using self.input
        self.input_callback = input_callback
        # function to call instead of using self.output
        self.output_callback = output_callback
        self.output = []
        self.default_input = default_input  # use value in self.input is empty

    def exec_prog(self, input_=None):
        if input_ is not None:
            assert not self.input  # possible but not used
            self.input.extend(input_)
        while not self.halt:
            self.exec_op()
        # print("Halted, output:", self.output)

    def exec_for_input(self, input_=None):
        """pause when input is empty"""
        assert not self.input_callback  # possible but not used
        if input_ is not None:
            assert not self.input
            self.input.extend(input_)
        while not self.halt and self.input:
            self.exec_op()
        # print("Halted, output:", self.output)
        return None  # Halted

    def exec_to_output(self, input_=None):
        """return first output value found and pause"""
        assert not self.output_callback
        if input_ is not None:
            assert not self.input  # possible but not used
            self.input.extend(input_)
        while not self.halt:
            if self.output:
                return self.output.pop(0)
            self.exec_op()
        # print("Halted, output:", self.output)
        return None  # Halted

    def exec_op(self):
        mem = self.mem
        pos = self.pos

        # parse opcode position
        opcode = mem[pos] % 100
        parameter_mode = list(
            reversed(list(map(int, str(mem[pos] // 100))))
        )  # 0 position mode, 1 immediate, 2 relative, default is 0
        # print("paramodes", parameter_mode)
        pos += 1  # we have eaten tho opcode

        def get_data():
            nonlocal pos
            val = mem[pos]
            pos += 1  # we have eaten a data field
            pm = parameter_mode.pop(0) if parameter_mode else 0

            if pm == 0:
                # print("pm", val, mem[val])
                # position mode
                return mem[val]
            elif pm == 1:
                # immediate mode
                # print("im", val)
                return val
            elif pm == 2:
                # relative mode
                return mem[self.relative_base + val]
            else:
                raise Exception("Unknown parameter_mode", pm)

        def write_data(data):
            nonlocal pos
            val = mem[pos]
            pos += 1  # we have eaten a data field
            pm = parameter_mode.pop(0) if parameter_mode else 0

            if pm == 0:
                # print("pm", val, mem[val])
                # position mode
                mem[val] = data
            elif pm == 1:
                # immediate mode
                Exception("target parameter in immediate mode")
            elif pm == 2:
                # relative mode
                mem[self.relative_base + val] = data
            else:
                raise Exception("Unknown parameter_mode", pm)

        if opcode == 1:
            write_data(get_data() + get_data())
        elif opcode == 2:
            write_data(get_data() * get_data())
        elif opcode == 3:
            # Opcode 3 takes a single integer as input and saves it to the address given by its only parameter.
            # For example, the instruction 3,50 would take an input value and store it at address 50.
            # print("read_input from", self.input)
            if self.input:
                val = self.input.pop(0)
                # print("in1", val)
            elif self.input_callback:
                val = self.input_callback()
                if isinstance(val, list):
                    self.input = val
                    val = self.input.pop(0)
            elif self.default_input is not None:
                val = self.default_input
            else:
                raise Exception("no input available")
            write_data(val)
        elif opcode == 4:
            # Opcode 4 outputs the value of its only parameter. For example, the instruction 4,50 would output the value at address 50.
            val = get_data()
            if self.output_callback is not None:
                # print("OUT", val)
                self.output_callback(val)
            else:
                # print("OUT2", val)
                self.output.append(val)
            # print("output:", val)
        elif opcode == 5:
            # Opcode 5 is jump-if-true: if the first parameter is non-zero,
            # it sets the instruction pointer to the value from the second parameter. Otherwise, it does nothing.
            if get_data():
                pos = get_data()
            else:
                get_data()
        elif opcode == 6:
            # Opcode 6 is jump-if-false: if the first parameter is zero, it sets the instruction
            # pointer to the value from the second parameter. Otherwise, it does nothing.
            if not get_data():
                pos = get_data()
            else:
                get_data()
        elif opcode == 7:
            # Opcode 7 is less than: if the first parameter is less than the second parameter,
            # it stores 1 in the position given by the third parameter. Otherwise, it stores 0.
            if get_data() < get_data():
                write_data(1)
            else:
                write_data(0)
        elif opcode == 8:
            # Opcode 8 is equals: if the first parameter is equal to the second parameter,
            # it stores 1 in the position given by the third parameter. Otherwise, it stores 0.
            if get_data() == get_data():
                write_data(1)
            else:
                write_data(0)
        elif opcode == 9:
            # Opcode 9 adjusts the relative base by the value of its only parameter.
            # The relative base increases (or decreases, if the value is negative) by the value of the parameter.
            self.relative_base += get_data()

        elif opcode == 99:
            self.halt = True
        else:
            raise Exception("Unknown opcode", opcode)

        self.pos = pos


def get_hull(_data):
    cmp = Computer(_data)
    cmp.exec_prog()
    hull = collections.defaultdict(lambda: ".")
    board_str = "".join(map(chr, cmp.output))
    for lineno, line in enumerate(board_str.splitlines()):
        for col, val in enumerate(line):
            hull[(col + lineno * 1j)] = val
    return hull


def interact(_data):


    auto_play = """north
take festive hat
east
take prime number
west
west
take sand
south
north
east
south
east
north
take weather machine
north
take mug
south
south
east
north
east
east
take astronaut ice cream
west
west
south
west
west
south
west
east
south
take mutex
west
east
south
take boulder
east
south
inv
solve
""".splitlines()

    cmd_history = []
    out_buf = []
    map_ = {}
    x, y = 0, 0
    solve_mode = False
    all_items = set() # that is or has been in the inventory
    to_try = []
    last_try = None
    def in_callback():
        nonlocal x
        nonlocal y
        nonlocal solve_mode
        nonlocal auto_play, to_try

        msg = "".join(out_buf)
        # parse_msg(msg)
        if "Items in your inventory:" in msg:
            for line in msg.splitlines():
                if line.startswith("- "):
                    all_items.add(line[2:])
        # print(repr("".join(out_buf)))
        print(msg)

        # def drop_everything():
        #     res = []
        #     for item in all_items:
        #         res.append(f"drop {item}")
        #     return res


        def solve():
            nonlocal solve_mode
            nonlocal last_try
            if "Alert! Droids on this ship are lighter than the detected value!" in msg:
                print("DETECT to heavy alert")
            elif "Alert! Droids on this ship are heavier than the detected value!" in msg:
                print("DETECT to light alert")
            else:
                print("DETECT failed")
                solve_mode = False
                cmd = input()
                return cmd

            if not last_try:
                commands = [f"drop {item}" for item in all_items]
            else:
                commands = [f"drop {item}" for item in last_try]

            combination = to_try.pop()
            commands.extend([f"take {item}" for item in combination])
            commands.append("east")
            last_try = combination
            return commands



        if auto_play:
            cmd = auto_play.pop(0)
        elif solve_mode:
            cmd = solve()
            if isinstance(cmd, list):
                auto_play = cmd
                cmd = auto_play.pop(0)


            # cmd = "inv"
            # solve_mode = False


            # print("inv:")
            # print("ite:")
            # print("inv:")
            # print(map_)


        else:
            cmd = input()
        if cmd == "history":
            print("History:", "\n".join(cmd_history))
            return in_callback()
        elif cmd == "repeat":
            print(out_buf)
            return in_callback()
        elif cmd == "map":
            print(map_)
            return in_callback()
        elif cmd == "solve":
            cmd = "east"  # try with current inventory
            solve_mode = True
            print("ALLIT", all_items)
            to_try = list()
            import itertools
            for i in range(1, len(all_items)+1):
                to_try.extend(itertools.combinations(all_items, i))
            to_try.sort(key=len)
            print(to_try)


        cmd_history.append(cmd)
        cmd2 = list(map(ord, cmd + "\n"))
        # print(repr(cmd), cmd2)
        out_buf[:] = []
        print(cmd)
        return cmd2

    def out_callback(val):
        out_buf.append(chr(val))
        #print(chr(val), end="")

    cmp = Computer(_data, input_callback=in_callback, output_callback=out_callback)
    cmp.exec_prog()
    print("Last try was:", last_try)
    print("remaining:", to_try)
    print(''.join(out_buf))
    # print("History:")
    # print("\n".join(cmd_history))



@timeme
def part1(_data):
    "Do the part1 calculation"

    print("""Commands
north, south, east, or west.
take <name of item>. For example, if the droid reports seeing a red ball, you can pick it up with take red ball.
To drop an item the droid is carrying, use the command drop <name of item>. For example, if the droid is carrying a green ball, you can drop it with drop green ball.
To get a list of all of the items the droid is currently carrying, use the command inv (for "inventory").""")


    interact(_data)
    return
    hull = get_hull(_data)

    utils.print_cboard(hull)

    alignment_parameter_sum = 0
    for pos, val in list(hull.items()):
        if val != "#":
            continue
        intersection = True
        for step in (-1, 1, 1j, -1j):
            pos2 = pos + step
            if hull[pos2] != "#":
                intersection = False
                break
        if intersection:
            # hull[pos] = "O"
            alignment_parameter_sum += pos.real * pos.imag

    return alignment_parameter_sum
    utils.print_cboard(hull)


def turn_right(dir_):
    dirs = (1, 1j, -1, -1j)
    return dirs[(dirs.index(dir_) + 1) % 4]


# def turn_left(dir_):
#     return -turn_right(dir_)


def calc_path(hull):
    start_pos = {v: k for k, v in hull.items()}["^"]
    pos = start_pos
    path = ["L"]  # to get in line with line
    dir_ = -1
    steps = 0

    while True:
        if hull[pos + dir_] == "#":
            # go forward to the end
            steps += 1
            pos = pos + dir_
            continue
        path.append(steps)
        steps = 0
        dir_right = turn_right(dir_)
        dir_left = -dir_right
        if hull[pos + dir_right] == "#":
            # pos = pos + dir_right
            dir_ = dir_right
            path.append("R")
        elif hull[pos + dir_left] == "#":
            # pos = pos + dir_left
            dir_ = dir_left
            path.append("L")
        else:
            return list(map(str, path))


@timeme
def part2(_data):
    "Do the part2 calculation"

    return
    hull = get_hull(_data)
    utils.print_cboard(hull, axis=True)

    assert turn_right(1) == 1j

    path = calc_path(hull)
    path = ",".join(path)
    print("path", path)
    # L,10,R,12,R,12,R,6,R,10,L,10,L,10,R,12,R,12,R,10,L,10,L,12,R,6,R,6,R,10,L,10,R,10,L,10,L,12,R,6,R,6,R,10,L,10,R,10,L,10,L,12,R,6,L,10,R,12,R,12,R,10,L,10,L,12,R,6
    """
    A    L,10,R,12,R,12,
    B    R,6,R,10,L,10,
    A    L,10,R,12,R,12,
    C    R,10,L,10,L,12,R,6,
    B    R,6,R,10,L,10,
    C    R,10,L,10,L,12,R,6,
    B    R,6,R,10,L,10,
    C    R,10,L,10,L,12,R,6,
    A    L,10,R,12,R,12,
    C    R,10,L,10,L,12,R,6

    """
    main_ = "A,B,A,C,B,C,B,C,A,C\n"
    a = "L,10,R,12,R,12\n"
    b = "R,6,R,10,L,10\n"
    c = "R,10,L,10,L,12,R,6\n"

    robot_input = list(map(ord, main_ + a + b + c + "n\n"))

    # routines = []
    # while path:
    #     if len(path) <= 20:
    #         routines.append(path)
    #         break
    #     split_at = path.rindex(",", 0, 20)
    #     routines.append(path[0:split_at])
    #     path = path[split_at+1:]
    # print(routines)

    _data[0] = 2  # wake robot up

    cmp = Computer(_data)
    cmp.exec_prog(robot_input)
    # print(cmp.output)
    return cmp.output[-1]

    pass
    # cmp = Computer(_data)


if __name__ == "__main__":
    main()
