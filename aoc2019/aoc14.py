#!/bin/env python3

import collections
import functools
import itertools
import math
import time

import input_readers


def timeme(method):
    """Decorator that prints the elapsed time after a call"""

    @functools.wraps(method)
    def wrapper(*args, **kw):
        start_time = time.time()
        result = method(*args, **kw)
        end_time = time.time()
        print("timed", wrapper.__name__, end_time - start_time, "s")
        return result

    return wrapper


Reaction = collections.namedtuple("Reaction", "inputs output")
Chemical = collections.namedtuple("Chemical", "count name")


def main():
    "read input data, process it if necessary and call the part workers"
    # data = input_readers.read_input_str()
    # data = input_readers.read_input_int_list_multiline()
    data = input_readers.read_input_str_list()
    # data = input_readers.read_input_table_tab()
    data2 = []
    for line in data:
        inps_str, outp_str = line.split(" => ")
        inp_strs = inps_str.split(", ")
        del inps_str
        inputs = [inp.split(" ") for inp in inp_strs]
        inputs = [Chemical(int(str1), str2) for str1, str2 in inputs]

        output = outp_str.split(" ")
        output = Chemical(int(output[0]), output[1])

        # inputs = [Chemical(*inp.split(" ")) for inp in inp_strs]
        # output = Chemical(*outp_str.split(" "))
        data2.append(Reaction(inputs, output))
    data = tuple(data2)

    # data = [line.split(" => ") for line in data]
    # data = [Reaction(tuple(line[0].split(", ")), line[1]) for line in data]

    # assert part1('') ==
    print("part1:", part1(data))

    # assert part2('') ==
    print("part2:", part2(data))


@timeme
def part1(_data):
    "Do the part1 calculation"

    res = needed_ore(_data, 1)
    print(res)
    return res


def needed_ore(_data, fuel_amount):
    # reverse the reactions to a {want: [need]}
    want_need = {
        reaction.output.name: (reaction.output.count, reaction.inputs)
        for reaction in _data
    }  # name: (count, inputs)

    # print(want_need)

    need_to_make = collections.Counter(FUEL=fuel_amount)
    needed_ore = 0  # we keep
    # have = {}  # {what, count}
    while need_to_make:
        # print("=========================")
        # print(needed_ore, need_to_make)
        make_chemical, need_amount = need_to_make.most_common(1)[0]
        if need_amount <= 0:
            break
        # make_chemical, need_amount = need_to_make.popitem()
        make_amount, inputs = want_need[make_chemical]
        make_iter = math.ceil(need_amount / make_amount)
        need_to_make[make_chemical] -= make_iter * make_amount

        for input_ in inputs:
            if input_.name == "ORE":
                needed_ore += input_.count * make_iter
            else:
                need_to_make[input_.name] += input_.count * make_iter
    return needed_ore


@timeme
def part2(_data):
    "Do the part2 calculation"
    has_ore = 1000000000000

    can_make = 0
    cant_make = -1
    for i in itertools.count(10):
        res = needed_ore(_data, 2 ** i)
        if res > has_ore:
            print("cant make", 2 ** i, res)
            cant_make = 2 ** i
            break
        else:
            can_make = 2 ** i
            print("can make", 2 ** i, res)
        # print("can make", can_make, "cant make", cant_make)

    del i

    # bisect_search
    print("bisect search")
    while not can_make == cant_make - 1:
        test_at = (cant_make - can_make) // 2 + can_make
        print("test_at", test_at)
        res = needed_ore(_data, test_at)
        if res > has_ore:
            # print("cant make", test_at, res)
            cant_make = test_at
        else:
            can_make = test_at
            # print("can make", test_at, res)
        print("can make", can_make, "cant make", cant_make)

    return can_make


if __name__ == "__main__":
    main()
