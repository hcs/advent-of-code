#!/bin/env python3

import collections
import functools
import itertools
import random
import time

import input_readers
import utils


def timeme(method):
    """Decorator that prints the elapsed time after a call"""

    @functools.wraps(method)
    def wrapper(*args, **kw):
        start_time = time.time()
        result = method(*args, **kw)
        end_time = time.time()
        print("timed", wrapper.__name__, end_time - start_time, "s")
        return result

    return wrapper


def main():
    "read input data, process it if necessary and call the part workers"
    data = input_readers.read_input_str()
    data = list(map(int, data.split(",")))
    # data = input_readers.read_input_int_list_multiline()
    # data = input_readers.read_input_str_list()
    # data = input_readers.read_input_table_tab()

    # assert part1('') ==
    print("part1:", part1(data))

    # assert part2('') ==
    print("part2:", part2(data))


class Computer:
    def __init__(self, mem, input_=None):
        # self.mem = list(mem)

        self.mem = collections.defaultdict(lambda: 0, enumerate(mem))

        self.pos = 0
        self.halt = False
        self.relative_base = 0
        if not input:
            self.input = []
        else:
            self.input = input_
        self.input_callback = None  # function to call instead of using self.input
        self.output = []

    def exec_prog(self, input_=None):
        if input_ is not None:
            self.input = input_
        while not self.halt:
            self.exec_op()
        # print("Halted, output:", self.output)

    def exec_for_input(self, input_=None):
        """pause when input is empty"""
        if input_ is not None:
            self.input = input_
        while not self.halt and self.input:
            self.exec_op()
        # print("Halted, output:", self.output)
        return None  # Halted

    def exec_to_output(self, input_=None):
        """return first output value found and pause"""
        if input_ is not None:
            self.input = input_
        while not self.halt:
            if self.output:
                return self.output.pop(0)
            self.exec_op()
        # print("Halted, output:", self.output)
        return None  # Halted

    def exec_op(self):
        mem = self.mem
        pos = self.pos

        # parse opcode position
        opcode = mem[pos] % 100
        parameter_mode = list(
            reversed(list(map(int, str(mem[pos] // 100))))
        )  # 0 position mode, 1 immediate, 2 relative, default is 0
        # print("paramodes", parameter_mode)
        pos += 1  # we have eaten tho opcode

        def get_data():
            nonlocal pos
            val = mem[pos]
            pos += 1  # we have eaten a data field
            pm = parameter_mode.pop(0) if parameter_mode else 0

            if pm == 0:
                # print("pm", val, mem[val])
                # position mode
                return mem[val]
            elif pm == 1:
                # immediate mode
                # print("im", val)
                return val
            elif pm == 2:
                # relative mode
                return mem[self.relative_base + val]
            else:
                raise Exception("Unknown parameter_mode", pm)

        def write_data(data):
            nonlocal pos
            val = mem[pos]
            pos += 1  # we have eaten a data field
            pm = parameter_mode.pop(0) if parameter_mode else 0

            if pm == 0:
                # print("pm", val, mem[val])
                # position mode
                mem[val] = data
            elif pm == 1:
                # immediate mode
                Exception("target parameter in immediate mode")
            elif pm == 2:
                # relative mode
                mem[self.relative_base + val] = data
            else:
                raise Exception("Unknown parameter_mode", pm)

        if opcode == 1:
            write_data(get_data() + get_data())
        elif opcode == 2:
            write_data(get_data() * get_data())
        elif opcode == 3:
            # Opcode 3 takes a single integer as input and saves it to the address given by its only parameter.
            # For example, the instruction 3,50 would take an input value and store it at address 50.
            # print("read_input from", self.input)
            if self.input_callback:
                write_data(self.input_callback())
            else:
                write_data(self.input.pop(0))
        elif opcode == 4:
            # Opcode 4 outputs the value of its only parameter. For example, the instruction 4,50 would output the value at address 50.
            val = get_data()
            self.output.append(val)
            # print("output:", val)
        elif opcode == 5:
            # Opcode 5 is jump-if-true: if the first parameter is non-zero,
            # it sets the instruction pointer to the value from the second parameter. Otherwise, it does nothing.
            if get_data():
                pos = get_data()
            else:
                get_data()
        elif opcode == 6:
            # Opcode 6 is jump-if-false: if the first parameter is zero, it sets the instruction
            # pointer to the value from the second parameter. Otherwise, it does nothing.
            if not get_data():
                pos = get_data()
            else:
                get_data()
        elif opcode == 7:
            # Opcode 7 is less than: if the first parameter is less than the second parameter,
            # it stores 1 in the position given by the third parameter. Otherwise, it stores 0.
            if get_data() < get_data():
                write_data(1)
            else:
                write_data(0)
        elif opcode == 8:
            # Opcode 8 is equals: if the first parameter is equal to the second parameter,
            # it stores 1 in the position given by the third parameter. Otherwise, it stores 0.
            if get_data() == get_data():
                write_data(1)
            else:
                write_data(0)
        elif opcode == 9:
            # Opcode 9 adjusts the relative base by the value of its only parameter.
            # The relative base increases (or decreases, if the value is negative) by the value of the parameter.
            self.relative_base += get_data()

        elif opcode == 99:
            self.halt = True
        else:
            raise Exception("Unknown opcode", opcode)

        self.pos = pos


@timeme
def part1(_data):
    "Do the part1 calculation"

    # north (1), south (2), west (3), and east (4)
    # dir_ is separate from above numbers, it's the index to direcections
    # directions = (
    #     ((0, -1), 1),
    #     ((1, 0), 4),
    #     ((0, 1), 2),
    #     ((-1, 0), 3),
    # )
    directions = (
        (0 + -1j, 1),
        (1 + 0j, 4),
        (0 + 1j, 2),
        (-1 + 0j, 3),
    )

    cmp = Computer(_data)
    hull = collections.defaultdict(lambda: " ")  # starts black
    hull[0j] = "X"  # start pos
    o_pos = None  # is at (-20-14j)

    # def spiral_search():
    #     last_pos = 0 + 0j
    #     # print("A")
    #     for go_to in utils.manhattan_spiral((0, 0), 100):
    #         go_to = complex(*go_to)
    #         # print("spir")
    #         movement = go_to - last_pos
    #         last_pos = go_to
    #         for dir_, direction_tuple in enumerate(directions):
    #             # print(dir_)
    #             if direction_tuple[0] == movement:
    #                 print(movement)
    #                 yield dir_
    #                 break

    def make_map():
        pos = complex(0, 0)
        dir_ = 0
        to_check = set(
            [0j + movement for movement, _inst in directions]
        )  # positions we have been close to but not checked, we are finished mapping when this is empty
        # print(to_check)
        known = set([0j])
        path_from_start = [0j]  # backtracked parts not included

        def where_to_go():
            # if we are next to an unknown, go there
            for (dir_, (movement, _direction_inst)) in enumerate(directions):
                if pos + movement in to_check:
                    return dir_

            # print(path_from_start, pos)

            # else backtrack util we are next ta an unknown

            while True:
                last_pos = path_from_start.pop()
                if last_pos != pos:
                    break
            movement = last_pos - pos
            for (dir_, (movement2, _direction_inst)) in enumerate(directions):
                if movement == movement2:
                    return dir_

            raise Exception("should not happen")

            return random.randint(0, 3)

        movement_counter = 0
        while to_check:
            movement_counter += 1
            dir_ = where_to_go()
            # print(dir_)
            movement, dir_inst = directions[dir_]
            # print(pos, movement, dir_inst)
            status = cmp.exec_to_output([dir_inst])
            if status == 0:
                # 0: The repair droid hit a wall. Its position has not changed.
                # hull[(pos[0] + movement[0], pos[1] + movement[1])] = "#"
                hull[pos + movement] = "#"
                known.add(pos + movement)
                to_check.discard(pos + movement)
                # dir_ = (dir_ + 1) % 4
            elif status == 1:
                # 1: The repair droid has moved one step in the requested direction.
                # hull[(pos[0] + movement[0], pos[1] + movement[1])] = "."
                hull[pos] = hull[pos] if hull[pos] in "OX." else "."
                pos = pos + movement
                hull[pos] = hull[pos] if hull[pos] in "OX." else "D"
                to_check.discard(pos)
                known.add(pos)
                path_from_start.append(pos)
                # pos = (pos[0] + movement[0], pos[1] + movement[1])
                # pos = pos + movement
            elif status == 2:
                # 2: The repair droid has moved one step in the requested direction; its new position is the location of the oxygen system.
                # pos = (pos[0] + movement[0], pos[1] + movement[1])
                hull[pos] = hull[pos] if hull[pos] in "OX." else "."
                pos = pos + movement
                hull[pos] = "O"
                to_check.discard(pos)
                known.add(pos)
                path_from_start.append(pos)
                nonlocal o_pos
                o_pos = pos

                # for wpos in path_from_start[1:-1]:
                #     hull[wpos] = "p"

                # return pos
            else:
                raise Exception("Unknown status")

            # add surrounding unknowns to list to check
            for movement, _dir_inst in directions:
                if pos + movement not in known:
                    to_check.add(pos + movement)

            if False and movement_counter % 10000 == 0:
                print("======================", pos, o_pos, to_check)
                utils.print_cboard(
                    hull,
                    get_char=lambda board, pos: f'{board.get(pos, " ")}'
                    # get_char=lambda board, pos: {0: " ", 1: "|", 2: "#", 3: "-", 4: "*"}[
                    #     board.get(pos, 0)
                    # ],
                )

    make_map()
    print("======================")
    utils.print_cboard(
        hull,
        # get_char=lambda board, pos: {0: " ", 1: "|", 2: "#", 3: "-", 4: "*"}[
        #     board.get(pos, 0)
        # ],
    )

    # walk map, shortest first
    reachable_in_steps = [set([0j])]  # position in list is the number of steps
    processed_positions = set([0j])
    for steps in itertools.count(1):
        positions = set()
        reachable_in_steps.append(positions)
        for pos in reachable_in_steps[steps - 1]:
            for movement, _dir_inst in directions:
                pos2 = pos + movement
                if pos2 not in processed_positions:
                    processed_positions.add(pos2)
                    if hull[pos2] == "#":
                        pass
                    elif hull[pos2] == ".":
                        positions.add(pos2)
                    elif hull[pos2] == "O":
                        return steps
                    else:
                        raise Exception("unexpected")


@timeme
def part2(_data):
    "Do the part2 calculation"

    # north (1), south (2), west (3), and east (4)
    # dir_ is separate from above numbers, it's the index to direcections
    # directions = (
    #     ((0, -1), 1),
    #     ((1, 0), 4),
    #     ((0, 1), 2),
    #     ((-1, 0), 3),
    # )
    directions = (
        (0 + -1j, 1),
        (1 + 0j, 4),
        (0 + 1j, 2),
        (-1 + 0j, 3),
    )

    cmp = Computer(_data)
    hull = collections.defaultdict(lambda: " ")  # starts black
    hull[0j] = "X"  # start pos
    o_pos = None  # is at (-20-14j)

    # def spiral_search():
    #     last_pos = 0 + 0j
    #     # print("A")
    #     for go_to in utils.manhattan_spiral((0, 0), 100):
    #         go_to = complex(*go_to)
    #         # print("spir")
    #         movement = go_to - last_pos
    #         last_pos = go_to
    #         for dir_, direction_tuple in enumerate(directions):
    #             # print(dir_)
    #             if direction_tuple[0] == movement:
    #                 print(movement)
    #                 yield dir_
    #                 break

    def make_map():
        pos = complex(0, 0)
        dir_ = 0
        to_check = set(
            [0j + movement for movement, _inst in directions]
        )  # positions we have been close to but not checked, we are finished mapping when this is empty
        # print(to_check)
        known = set([0j])
        path_from_start = [0j]  # backtracked parts not included

        def where_to_go():
            # if we are next to an unknown, go there
            for (dir_, (movement, _direction_inst)) in enumerate(directions):
                if pos + movement in to_check:
                    return dir_

            # print(path_from_start, pos)

            # else backtrack util we are next ta an unknown

            while True:
                last_pos = path_from_start.pop()
                if last_pos != pos:
                    break
            movement = last_pos - pos
            for (dir_, (movement2, _direction_inst)) in enumerate(directions):
                if movement == movement2:
                    return dir_

            raise Exception("should not happen")

            return random.randint(0, 3)

        movement_counter = 0
        while to_check:
            movement_counter += 1
            dir_ = where_to_go()
            # print(dir_)
            movement, dir_inst = directions[dir_]
            # print(pos, movement, dir_inst)
            status = cmp.exec_to_output([dir_inst])
            if status == 0:
                # 0: The repair droid hit a wall. Its position has not changed.
                # hull[(pos[0] + movement[0], pos[1] + movement[1])] = "#"
                hull[pos + movement] = "#"
                known.add(pos + movement)
                to_check.discard(pos + movement)
                # dir_ = (dir_ + 1) % 4
            elif status == 1:
                # 1: The repair droid has moved one step in the requested direction.
                # hull[(pos[0] + movement[0], pos[1] + movement[1])] = "."
                hull[pos] = hull[pos] if hull[pos] in "OX." else "."
                pos = pos + movement
                hull[pos] = hull[pos] if hull[pos] in "OX." else "D"
                to_check.discard(pos)
                known.add(pos)
                path_from_start.append(pos)
                # pos = (pos[0] + movement[0], pos[1] + movement[1])
                # pos = pos + movement
            elif status == 2:
                # 2: The repair droid has moved one step in the requested direction; its new position is the location of the oxygen system.
                # pos = (pos[0] + movement[0], pos[1] + movement[1])
                hull[pos] = hull[pos] if hull[pos] in "OX." else "."
                pos = pos + movement
                hull[pos] = "O"
                to_check.discard(pos)
                known.add(pos)
                path_from_start.append(pos)
                nonlocal o_pos
                o_pos = pos

                # for wpos in path_from_start[1:-1]:
                #     hull[wpos] = "p"

                # return pos
            else:
                raise Exception("Unknown status")

            # add surrounding unknowns to list to check
            for movement, _dir_inst in directions:
                if pos + movement not in known:
                    to_check.add(pos + movement)

            if False and movement_counter % 10000 == 0:
                print("======================", pos, o_pos, to_check)
                utils.print_cboard(
                    hull,
                    get_char=lambda board, pos: f'{board.get(pos, " ")}'
                    # get_char=lambda board, pos: {0: " ", 1: "|", 2: "#", 3: "-", 4: "*"}[
                    #     board.get(pos, 0)
                    # ],
                )

    make_map()
    print("======================")
    utils.print_cboard(
        hull,
        # get_char=lambda board, pos: {0: " ", 1: "|", 2: "#", 3: "-", 4: "*"}[
        #     board.get(pos, 0)
        # ],
    )

    # walk map, shortest first from O
    reachable_in_steps = [set([o_pos])]  # position in list is the number of steps
    processed_positions = set([o_pos])
    for steps in itertools.count(1):
        positions = set()
        reachable_in_steps.append(positions)
        for pos in reachable_in_steps[steps - 1]:
            for movement, _dir_inst in directions:
                pos2 = pos + movement
                if pos2 not in processed_positions:
                    processed_positions.add(pos2)
                    if hull[pos2] == "#":
                        pass
                    elif hull[pos2] == ".":
                        positions.add(pos2)
                    elif hull[pos2] == "X":
                        positions.add(pos2)
                    else:
                        raise Exception("unexpected")
        if not positions:
            return steps - 1


if __name__ == "__main__":
    main()
