#!/bin/env python3

import functools
import time

import input_readers
import utils


def timeme(method):
    """Decorator that prints the elapsed time after a call"""

    @functools.wraps(method)
    def wrapper(*args, **kw):
        start_time = time.time()
        result = method(*args, **kw)
        end_time = time.time()
        print("timed", wrapper.__name__, end_time - start_time, "s")
        return result

    return wrapper


def main():
    "read input data, process it if necessary and call the part workers"
    data = input_readers.read_input_str()
    # data = input_readers.read_input_int_list_multiline()
    data = input_readers.read_input_str_list()
    # data = input_readers.read_input_table_tab()

    # assert part1('') ==
    assert (
        part1(["R75,D30,R83,U83,L12,D49,R71,U7,L72", "U62,R66,U55,R34,D71,R55,D58,R83"])
        == 159
    )
    assert (
        part1(
            [
                "R98,U47,R26,D63,R33,U87,L62,D20,R33,U53,R51",
                "U98,R91,D20,R16,D67,R40,U7,R15,U6,R7",
            ]
        )
        == 135
    )
    print("part1:", part1(data))

    # assert part2('') ==
    assert (
        part2(["R75,D30,R83,U83,L12,D49,R71,U7,L72", "U62,R66,U55,R34,D71,R55,D58,R83"])
        == 610
    )
    assert (
        part2(
            [
                "R98,U47,R26,D63,R33,U87,L62,D20,R33,U53,R51",
                "U98,R91,D20,R16,D67,R40,U7,R15,U6,R7",
            ]
        )
        == 410
    )
    print("part2:", part2(data))


def draw_wire(board, path, character):
    xpos = 0
    ypos = 0
    for inst in path:
        xstep = 0
        ystep = 0
        dir_ = inst[0]
        dist = int(inst[1:])
        if dir_ == "R":
            xstep = 1
        elif dir_ == "L":
            xstep = -1
        elif dir_ == "U":
            ystep = 1
        elif dir_ == "D":
            ystep = -1
        else:
            raise Exception("unknown direction in instruction", inst)
        for _step in range(dist):
            xpos += xstep
            ypos += ystep
            board.setdefault((xpos, ypos), 0)
            board[(xpos, ypos)] += character


@timeme
def part1(_data):
    "Do the part1 calculation"
    board = {}  # (x, y): code
    for wire_number, wire in enumerate(_data, start=1):
        instructions = wire.split(",")
        draw_wire(board, instructions, wire_number)
    # utils.print_board(board)
    for pos in utils.manhattan_spiral((0, 0), 1000):
        if board.get(pos, 0) == 3:
            dist = utils.manhattan_distance((0, 0), pos)
            # print(pos, dist)
            return dist
    raise Exception("found nothing")


def draw_wire2(board, path, wire_id):
    xpos = 0
    ypos = 0
    wire_length = 0
    for inst in path:
        xstep = 0
        ystep = 0
        dir_ = inst[0]
        dist = int(inst[1:])
        if dir_ == "R":
            xstep = 1
        elif dir_ == "L":
            xstep = -1
        elif dir_ == "U":
            ystep = 1
        elif dir_ == "D":
            ystep = -1
        else:
            raise Exception("unknown direction in instruction", inst)
        for _step in range(dist):
            xpos += xstep
            ypos += ystep
            wire_length += 1
            posdata = board.setdefault((xpos, ypos), {})
            if wire_id not in posdata:
                posdata[wire_id] = wire_length
            # board[(xpos, ypos)] += wire_id


@timeme
def part2(_data):
    "Do the part2 calculation"
    board = {}  # (x, y): code
    for wire_number, wire in enumerate(_data, start=1):
        instructions = wire.split(",")
        draw_wire2(board, instructions, wire_number)
    # utils.print_board(board, get_char=lambda board, pos: f'{len(board.get(pos, ""))}', end="")
    # bsmax = max(utils.board_size(board))

    crossings = []
    for pos, posdata in board.items():
        if len(posdata) == 2:
            crossings.append((posdata[1] + posdata[2], pos))
    crossings.sort()
    print(crossings)
    return crossings[0][0]

    # for pos in utils.manhattan_spiral((0,0), bsmax+1):
    #     posdata = board.get(pos, {})

    #         dist =  utils.manhattan_distance((0, 0), pos)
    #         #print(pos, dist)
    #         return dist
    # raise Exception("found nothing")


if __name__ == "__main__":
    main()
