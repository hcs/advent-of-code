def manhattan_distance(pos1, pos2):

    res = abs(abs(pos1[0]) - abs(pos2[0])) + abs(abs(pos1[1]) - abs(pos2[1]))
    # if pos1 == (5,5):
    #     print(f'md({pos1}, {pos2}) -> {res}')
    return res


def manhattan_spiral(center=(0, 0), max_radius=0):
    """spiral out from the center"""
    for radius in range(1, max_radius):
        for pos in manhattan_circle(center, radius):
            yield pos


def board_size(board):
    xkeys, ykeys = zip(*board.keys())
    minx = min(xkeys)
    miny = min(ykeys)
    maxx = max(xkeys)
    maxy = max(ykeys)
    return maxx - minx, maxy - miny


def manhattan_circle(center, radius):
    """walk a radius with the same manhattan distance"""
    x, y = center
    x -= radius
    for _i in range(radius):
        x, y = x + 1, y + 1
        yield x, y
    for _i in range(radius):
        x, y = x + 1, y - 1
        yield x, y
    for _i in range(radius):
        x, y = x - 1, y - 1
        yield x, y
    for _i in range(radius):
        x, y = x - 1, y + 1
        yield x, y


def print_board(
    board: dict, get_char=lambda board, pos: f'{board.get(pos, ".")}', end=""
):
    xkeys, ykeys = zip(*board.keys())
    minx = min(xkeys)
    miny = min(ykeys)
    maxx = max(xkeys)
    maxy = max(ykeys)
    for y in range(miny, maxy + 1):
        for x in range(minx, maxx + 1):
            print(get_char(board, (x, y)), end="")
        print("")


def print_cboard(
    board: dict,
    get_char=lambda board, pos: f'{board.get(pos, ".")}',
    end="",
    axis=False,
):
    """board uses complex numbers as coordinates"""
    xkeys, ykeys = zip(*[(int(pos.real), int(pos.imag)) for pos in board.keys()])
    minx = min(xkeys)
    miny = min(ykeys)
    maxx = max(xkeys)
    maxy = max(ykeys)
    if axis:
        print("  ", end="")
        for x in range(minx, maxx + 1):
            print(x % 10, end="")
        print()
    for y in range(miny, maxy + 1):
        if axis:
            print(f"{y%10:<2}", end="")
        for x in range(minx, maxx + 1):
            print(get_char(board, complex(x, y)), end="")
        print("")


# print_board(board)
