import typing
from pathlib import Path

# Input data readers


def read_input(day) -> str:
    this_file = Path(__file__)
    input_file = this_file.parent / f"day{day}.input"  # ./day1.input
    return input_file.read_text()


def parse_input_str_list_multiline(input_str: str) -> typing.Sequence[str]:
    """
    >>> parse_input_str_list_multiline("one\\ntwo\\nthree")
    ('one', 'two', 'three')
    """
    return tuple([val.strip() for val in input_str.splitlines() if val.strip()])


def parse_input_int_list_multiline(input_str: str) -> typing.Sequence[int]:
    """
    >>> parse_input_int_list_multiline("1\\n-2\\n3")
    (1, -2, 3)
    """
    return tuple([int(val.strip()) for val in input_str.splitlines() if val.strip()])


def parse_input_int_list_comma(input_str: str, sep=",") -> typing.Sequence[int]:
    """
    >>> parse_input_int_list_comma("1,-2,3")
    (1, -2, 3)
    """
    return tuple(map(int, input_str.strip().split(sep)))


def parse_input_str_table_tab(
    input_str: str, sep="\t"
) -> typing.Sequence[typing.Sequence[str]]:
    """
    >>> parse_input_str_table_tab("1\\t2\\t3\\n4\\t-5\\t6\\n7\\t8\\t9")
    (('1', '2', '3'), ('4', '-5', '6'), ('7', '8', '9'))
    """
    rows = []
    for line in input_str.splitlines():
        row = tuple(element.strip() for element in line.strip().split(sep))
        rows.append(row)
    return tuple(rows)


def parse_input_int_table_tab(
    input_str: str, sep="\t"
) -> typing.Sequence[typing.Sequence[int]]:
    """
    >>> parse_input_int_table_tab("1\\t2\\t3\\n4\\t-5\\t6\\n7\\t8\\t9")
    ((1, 2, 3), (4, -5, 6), (7, 8, 9))
    """
    rows = []
    for line in input_str.splitlines():
        row = line.strip().split(sep)
        row = tuple(map(int, row))
        rows.append(row)
    return tuple(rows)
