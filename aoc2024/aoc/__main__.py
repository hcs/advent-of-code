import importlib

import typer
from icecream import ic  # pyright: ignore # noqa: F401

app = typer.Typer()


@app.command()
def aoc(day: int, part: int):
    mod = importlib.import_module(f"aoc.day{day}")
    if part == 1:
        mod.cmd1()
    elif part == 2:
        mod.cmd2()
    else:
        Exception()


if __name__ == "__main__":
    app()
