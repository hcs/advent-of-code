#!/bin/env python3
import collections

import sys
from pathlib import Path

INPUT_FILE = Path(f'./{Path(sys.argv[0]).stem}.input')  # ./aoc1.input


def read_input_str():
    return INPUT_FILE.read_text().strip()


def read_input_int_list():
    return [int(val) for val in INPUT_FILE.open().readlines()]


def read_input_str_list():
    return [val.strip()
            for val in INPUT_FILE.open().readlines() if val.strip()]


def read_input_table():
    rows = []
    for line in INPUT_FILE.open():
        row = line.strip().split('\t')
        row = list(map(int, row))
        rows.append(row)
    return rows


INPUT = read_input_str_list()
print(INPUT)


def part1():
    # assert calc1('') ==
    print('part1:', calc1(INPUT))


def calc1(data):
    def check(words):
        print(words)
        counts = collections.Counter(words)
        if max(counts.values()) > 1:
            return False
        return True
    valid = 0
    for line in data:
        words = line.split()
        if check(words):
            valid += 1
    return valid


def part2():
    # assert calc2('') ==
    print('part2:', calc2(INPUT))


def calc2(data):
    def check(words):
        words = [''.join(sorted(word)) for word in words]
        # print(words)

        counts = collections.Counter(words)
        if max(counts.values()) > 1:
            return False
        return True
    valid = 0
    for line in data:
        words = line.split()
        if check(words):
            valid += 1
    return valid


if __name__ == '__main__':
    part1()
    part2()
