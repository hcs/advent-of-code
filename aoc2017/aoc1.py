#!/bin/env python3

# The night before Christmas, one of Santa's Elves calls you in a panic. "The
# printer's broken! We can't print the Naughty or Nice List!" By the time you
# make it to sub-basement 17, there are only a few minutes until midnight. "We
# have a big problem," she says; "there must be almost fifty bugs in this system,
# but nothing else can print The List. Stand in this square, quick! There's no
# time to explain; if you can convince them to pay you in stars, you'll be able
# to--" She pulls a lever and the world goes blurry.

# When your eyes can focus again, everything seems a lot more pixelated than
# before. She must have sent you inside the computer! You check the system clock:
#     25 milliseconds until midnight. With that much time, you should be able to
#     collect all fifty stars by December 25th.

# Collect stars by solving puzzles. Two puzzles will be made available on each
# day millisecond in the advent calendar; the second puzzle is unlocked when you
# complete the first. Each puzzle grants one star. Good luck!

# You're standing in a room with "digitization quarantine" written in LEDs along
# one wall. The only door is locked, but it includes a small interface.
# "Restricted Area - Strictly No Digitized Users Allowed."

# It goes on to explain that you may only leave by solving a captcha to prove
# you're not a human. Apparently, you only get one millisecond to solve the
# captcha: too fast for a normal human, but it feels like hours to you.

# The captcha requires you to review a sequence of digits (your puzzle input) and
# find the sum of all digits that match the next digit in the list. The list is
# circular, so the digit after the last digit is the first digit in the list.

# For example:

#     1122 produces a sum of 3 (1 + 2) because the first digit (1) matches
#          the second digit and the third digit (2) matches the fourth digit.
#     1111 produces 4 because each digit (all 1) matches the next.
#     1234 produces 0 because no digit matches the next.
# 91212129 produces 9 because the only digit that matches the next one is
# the last digit, 9.


import sys
from pathlib import Path

INPUT_FILE = Path(f'./{Path(sys.argv[0]).stem}.input')  # ./aoc1.input


def read_input_str():
    return INPUT_FILE.read_text().strip()


def read_input_int_list():
    return [int(val) for val in INPUT_FILE.open().readlines()]


def read_input_table():
    rows = []
    for line in INPUT_FILE.open():
        row = line.strip().split('\t')
        row = list(map(int, row))
        rows.append(row)
    return rows


INPUT = read_input_str()


def part1():
    assert calc1('1122') == 3
    assert calc1('1111') == 4
    assert calc1('1234') == 0
    assert calc1('91212129') == 9
    print('part1:', calc1(INPUT))


def calc1(data):
    data = list(map(int, data))
    data.append(data[0])  # works instead of a circular list for this problem.
    sum_ = 0
    for idx in range(len(data) - 1):
        if data[idx] == data[idx + 1]:
            sum_ = sum_ + data[idx]
    return sum_


def part2():
    """\
    You notice a progress bar that jumps to 50% completion. Apparently, the
    door isn't yet satisfied, but it did emit a star as encouragement. The
    instructions change:

    Now, instead of considering the next digit, it wants you to consider the digit
    halfway around the circular list. That is, if your list contains 10 items, only
    include a digit in your sum if the digit 10/2 = 5 steps forward matches it.
    Fortunately, your list has an even number of elements.

    For example:

        1212 produces 6: the list contains 4 items, and all four digits match the digit 2 items ahead.
        1221 produces 0, because every comparison is between a 1 and a 2.
        123425 produces 4, because both 2s match each other, but no other digit has a match.
        123123 produces 12.
        12131415 produces 4.
    """
    assert calc2('1212') == 6
    assert calc2('1221') == 0
    assert calc2('123425') == 4
    assert calc2('123123') == 12
    assert calc2('12131415') == 4
    print('part1:', calc2(INPUT))


def calc2(data):
    data = list(map(int, data))
    data = data + data  # works instead of a circular list for this problem.
    sum_ = 0
    for idx in range(len(data)):
        if data[idx] == data[int(idx + len(data) / 2)]:
            sum_ = sum_ + data[idx]
    return sum_


if __name__ == '__main__':
    part1()
    part2()
