#!/bin/env python3

import sys
from pathlib import Path


def part1(data):
    # assert calc1('') ==
    print('part1:', calc1(data))


def calc1(data):
    pass


def part2(data):
    # assert calc2('') ==
    print('part2:', calc2(data))


def calc2(data):
    pass


# Input data readers


def read_input_str(input_file):
    return input_file.read_text().strip()


def read_input_int_list(input_file):
    return tuple([int(val) for val in input_file.open().readlines()])


def read_input_str_list(input_file):
    return tuple([val.strip()
                  for val in input_file.open().readlines() if val.strip()])


def read_input_table(input_file):
    rows = []
    for line in input_file.open():
        row = line.strip().split('\t')
        row = tuple(map(int, row))
        rows.append(row)
    return tuple(rows)


def main():
    input_file = Path(f'./{Path(sys.argv[0]).stem}.input')  # ./aoc1.input

    data = read_input_str(input_file)
    # data = read_input_int_list(input_file)
    # data = read_input_str_list(input_file)
    # data = read_input_table(input_file)

    part1(data)
    part2(data)


if __name__ == '__main__':
    main()
