#!/bin/env python3

import sys
from pathlib import Path

INPUT_FILE = Path(f'./{Path(sys.argv[0]).stem}.input')  # ./aoc1.input


def read_input_str():
    return INPUT_FILE.read_text().strip()


def read_input_int_list():
    return [int(val) for val in INPUT_FILE.open().readlines()]


def read_input_str_list():
    return [val.strip()
            for val in INPUT_FILE.open().readlines() if val.strip()]


def read_input_table():
    rows = []
    for line in INPUT_FILE.open():
        row = line.strip().split('\t')
        row = list(map(int, row))
        rows.append(row)
    return rows


INPUT = read_input_int_list()


def part1():
    assert calc1([0, 3, 0, 1, -3]) == 5
    print('part1:', calc1(INPUT))


def calc1(data):
    print(data)
    li = list(data)
    pos = 0
    jumps = 0
    while True:
        offset = li[pos]
        new_pos = pos + offset
        li[pos] += 1
        pos = new_pos
        jumps += 1
        if pos < 0 or pos >= len(li):
            return jumps


def part2():
    # assert calc2('') ==
    assert calc2([0, 3, 0, 1, -3]) == 10
    print('part2:', calc2(INPUT))


def calc2(data):
    print(data)
    li = list(data)
    pos = 0
    jumps = 0
    while True:
        offset = li[pos]
        new_pos = pos + offset
        if offset >= 3:
            li[pos] -= 1
        else:
            li[pos] += 1
        pos = new_pos
        jumps += 1
        if pos < 0 or pos >= len(li):
            print(li)
            return jumps


if __name__ == '__main__':
    part1()
    part2()
