#!/bin/env python3

import sys
from dataclasses import dataclass
from collections import defaultdict
from pathlib import Path


def part1(data):
    tdata = ['b inc 5 if a > 1',
             'a inc 1 if b < 5',
             'c dec -10 if a >= 1',
             'c inc -20 if c == 10']
    assert calc1(tdata) == 1
    print('part1:', calc1(data))


def calc1(data):
    ops = parse(data)
    registers = defaultdict(lambda: 0)
    for op in ops:
        if eval(f"{registers[op.comp_reg]} {op.comp_op} {op.comp_with}"):
            registers[op.register] += op.amount if op.inc else -op.amount


    #print(registers)
    return max(registers.values())


def part2(data):
    tdata = ['b inc 5 if a > 1',
             'a inc 1 if b < 5',
             'c dec -10 if a >= 1',
             'c inc -20 if c == 10']
    assert calc2(tdata) == 10
    # assert calc2('') ==
    print('part2:', calc2(data))


def calc2(data):
    ops = parse(data)
    registers = defaultdict(lambda: 0)
    max_val = 0
    for op in ops:
        if eval(f"{registers[op.comp_reg]} {op.comp_op} {op.comp_with}"):
            registers[op.register] += op.amount if op.inc else -op.amount
            max_val =  max(max_val, max(registers.values()))


    return max_val


# Input data readers


def read_input_str(input_file):
    return input_file.read_text().strip()


def read_input_int_list(input_file):
    return tuple([int(val) for val in input_file.open().readlines()])


def read_input_str_list(input_file):
    return tuple([val.strip()
                  for val in input_file.open().readlines() if val.strip()])


def read_input_table(input_file):
    rows = []
    for line in input_file.open():
        row = line.strip().split('\t')
        row = tuple(map(int, row))
        rows.append(row)
    return tuple(rows)





@dataclass
class Op:
    '''Class for keeping track of an item in inventory.'''
    register: str
    inc: str
    amount: int
    comp_reg: str
    comp_op: str
    comp_with: int

def parse(lines):
    data = []
    for line in lines:
        if not line:
            continue
        register, inc, amount, _, comp_reg, comp_op, comp_with = line.split(' ')
        op = Op(register, inc == 'inc', int(amount), comp_reg, comp_op, int(comp_with))
        #print(line, op)
        data.append(op)

    # data = [line.split(' ') for line in lines if line.strip()]
    # data = tuple([Op(*line) for line in data])
    return data

def main():
    input_file = Path(f'./{Path(sys.argv[0]).stem}.input')  # ./aoc1.input

    #data = read_input_str(input_file)
    # data = read_input_int_list(input_file)
    data = read_input_str_list(input_file)
    # data = read_input_table(input_file)



    part1(data)
    part2(data)


if __name__ == '__main__':
    main()
