#!/bin/env python3

import itertools
import math

INPUT = 361527


def part1():
    '''
    must be carried back to square 1 (the location of the only access port for
    this memory system) by programs that can only move up, down, left, or
    right. They always take the shortest path: the Manhattan Distance between
    the location of the data and square 1.

    For example:

        Data from square 1 is carried 0 steps, since it's at the access port.
        Data from square 12 is carried 3 steps, such as: down, left, left.
        Data from square 23 is carried only 2 steps: up twice.
        Data from square 1024 must be carried 31 steps.

    How many steps are required to carry the data from the square identified in your puzzle input all the way to the access port?
    '''
    assert calc1(1) == 0
    assert calc1(12) == 3
    assert calc1(23) == 2
    assert calc1(1024) == 31
    print('part1:', calc1(INPUT))


def calc1(data):
    '''
    v, antalet rutor
    l, levels, antalet varv runt mittpunkten
    w, with bredd/höjd på brädet
    bredden på brädet är w=1+l*2
    antalet rutor på brädet är v=(1+l*2)^2
    antalet varv är l=(sqrt(v)-1)/2


    '''
    # Först låt oss använda lite matte för att komma förbi alla kompletta varv
    print('data', data)
    # Antalet fullbordade varv
    full_l = math.floor((math.sqrt(data) - 1) / 2)
    print('full_l', full_l)
    # That is x squares
    full_squares = (1 + full_l * 2)**2
    print('full_s', full_squares)

    # Specialfallet då det är exakt ett helt antal varv
    if full_squares == data:
        return 2 * full_l

    # låt oss vandra runt i det sista varvet
    x = full_l + 1
    y = -full_l
    pos = (x, y)
    print(pos)
    l = full_l + 1  # the partial level
    for _ in range(data - full_squares - 1):
        print(pos)
        # first handle the corners
        if pos == (l, l):
            pos = (x - 1, y)
        elif pos == (-l, l):
            pos = (x, y - 1)
        elif pos == (-l, -l):
            pos = (x + 1, y)
        elif pos == (l, -l):
            pos = (x, y + 1)
        elif x == l:
            pos = (x, y + 1)
        elif y == l:
            pos = (x - 1, y)
        elif x == -l:
            pos = (x, y - 1)
        elif y == -l:
            pos = (x + 1, y)
        x, y = pos

    print(data, pos)

    return abs(x) + abs(y)


def part2():
    # assert calc2('') ==
    print('part2:', calc2(INPUT))


def calc2(data):
    for pos, val in walk_spiral():
        print(pos, val)
        if val > data:
            return val
    return None


def walk_spiral():
    values = {(0, 0): 1}  # pos: value

    def calc_value(pos):
        assert pos == (0, 0) or pos not in values
        x, y = pos
        positions = itertools.product(
            (x - 1, x, x + 1), (y - 1, y, y + 1))  # lets get all 9 pos
        vals = [values.get(pos, 0) for pos in positions]
        val = values[pos] = sum(vals)
        return val

    pos = (0, 0)
    l = 0
    while True:
        yield pos, calc_value(pos)
        x, y = pos
        # first handle the corners
        if pos == (l, -l):  # This is the exit corner
            pos = (x + 1, y)
            l = l + 1
        elif pos == (l, l):
            pos = (x - 1, y)
        elif pos == (-l, l):
            pos = (x, y - 1)
        elif pos == (-l, -l):
            pos = (x + 1, y)
        elif x == l:
            pos = (x, y + 1)
        elif y == l:
            pos = (x - 1, y)
        elif x == -l:
            pos = (x, y - 1)
        elif y == -l:
            pos = (x + 1, y)


if __name__ == '__main__':
    part1()
    part2()
