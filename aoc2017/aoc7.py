#!/bin/env python3

import sys
import dataclasses
from pathlib import Path


def part1(data):
    testdata = (('pbga', 66, ()),
                ('xhth', 57, ()),
                ('ebii', 61, ()),
                ('havc', 66, ()),
                ('ktlj', 57, ()),
                ('fwft', 72, ('ktlj', 'cntj', 'xhth')),
                ('qoyq', 66, ()),
                ('padx', 45, ('pbga', 'havc', 'qoyq',)),
                ('tknk', 41, ('ugml', 'padx', 'fwft',)),
                ('jptl', 61, ()),
                ('ugml', 68, ('gyxo', 'ebii', 'jptl',)),
                ('gyxo', 61, ()),
                ('cntj', 57, ()))
    assert calc1(testdata) == 'tknk'
    print('part1:', calc1(data))


def calc1(data):
    nodes, tree = build_tree(data)
    return tree.name


@dataclasses.dataclass
class Node:
    name: str
    weight: int
    branch_names: list  # Used when building the tree
    branches: list
    parent: 'Node' = None
    tot_weight: int = None

def calc_weights(node):
    '''Calculate and set tot_weight recursively

    returns root nodes tot_weight'''
    tot_weight = node.weight
    for sub in node.branches:
        tot_weight += calc_weights(sub)
    node.tot_weight = tot_weight
    return tot_weight


def build_tree(data):
    nodes = {}  # nodename: Node
    for name, weight, branch_names in data:
        nodes[name] = Node(name, weight, branch_names, [])

    for node in nodes.values():
        for branch_name in node.branch_names:
            node.branches.append(nodes[branch_name])
            nodes[branch_name].parent = node  # Tell the node that node is the parent

    # lets find the root
    root = None
    for node in nodes.values():
        if not node.parent:
            root = node
            break

    calc_weights(root)
    return nodes.values(), root


def part2(data):
    testdata = (('pbga', 66, ()),
                ('xhth', 57, ()),
                ('ebii', 61, ()),
                ('havc', 66, ()),
                ('ktlj', 57, ()),
                ('fwft', 72, ('ktlj', 'cntj', 'xhth')),
                ('qoyq', 66, ()),
                ('padx', 45, ('pbga', 'havc', 'qoyq',)),
                ('tknk', 41, ('ugml', 'padx', 'fwft',)),
                ('jptl', 61, ()),
                ('ugml', 68, ('gyxo', 'ebii', 'jptl',)),
                ('gyxo', 61, ()),
                ('cntj', 57, ()))
    assert calc2(testdata) == 60
    print('part2:', calc2(data))

def find_odd_node(branches):
    try_one = list(filter(lambda x: x.tot_weight != branches[0].tot_weight, branches))
    try_two = list(filter(lambda x: x.tot_weight != branches[1].tot_weight, branches))
    if len(try_one) == 1:
        node_to_fix = try_one[0]
        #correct_tot_weight=try_two[0].tot_weight
    elif len(try_two) == 1:
        node_to_fix = try_two[0]
        #correct_tot_weight=try_one[0].tot_weight
    else:
        node_to_fix = None
    return node_to_fix


def find_faulty_node(node):

    odd_branch = find_odd_node(node.branches)
    if not odd_branch:
        return None  # it's this node and not a branch, we Use none to signal match in this case!
    res = find_faulty_node(odd_branch)
    if not res: # odd_branch IS the one needing to be fixed.
        return (node,  odd_branch)
    return res


# def calc2_fix_top_level_branch(data):
#     nodes, tree = build_tree(data)
#     tree, node = find_faulty_node(tree)
#     print(tree.name)
#     for n in tree.branches:
#         print(n.name, n.weight, n.tot_weight)


def calc2(data):
    nodes, tree = build_tree(data)
    subtree, node = find_faulty_node(tree)
    print(subtree.name)
    for n in subtree.branches:
        print(n.name, n.weight, n.tot_weight)

    try_one = list(filter(lambda x: x.tot_weight != subtree.branches[0].tot_weight, subtree.branches))
    try_two = list(filter(lambda x: x.tot_weight != subtree.branches[1].tot_weight, subtree.branches))

    if len(try_one) == 1:
        #node_to_fix = try_one[0]
        correct_tot_weight = try_two[0].tot_weight
    elif len(try_two) == 1:
        #node_to_fix = try_two[0]
        correct_tot_weight = try_one[0].tot_weight

    error = correct_tot_weight - node.tot_weight
    needed_weight = node.weight + error
    return needed_weight


# Input data readers


def read_input_str(input_file):
    return input_file.read_text().strip()


def read_input_int_list(input_file):
    return tuple([int(val) for val in input_file.open().readlines()])


def read_input_str_list(input_file):
    return tuple([val.strip()
                  for val in input_file.open().readlines() if val.strip()])


def read_input_table(input_file):
    rows = []
    for line in input_file.open():
        row = line.strip().split('\t')
        row = tuple(map(int, row))
        rows.append(row)
    return tuple(rows)


def main():
    input_file = Path(f'./{Path(sys.argv[0]).stem}.input')  # ./aoc1.input

    #data = read_input_str(input_file)
    # data = read_input_int_list(input_file)
    datai = read_input_str_list(input_file)
    # data = read_input_table(input_file)

    data = []
    for line in datai:
        import re
        match = re.match(r'([a-z]+) \(([0-9]+)\)(?: -> )?(.*)', line)
        name, weight, branches = match.groups()
        branches = tuple([branch for branch in branches.split(', ') if branch])
        res = (name, int(weight), branches)
        data.append(res)
    data = tuple(data)

    part1(data)
    part2(data)


if __name__ == '__main__':
    main()
