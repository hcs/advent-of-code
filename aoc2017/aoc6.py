#!/bin/env python3

import sys
from pathlib import Path


def part1(data):
    # assert calc1('') ==
    assert calc1((0, 2, 7, 0)) == 5
    print('part1:', calc1(data))


def calc1(data):
    banks = tuple(data)
    seen_banks = set()
    while banks not in seen_banks:
        seen_banks.update([banks])
        # print(seen_banks)
        banks = reallocate(banks)
    return len(seen_banks)


def reallocate(banks):
    banks = list(banks)

    def find_largest_index():
        lval = 0  # largest value
        lidx = 0  # largest values index
        for idx, val in enumerate(banks):
            if val > lval:
                lidx = idx
                lval = val
        return lidx

    lidx = find_largest_index()
    blocks, banks[lidx] = banks[lidx], 0
    pos = lidx
    while blocks:
        pos += 1
        if pos >= len(banks):
            pos = 0
        blocks -= 1
        banks[pos] += 1
    return tuple(banks)


def part2(data):
    # assert calc2('') ==
    assert calc2((0, 2, 7, 0)) == 4
    print('part2:', calc2(data))


def calc2(data):
    banks = tuple(data)
    seen_banks = dict()

    while banks not in seen_banks.keys():
        seen_banks[banks] = len(seen_banks)
        banks = reallocate(banks)
    return len(seen_banks) - seen_banks[banks]


# Input data readers


def read_input_str(input_file):
    return input_file.read_text().strip()


def read_input_int_list(input_file):
    return [int(val) for val in input_file.open().readlines()]


def read_input_str_list(input_file):
    return [val.strip()
            for val in input_file.open().readlines() if val.strip()]


def read_input_table(input_file):
    rows = []
    for line in input_file.open():
        row = line.strip().split('\t')
        row = list(map(int, row))
        rows.append(row)
    return rows


def main():
    input_file = Path(f'./{Path(sys.argv[0]).stem}.input')  # ./aoc1.input

    data = read_input_str(input_file)
    # data = read_input_int_list(input_file)
    # data = read_input_str_list(input_file)
    # data = read_input_table(input_file)

    data = tuple(map(int, data.split('\t')))
    part1(data)
    part2(data)


if __name__ == '__main__':
    main()
