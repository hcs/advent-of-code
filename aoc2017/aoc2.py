#!/bin/env python3
import sys
from pathlib import Path

INPUT_FILE = Path(f'./{Path(sys.argv[0]).stem}.input')  # ./aoc1.input


def read_input_str():
    return INPUT_FILE.read_text().strip()


def read_input_int_list():
    return [int(val) for val in INPUT_FILE.open().readlines()]


def read_input_table():
    rows = []
    for line in INPUT_FILE.open():
        row = line.strip().split('\t')
        row = list(map(int, row))
        rows.append(row)
    return rows


INPUT = read_input_table()
print(INPUT)


def part1():
    '''
    For each row, determine the difference between the largest value and the smallest value; the checksum is the sum of all of these differences.

For example, given the following spreadsheet:

5 1 9 5
7 5 3
2 4 6 8

    The first row's largest and smallest values are 9 and 1, and their difference is 8.
    The second row's largest and smallest values are 7 and 3, and their difference is 4.
    The third row's difference is 6.

In this example, the spreadsheet's checksum would be 8 + 4 + 6 = 18.
    '''
    assert calc1([[5, 1, 9, 5], [7, 5, 3], [2, 4, 6, 8]]) == 18
    print('part1:', calc1(INPUT))


def calc1(input):
    sum_ = 0
    for row in input:
        # print(max(row)-min(row))
        sum_ += max(row) - min(row)
    return sum_


def part2():
    '''
    '''
    # assert calc2('') ==
    print('part2:', calc2(INPUT))


def calc2(input):
    sum_ = 0
    for row in input:
        for idx in range(len(input)):
            val = row[idx]
            for idx2 in range(len(input)):
                if idx2 == idx:
                    continue
                divider = row[idx2]
                if val % divider == 0:
                    print(val, divider, val / divider)
                    sum_ += val / divider
                    break
    return sum_


if __name__ == '__main__':
    part1()
    part2()
