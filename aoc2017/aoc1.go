package main

import (
	"os"
	"fmt"
	"strconv"
	"io/ioutil"
	"strings"
)

func read_input() string {
	prog := os.Args[0]
	data, err := ioutil.ReadFile(prog + ".input")
	input := string(data)
	check(err)
	//fmt.Print(input)
	return strings.TrimSpace(input)
}

func check(e error) {
    if e != nil {
        panic(e)
    }
}

func part1(input string) int {
	var digits = []int{}
	var sum = 0
	for _, char := range input {
		d, _ := strconv.Atoi(string(char))
		digits = append(digits, d)
	}
	var digit_count = len(digits)
	digits = append(digits, digits[0])  // emulates circular list

	for i := 0; i < digit_count; i++ {
		if digits[i] == digits[i+1] {
			sum += digits[i]
		}
	}
	return sum
}

func part2(input string) int {
	var digits = []int{}
	var sum = 0
	for _, char := range input {
		d, _ := strconv.Atoi(string(char))
		digits = append(digits, d)
	}
	var digit_count = len(digits)
	digits = append(digits, digits[0])  // emulates circular list

	for i := 0; i < digit_count; i++ {
		if digits[i] == digits[(i+digit_count/2) % digit_count] {
			sum += digits[i]
		}
	}
	return sum
}

func main() {
	var input = read_input()

	if part1("1122") != 3 { panic("wrong!")}
	if part1("1111") != 4 { panic("wrong!")}
	if part1("1234") != 0 { panic("wrong!")}
	if part1("91212129") != 9 { panic("wrong!")}
	fmt.Println("Part 1:", part1(input))

	if part2("1212") != 6 { panic("wrong!")}
	if part2("1221") != 0 { panic("wrong!")}
	if part2("12131415") != 4 { panic("wrong!")}
	fmt.Println("Part 2:", part2(input))

	//for _, r := range input {
	//  d, _ := strconv.Atoi(string(r))
	//  digits = append(digits, d)
	//}

	//for i := 0; i < len(digits)-1; i++ {
	//  if digits[i] == digits[i+1] {
	//    sum += digits[i]
	//  }
	//}
	//if digits[len(digits)-1] == digits[0] {
	//  sum += digits[len(digits)-1]
	//}

	//fmt.Println("Part 1: ", sum)

	//l1 := digits[:len(digits)/2]
	//l2 := digits[len(digits)/2:]
	//sum = 0
	//for i := 0; i < len(l1); i++ {
	//  if l1[i] == l2[i] {
	//    sum += l1[i] * 2
	//  }
	//}
	//fmt.Println("Part 2: ", sum)
}
