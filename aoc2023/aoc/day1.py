import typer
from icecream import ic  # pyright: ignore # noqa: F401

from . import input_readers as ir
from . import util

app = typer.Typer()


def read_data(input_str: str | None = None):
    """read input data, pre-process it if necessary

    input_str is used for testdata.
    """
    # get the day number from the filename
    day = int(__spec__.name.removeprefix("aoc.day"))
    if input_str is None:
        input_str = ir.read_input(day)

    # return input_str.strip()
    # return ir.parse_input_int_list_multiline(input_str)
    return ir.parse_input_str_list_multiline(input_str)
    # return ir.parse_input_int_list_multiline(input_str)
    # return ir.parse_input_int_list_comma(input_str)
    # return ir.parse_input_str_table_tab(input_str)
    # return ir.parse_input_int_table_tab(input_str)


@app.command("1")
def cmd1():
    assert (
        part1(
            read_data("""\
1abc2
pqr3stu8vwx
a1b2c3d4e5f
treb7uchet""")
        )
        == 142
    )

    data = read_data()
    print("part1:", part1(data))


@util.timeme
def part1(data) -> int:
    "Do the part1 calculation"
    res = 0
    for line in data:
        line = line.strip("abcdefghijklmnopqrstuvwxyz")
        res += int(f"{line[0]}{line[-1]}")

    return res


@app.command("2")
def cmd2():
    # assert part1(test_data) ==
    assert (
        part2(
            read_data("""\
two1nine
eightwothree
abcone2threexyz
xtwone3four
4nineeightseven2
zoneight234
7pqrstsixteen""")
        )
        == 281
    )
    data = read_data()
    print("part2:", part2(data))


@util.timeme
def part2(data) -> int:
    "Do the part2 calculation"
    acceptable: dict[str, int] = {
        nstr: num
        for num, nstr in enumerate(
            [
                "one",
                "two",
                "three",
                "four",
                "five",
                "six",
                "seven",
                "eight",
                "nine",
            ],
            start=1,
        )
    }
    # add the numeric numbers
    for i in range(10):
        acceptable[str(i)] = i
    # reverse the strings for reverse searching
    acceptable_reverse = {nstr[::-1]: num for nstr, num in acceptable.items()}

    res = 0

    def find_first(accept: dict[str, int], line: str):
        for i in range(len(line)):
            for acc in accept:
                # ic(i, line[i:], accept)
                if line.startswith(acc, i):
                    return accept[acc]
        raise Exception()
        
    assert find_first(acceptable, "tnheouathree2ninesaeotuh") == 3
    assert find_first(acceptable_reverse, "tnheouathree2ninesaeotuh"[::-1]) == 9

    for line in data:
        first = find_first(acceptable, line)
        last = find_first(acceptable_reverse, line[::-1])
        res += int(f"{first}{last}")
    return res
    


if __name__ == "__main__":
    app()
