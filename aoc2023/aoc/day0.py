"""day 0 is used for template testing"""

import typer
from icecream import ic  # pyright: ignore # noqa: F401

from . import input_readers as ir
from . import util

app = typer.Typer()


def read_data(input_str: str | None = None):
    """read input data, pre-process it if necessary

    input_str is used for testdata.
    """
    # get the day number from the filename
    day = int(__spec__.name.removeprefix("aoc.day"))
    if input_str is None:
        input_str = ir.read_input(day)

    # return input_str.strip()
    return ir.parse_input_int_list_multiline(input_str)
    # return ir.parse_input_str_list_multiline(input_str)
    # return ir.parse_input_int_list_multiline(input_str)
    # return ir.parse_input_int_list_comma(input_str)
    # return ir.parse_input_str_table_tab(input_str)
    # return ir.parse_input_int_table_tab(input_str)


@app.command("1")
def cmd1():
    # assert part1(test_data) ==

    data = read_data()
    print("part1:", part1(data))


@util.timeme
def part1(data) -> int:
    "Do the part1 calculation"
    last = None
    inc_count = 0
    for val in data:
        if last is not None and val > last:
            inc_count += 1
        last = val
    return inc_count


@app.command("2")
def cmd2():
    # assert part1(test_data) ==

    data = read_data()
    print("part2:", part2(data))


@util.timeme
def part2(data) -> int:
    "Do the part2 calculation"

    last = None
    inc_count = 0

    # 3 val sliding value sum
    for i in range(len(data) - 2):
        val = data[i] + data[i + 1] + data[i + 2]
        if last is not None and val > last:
            inc_count += 1
        last = val
    return inc_count


if __name__ == "__main__":
    app()
