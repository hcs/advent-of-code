#!/bin/env python3

import functools
import time

import input_readers


def timeme(method):
    """Decorator that prints the elapsed time after a call"""

    @functools.wraps(method)
    def wrapper(*args, **kw):
        start_time = time.time()
        result = method(*args, **kw)
        end_time = time.time()
        print("timed", wrapper.__name__, end_time - start_time, "s")
        return result

    return wrapper


def main():
    "read input data, process it if necessary and call the part workers"

    input_reader = input_readers.read_input_str
    # input_reader = input_readers.read_input_int_list_multiline
    # input_reader = input_readers.read_input_str_list
    # input_reader = input_readers.read_input_table_tab

    example_data = tuple(
        map(int, input_reader(input_file=input_readers.EXAMPLE_INPUT_FILE).split(","))
    )
    data = tuple(map(int, input_reader(input_file=input_readers.INPUT_FILE).split(",")))

    part1_example = part1(example_data)
    part1_example_correct = 5934
    assert (
        part1_example == part1_example_correct
    ), f"part 1 example expected {part1_example_correct} but got {part1_example}"
    print("part1:", part1(data))

    part2_example = part2(example_data)
    part2_example_correct = 26984457539
    assert (
        part2_example == part2_example_correct
    ), f"part 2 example expected {part2_example_correct} but got {part2_example}"
    print("part2:", part2(data))


@timeme
def part1(_data):
    "Do the part1 calculation"
    state = [0] * 9  # position is number of days, value is the number of fish
    for days in _data:
        state[days] += 1

    for _day in range(1, 80 + 1):
        spawning_fish = state.pop(0)
        state[6] += spawning_fish  # reset timer to 6 for spawning_fish
        state.append(spawning_fish)  # newly spawned fish at pos 8

    return sum(state)


@timeme
def part2(_data):
    "Do the part2 calculation"
    state = [0] * 9  # position is number of days, value is the number of fish
    for days in _data:
        state[days] += 1

    for _day in range(1, 256 + 1):
        spawning_fish = state.pop(0)
        state[6] += spawning_fish  # reset timer to 6 for spawning_fish
        state.append(spawning_fish)  # newly spawned fish at pos 8

    return sum(state)


if __name__ == "__main__":
    main()
