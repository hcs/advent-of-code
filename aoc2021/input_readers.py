import sys
from pathlib import Path

INPUT_FILE = Path(f"./{Path(sys.argv[0]).stem}.input")  # ./aoc1.input
EXAMPLE_INPUT_FILE = Path(f"./{Path(sys.argv[0]).stem}.example.input")  # ./aoc1.input

# Input data readers


def read_input_str_no_strip(input_file=INPUT_FILE):
    return input_file.read_text()


def read_input_str(input_file=INPUT_FILE):
    return input_file.read_text().strip()


def read_input_int_list_multiline(input_file=INPUT_FILE):
    return tuple(
        [int(val.strip()) for val in input_file.open().readlines() if val.strip()]
    )


def read_input_int_list_comma(input_file=INPUT_FILE):
    return tuple(map(int, input_file.read_text().strip().split(",")))


def read_input_str_list(input_file=INPUT_FILE):
    return tuple([val.strip() for val in input_file.open().readlines() if val.strip()])


def read_input_table_tab(input_file=INPUT_FILE, sep="\t"):
    rows = []
    for line in input_file.open():
        row = line.strip().split(sep)
        row = tuple(map(int, row))
        rows.append(row)
    return tuple(rows)
