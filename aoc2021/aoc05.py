#!/bin/env python3

import collections
import functools
import time

import input_readers


def timeme(method):
    """Decorator that prints the elapsed time after a call"""

    @functools.wraps(method)
    def wrapper(*args, **kw):
        start_time = time.time()
        result = method(*args, **kw)
        end_time = time.time()
        print("timed", wrapper.__name__, end_time - start_time, "s")
        return result

    return wrapper


def main():
    "read input data, process it if necessary and call the part workers"

    # input_reader = input_readers.read_input_str
    # input_reader = input_readers.read_input_int_list_multiline
    #  input_reader = input_readers.read_input_str_list
    # input_reader = input_readers.read_input_table_tab
    input_reader = read_input_mapping

    example_data = input_reader(input_file=input_readers.EXAMPLE_INPUT_FILE)
    data = input_reader(input_file=input_readers.INPUT_FILE)

    part1_example = part1(example_data)
    part1_example_correct = 5
    assert (
        part1_example == part1_example_correct
    ), f"part 1 example expected {part1_example_correct} but got {part1_example}"
    print("part1:", part1(data))

    part2_example = part2(example_data)
    part2_example_correct = 12
    assert (
        part2_example == part2_example_correct
    ), f"part 2 example expected {part2_example_correct} but got {part2_example}"
    print("part2:", part2(data))


Coord = collections.namedtuple("Coord", ["x", "y"])


def read_input_mapping(
    input_file=input_readers.INPUT_FILE,
) -> tuple[tuple[int, int], tuple[int, int]]:
    rows = []
    for line in input_file.open():
        start, stop = line.strip().split(" -> ")
        startx, starty = map(int, start.split(","))
        stopx, stopy = map(int, stop.split(","))
        rows.append((Coord(startx, starty), Coord(stopx, stopy)))
    return tuple(rows)


def draw_line(line, enable_diagonals=False):
    """
    >>> list(draw_line((Coord(1,2), Coord(1,4))))
    [Coord(x=1, y=2), Coord(x=1, y=3), Coord(x=1, y=4)]
    >>> list(draw_line((Coord(1,4), Coord(1,2))))
    [Coord(x=1, y=2), Coord(x=1, y=3), Coord(x=1, y=4)]
    >>> list(draw_line((Coord(2,2), Coord(4,2))))
    [Coord(x=2, y=2), Coord(x=3, y=2), Coord(x=4, y=2)]
    >>> list(draw_line((Coord(4,2), Coord(2,2))))
    [Coord(x=2, y=2), Coord(x=3, y=2), Coord(x=4, y=2)]
    >>> list(draw_line((Coord(1,2), Coord(2,3))))
    []
    >>> list(draw_line((Coord(1,2), Coord(2,3)), enable_diagonals=True))
    [Coord(x=1, y=2), Coord(x=2, y=3)]
    >>> list(draw_line((Coord(2,3), Coord(1,2)), enable_diagonals=True))
    [Coord(x=2, y=3), Coord(x=1, y=2)]
    >>> list(draw_line((Coord(2,1), Coord(3,2)), enable_diagonals=True))
    [Coord(x=2, y=1), Coord(x=3, y=2)]
    >>> list(draw_line((Coord(3,2), Coord(2,1)), enable_diagonals=True))
    [Coord(x=3, y=2), Coord(x=2, y=1)]

    """
    start, stop = line
    if start.x == stop.x:
        miny, maxy = sorted((start.y, stop.y))
        for y in range(miny, maxy + 1):
            yield Coord(start.x, y)
    elif start.y == stop.y:
        minx, maxx = sorted((start.x, stop.x))
        for x in range(minx, maxx + 1):
            yield Coord(x, start.y)
    elif enable_diagonals:
        xstep = 1 if start.x < stop.x else -1
        ystep = 1 if start.y < stop.y else -1
        current = start
        line_length = 0
        yield current
        while current != stop:
            line_length += 1
            current = Coord(
                start.x + line_length * xstep, start.y + line_length * ystep
            )
            yield current


@timeme
def part1(data):
    "Do the part1 calculation"
    counts = collections.Counter()  # counts how many lines passes thru a point
    for line in data:
        counts.update(draw_line(line))
    return len([val for val in counts.values() if val > 1])


@timeme
def part2(data):
    "Do the part2 calculation"
    counts = collections.Counter()  # counts how many lines passes thru a point
    for line in data:
        counts.update(draw_line(line, enable_diagonals=True))
    return len([val for val in counts.values() if val > 1])


if __name__ == "__main__":
    main()
