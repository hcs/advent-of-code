#!/bin/env python3

import functools
import time

import input_readers


def timeme(method):
    """Decorator that prints the elapsed time after a call"""

    @functools.wraps(method)
    def wrapper(*args, **kw):
        start_time = time.time()
        result = method(*args, **kw)
        end_time = time.time()
        print("timed", wrapper.__name__, end_time - start_time, "s")
        return result

    return wrapper


def main():
    "read input data, process it if necessary and call the part workers"

    # input_reader = input_readers.read_input_str
    # input_reader = input_readers.read_input_int_list_multiline
    # input_reader = input_readers.read_input_int_list_comma
    input_reader = input_readers.read_input_str_list
    # input_reader = input_readers.read_input_table_tab

    example_data = input_reader(input_file=input_readers.EXAMPLE_INPUT_FILE)
    data = input_reader(input_file=input_readers.INPUT_FILE)

    part1_example = part1(example_data)
    part1_example_correct = 26397
    assert (
        part1_example == part1_example_correct
    ), f"part 1 example expected {part1_example_correct} but got {part1_example}"
    print("part1:", part1(data))

    part2_example = part2(example_data)
    part2_example_correct = 288957
    assert (
        part2_example == part2_example_correct
    ), f"part 2 example expected {part2_example_correct} but got {part2_example}"
    print("part2:", part2(data))


PAIRS = dict(("()", "[]", "{}", "<>"))
OPENERS = PAIRS.keys()
#  CLOSERS = PAIRS.values()
POINTS1 = {")": 3, "]": 57, "}": 1197, ">": 25137}
POINTS2 = {")": 1, "]": 2, "}": 3, ">": 4}


@timeme
def part1(data):
    "Do the part1 calculation"
    res = 0  # sum of points
    for line in data:
        active_openers = []  # Used a stack
        for char in line:
            if char in OPENERS:
                active_openers.append(char)
            else:
                expects = PAIRS[active_openers.pop()]
                if char != expects:
                    # char in not the correct closer
                    #  print(f"expected {expects} but got {char}")
                    res += POINTS1[char]
    return res


@timeme
def part2(data):
    "Do the part2 calculation"
    scores = []
    for line in data:
        active_openers = []  # Used a stack
        for char in line:
            if char in OPENERS:
                active_openers.append(char)
            else:
                expects = PAIRS[active_openers.pop()]
                if char != expects:
                    # char in not the correct closer
                    #  print(f"expected {expects} but got {char}")
                    active_openers = None  # to signal we should ignore this line
                    break
        if active_openers:
            line_points = 0
            for char in reversed(active_openers):
                closer = PAIRS[char]
                points = POINTS2[closer]
                line_points = line_points * 5 + points
            #  print(line_points)
            scores.append(line_points)
            #  res += line_points
    scores.sort()
    middle_score = scores[int(len(scores) / 2)]

    return middle_score


if __name__ == "__main__":
    main()
