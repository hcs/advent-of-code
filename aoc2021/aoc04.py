#!/bin/env python3

import functools
import time

import input_readers


def timeme(method):
    """Decorator that prints the elapsed time after a call"""

    @functools.wraps(method)
    def wrapper(*args, **kw):
        start_time = time.time()
        result = method(*args, **kw)
        end_time = time.time()
        print("timed", wrapper.__name__, end_time - start_time, "s")
        return result

    return wrapper


def main():
    "read input data, process it if necessary and call the part workers"

    input_reader = input_readers.read_input_str
    # input_reader = input_readers.read_input_int_list_multiline
    # input_reader = input_readers.read_input_str_list
    # input_reader = input_readers.read_input_table_tab

    example_rdata = input_reader(input_file=input_readers.EXAMPLE_INPUT_FILE)
    rdata = input_reader(input_file=input_readers.INPUT_FILE)

    example_data = parse(example_rdata)
    data = parse(rdata)

    part1_example = part1(example_data)
    part1_example_correct = 4512
    assert (
        part1_example == part1_example_correct
    ), f"part 1 example expected {part1_example_correct} but got {part1_example}"
    print("part1:", part1(data))

    example_data = parse(example_rdata)
    data = parse(rdata)

    part2_example = part2(example_data)
    part2_example_correct = 1924
    assert (
        part2_example == part2_example_correct
    ), f"part 2 example expected {part2_example_correct} but got {part2_example}"
    print("part2:", part2(data))


class BingoCard:
    def __init__(self, table):
        self.table = table
        assert len(table) == 5
        for row in table:
            assert len(row) == 5
        self.row_mark_count: list[int] = [0] * 5
        self.col_mark_count: list[int] = [0] * 5
        self.marked = []  # positions that has been marked

    def mark(self, num):
        match self.find(num):
            case (row, col):
                self.row_mark_count[row] += 1
                self.col_mark_count[col] += 1
                self.marked.append((row, col))

    def winner(self):
        for count in self.row_mark_count + self.col_mark_count:
            if count == 5:
                return True
        return False

    def find(self, num):
        for row in range(5):
            for col in range(5):
                if self.table[row][col] == num:
                    return row, col

        return None

    def get_all_values(self):
        """
        >>> bc = BingoCard([[22, 13, 17, 11, 0], [8, 2, 23, 4, 24], \
        [21, 9, 14, 16, 7], [6, 10, 3, 18, 5], [1, 12, 20, 15, 19]])
        >>> bc.get_all_values()
        [22, 13, 17, 11, 0, 8, 2, 23, 4, 24, 21, 9, 14, 16, 7, 6, 10, \
3, 18, 5, 1, 12, 20, 15, 19]
        """
        return [val for row in self.table for val in row]

    def __repr__(self):
        return repr(self.table)

    def __str__(self):
        return (
            "\n"
            + "\n".join([" ".join(map(str, row)) for row in self.table])
            + f"     {self.row_mark_count} {self.col_mark_count}"
        )


def parse(data):
    """returns a dict"""
    data = data.splitlines()
    tables = []
    res_data = dict(random_data=list(map(int, data.pop(0).split(","))), cards=None)

    table = None
    for line in data:
        if not line:
            table = []
            tables.append(table)
        else:
            table.append(list(map(int, line.split())))

    res_data["cards"] = list(map(BingoCard, tables))

    return res_data


@timeme
def part1(data):
    "Do the part1 calculation"
    called_numbers = []
    for num in data["random_data"]:
        called_numbers.append(num)
        for card in data["cards"]:
            card.mark(num)
            if card.winner():
                # calculate score
                unmarked = set(card.get_all_values()) - set(called_numbers)
                score = sum(unmarked) * num
                return score


@timeme
def part2(data):
    "Do the part2 calculation"
    called_numbers = []
    winning_boards = set()
    for num in data["random_data"]:
        print("called", num)
        called_numbers.append(num)
        for card_num, card in enumerate(data["cards"]):
            #  print(card_num, card)
            if card_num in winning_boards:
                continue
            card.mark(num)
            if card.winner():
                print(f"card {card_num} won")
                #  print(card)
                winning_boards.add(card_num)
                if len(winning_boards) == len(data["cards"]):  # last bord to win
                    print(
                        f"Last card to win is {card_num}. Called numbers: "
                        + f"{called_numbers}, previous winning boards: {winning_boards}"
                    )
                    # calculate score
                    unmarked = set(card.get_all_values()) - set(called_numbers)
                    score = sum(unmarked) * num
                    return score


if __name__ == "__main__":
    main()
