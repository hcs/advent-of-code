#!/bin/env python3

import functools
import time

import input_readers
from utils import print_board


def timeme(method):
    """Decorator that prints the elapsed time after a call"""

    @functools.wraps(method)
    def wrapper(*args, **kw):
        start_time = time.time()
        result = method(*args, **kw)
        end_time = time.time()
        print("timed", wrapper.__name__, end_time - start_time, "s")
        return result

    return wrapper


def main():
    "read input data, process it if necessary and call the part workers"

    # input_reader = input_readers.read_input_str
    # input_reader = input_readers.read_input_int_list_multiline
    # input_reader = input_readers.read_input_int_list_comma
    input_reader = input_readers.read_input_str_list
    # input_reader = input_readers.read_input_table_tab

    example_data = input_reader(input_file=input_readers.EXAMPLE_INPUT_FILE)
    data = input_reader(input_file=input_readers.INPUT_FILE)

    part1_example = part1(example_data)
    part1_example_correct = 17
    assert (
        part1_example == part1_example_correct
    ), f"part 1 example expected {part1_example_correct} but got {part1_example}"
    print("part1:", part1(data))

    part2_example = part2(example_data)
    part2_example_correct = {
        (0, 1),
        (4, 4),
        (2, 4),
        (4, 0),
        (0, 4),
        (3, 4),
        (4, 3),
        (0, 0),
        (0, 3),
        (2, 0),
        (4, 2),
        (1, 4),
        (3, 0),
        (0, 2),
        (1, 0),
        (4, 1),
    }
    assert (
        part2_example == part2_example_correct
    ), f"part 2 example expected {part2_example_correct} but got {part2_example}"
    print("part2:", part2(data))


@timeme
def part1(data):
    "Do the part1 calculation"
    coords = set()
    fold_along = []  # [(axis, int)]
    for line in data:
        if not line:
            continue
        if line.startswith("fold"):
            axis, num = tuple(line.split(" ")[2].split("="))
            fold_along.append((axis, int(num)))
        else:
            x, y = tuple(map(int, line.split(",")))
            coords.add((x, y))

    for axis, num in fold_along:
        folded_coords = set()
        for x, y in coords:
            if axis == "y":
                if y > num:
                    y = num - (y - num)
            else:
                if x > num:
                    x = num - (x - num)
            folded_coords.add((x, y))
        coords = folded_coords
        break  # quit after first fold once
    return len(coords)


@timeme
def part2(data):
    "Do the part2 calculation"
    coords = set()
    fold_along = []  # [(axis, int)]
    for line in data:
        if not line:
            continue
        if line.startswith("fold"):
            axis, num = tuple(line.split(" ")[2].split("="))
            fold_along.append((axis, int(num)))
        else:
            x, y = tuple(map(int, line.split(",")))
            coords.add((x, y))

    for axis, num in fold_along:
        folded_coords = set()
        for x, y in coords:
            if axis == "y":
                if y > num:
                    y = num - (y - num)
            else:
                if x > num:
                    x = num - (x - num)
            folded_coords.add((x, y))
        coords = folded_coords

    print_board({coor: "x" for coor in coords})
    return coords


if __name__ == "__main__":
    main()
