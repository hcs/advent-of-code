#!/bin/env python3

import collections
import functools
import itertools
import time

import input_readers


def timeme(method):
    """Decorator that prints the elapsed time after a call"""

    @functools.wraps(method)
    def wrapper(*args, **kw):
        start_time = time.time()
        result = method(*args, **kw)
        end_time = time.time()
        print("timed", wrapper.__name__, end_time - start_time, "s")
        return result

    return wrapper


def main():
    "read input data, process it if necessary and call the part workers"

    # input_reader = input_readers.read_input_str
    # input_reader = input_readers.read_input_int_list_multiline
    # input_reader = input_readers.read_input_int_list_comma
    input_reader = input_readers.read_input_str_list
    # input_reader = input_readers.read_input_table_tab

    example_data = input_reader(input_file=input_readers.EXAMPLE_INPUT_FILE)
    data = input_reader(input_file=input_readers.INPUT_FILE)

    part1_example = part1(example_data)
    part1_example_correct = 1588
    assert (
        part1_example == part1_example_correct
    ), f"part 1 example expected {part1_example_correct} but got {part1_example}"
    print("part1:", part1(data))

    part2_example = part2(example_data)
    part2_example_correct = 2188189693529
    assert (
        part2_example == part2_example_correct
    ), f"part 2 example expected {part2_example_correct} but got {part2_example}"
    print("part2:", part2(data))


@timeme
def part1(data):
    "Do the part1 calculation"
    template = None
    rules = {}
    for line in data:
        if template is None:  # first row in data
            template = line
            continue
        elif not line:
            continue
        from_, to = line.split(" -> ")
        rules[from_] = to

    #  print(template)
    #  print(rules)
    polymer = list(template)
    #  print(0, polymer)
    for _step in range(1, 11):
        last = None
        polymer2 = []
        for val in polymer:
            if last is None:
                polymer2.append(val)
            else:
                insert = rules.get(f"{last}{val}")
                if insert:
                    polymer2.append(insert)
                polymer2.append(val)
            last = val
        polymer = polymer2
        #  print(_step, polymer)
    #  print(polymer)
    counts = collections.Counter(polymer)
    mc = counts.most_common()
    #  print(mc)
    return mc[0][1] - mc[-1][1]


@timeme
def part2(data):
    "Do the part2 calculation"

    # TODO needs extreame optimization to work

    template = None
    rules = {}
    for line in data:
        if template is None:  # first row in data
            template = line
            continue
        elif not line:
            continue
        from_, insert = line.split(" -> ")
        to = [f"{from_[0]}{insert}", f"{insert}{from_[1]}"]
        rules[from_] = to

    #  print(template)
    #  print(rules)

    pairs = collections.Counter("".join(pair) for pair in itertools.pairwise(template))
    #  last = None
    #  for val in template:
    #      if last is not None:
    #          pairs.update([f"{last}{val}"])
    #      last = val
    # NCNBCHB
    # NBCCNBBBCBHCB
    counts = collections.Counter(template)
    for _step in range(1, 41):
        pairs2 = collections.Counter()
        for pair, pcount in pairs.items():
            pair1, pair2 = rules.get(pair)
            pairs2[pair1] += pcount
            pairs2[pair2] += pcount
            counts[pair1[1]] += pcount
        pairs = pairs2
        #  print(_step, pairs)
        #  print(_step, counts)

    mc = counts.most_common()
    #  print("most common", mc)
    return mc[0][1] - mc[-1][1]


if __name__ == "__main__":
    main()
