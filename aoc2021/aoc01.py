#!/bin/env python3

import functools
import time

import input_readers


def timeme(method):
    """Decorator that prints the elapsed time after a call"""

    @functools.wraps(method)
    def wrapper(*args, **kw):
        start_time = time.time()
        result = method(*args, **kw)
        end_time = time.time()
        print("timed", wrapper.__name__, end_time - start_time, "s")
        return result

    return wrapper


def main():
    "read input data, process it if necessary and call the part workers"
    # data = input_readers.read_input_str()
    data = input_readers.read_input_int_list_multiline()
    # data = input_readers.read_input_str_list()
    # data = input_readers.read_input_table_tab()

    # assert part1('') ==
    print("part1:", part1(data))

    # assert part2('') ==
    print("part2:", part2(data))


@timeme
def part1(_data):
    "Do the part1 calculation"
    last = None
    inc_count = 0
    for val in _data:
        if last is not None and val > last:
            inc_count += 1
        last = val
    return inc_count


@timeme
def part2(_data):
    "Do the part2 calculation"

    last = None
    inc_count = 0

    # 3 val sliding value sum
    for i in range(len(_data) - 2):
        val = _data[i] + _data[i + 1] + _data[i + 2]
        if last is not None and val > last:
            inc_count += 1
        last = val
    return inc_count


if __name__ == "__main__":
    main()
