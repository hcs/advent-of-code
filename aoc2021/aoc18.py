#!/bin/env python3


import functools
import time
from dataclasses import dataclass

import input_readers

SCAF = False  # Deactivate scaffolding,


def timeme(method):
    """Decorator that prints the elapsed time after a call"""

    @functools.wraps(method)
    def wrapper(*args, **kw):
        start_time = time.time()
        result = method(*args, **kw)
        end_time = time.time()
        print("timed", wrapper.__name__, end_time - start_time, "s")
        return result

    return wrapper


def main():
    "read input data, process it if necessary and call the part workers"

    # input_reader = input_readers.read_input_str
    # input_reader = input_readers.read_input_int_list_multiline
    # input_reader = input_readers.read_input_int_list_comma
    input_reader = input_readers.read_input_str_list
    # input_reader = input_readers.read_input_table_tab

    example_data = input_reader(input_file=input_readers.EXAMPLE_INPUT_FILE)
    data = input_reader(input_file=input_readers.INPUT_FILE)

    part1_example = part1(example_data)
    part1_example_correct = 4140
    assert (
        part1_example == part1_example_correct
    ), f"part 1 example expected {part1_example_correct} but got {part1_example}"
    print("part1:", part1(data))

    part2_example = part2(example_data)
    part2_example_correct = 42
    assert (
        part2_example == part2_example_correct
    ), f"part 2 example expected {part2_example_correct} but got {part2_example}"
    print("part2:", part2(data))


@dataclass
class Value:
    parent: "Value"
    val1: "Value"
    val2: "Value"
    level: int

    def magnitude(
        self,
    ) -> int:
        """
        # magnitude
        >>> dtov([[1,2],[[3,4],5]]).magnitude()
        143
        >>> dtov([[[[8,7],[7,7]],[[8,6],[7,7]]],[[[0,7],[6,6]],[8,7]]]).magnitude()
        3488
        """

        mag1 = self.val1 if isinstance(self.val1, int) else self.val1.magnitude()
        mag2 = self.val2 if isinstance(self.val2, int) else self.val2.magnitude()
        return mag1 * 3 + mag2 * 2

    def walk(self, cmd):
        """
        >>> def walker(val):
        ...   print(val.to_list())
        ...   return True
        >>> dtov([[1,2],[[3,4],5]]).walk(walker)
        [[1, 2], [[3, 4], 5]]
        [1, 2]
        [[3, 4], 5]
        [3, 4]





        # TODO gets more
        # walk
        #  [[9, 8], 1]
        """
        if cmd(self) is False:  # exit if cmd returns False
            return
        if not isinstance(self.val1, int):
            self.val1.walk(cmd)
        if not isinstance(self.val2, int):
            self.val2.walk(cmd)

    def to_list(self):
        v1 = self.val1 if isinstance(self.val1, int) else self.val1.to_list()
        v2 = self.val2 if isinstance(self.val2, int) else self.val2.to_list()
        return [v1, v2]

    def __str__(self):
        return f"V{self.level}({self.val1}, {self.val2})"

    #  def __repr__(self):
    #      return repr(self.to_list())

    def __add__(self, other: "Value") -> list:
        """
        >>> (dtov([1, 2]) + dtov([3, 4])).to_list()
        [[1, 2], [3, 4]]
        >>> (dtov([[[1, 1], 2], 3]) + dtov([6, 6])).to_list()
        [[[[1, 1], 2], 3], [6, 6]]

        #  >>> (dtov([[[[1, 1], 2], 3], 4]) + dtov([6, 6])).to_list()
        #  [[[[[1, 1], 2], 3], 4], [6, 6]]  # TODO needs reduce
        #  [[[[0, 3], 3], 4], [6, 6]]
        >>> (dtov([[[0, [4, 5]], [0, 0]], [[[4, 5], [2, 6]], [9, 5]]]) + \
             dtov([7, [[[3, 7], [4, 3]], [[6, 3], [8, 8]]]])).to_list()
        [[[[4, 0], [5, 4]], [[7, 7], [6, 0]]], [[8, [7, 7]], [[7, 9], [5, 0]]]])
        """
        if SCAF:
            print("add", self.to_list(), "  +  ", other.to_list())
            print("add", self, "  +  ", other)
        val = Value(None, self, other, level=0)
        self.parent = val
        other.parent = val

        def increase_level(vval):
            vval.level += 1

        val.walk(increase_level)
        val.reduce()
        return val

    def explode_val1(self):
        """Call on the level 4 value whose val1 should explode."""
        val = self
        assert self.level == 4, f"shold only be called on level 4 value, not {self}"
        assert isinstance(self.val1, Value)
        add_left = val.val1.val1
        add_right = val.val1.val2
        assert isinstance(add_left, int)
        assert isinstance(add_right, int)
        val.val1 = 0
        do_add_left(val, add_left)
        if isinstance(val.val2, int):
            val.val2 += add_right
            if val.val2 > 9:
                val.split_val2()
        else:
            do_add_right(val, add_right)  # walk to find and add

    def explode_val2(self):
        val = self
        assert self.level == 4
        assert isinstance(self.val1, int)
        assert isinstance(self.val2, Value)
        add_left = val.val2.val1
        add_right = val.val2.val2
        assert isinstance(add_left, int)
        assert isinstance(add_right, int), add_right
        val.val2 = 0
        if isinstance(val.val1, int):
            val.val1 += add_left
            if val.val1 > 9:
                val.split_val1()
        else:
            do_add_left(val, add_left)  # walk to find and add
        do_add_right(val, add_right)

    def reduce(self):
        """
        >>> dtov([[[[[9, 8], 1], 2], 3], 4], debug_allow_level5=True).reduce().to_list()
        [[[[0, 9], 2], 3], 4]
        >>> dtov([[[[[9, 9], 1], 2], 3], 4], debug_allow_level5=True).reduce().to_list()
        [[[[5, 0], 7], 3], 4]
        """
        if SCAF:
            print("reduce", self)
        exploded = True  # To start loop

        def process(val):
            nonlocal exploded
            assert val.level < 5
            if val.level == 4:
                if isinstance(val.val1, Value):  # Explodes
                    #  add_left = val.val1.val1
                    #  add_right = val.val1.val2
                    #  assert isinstance(add_left, int)
                    #  assert isinstance(add_right, int)
                    #  val.val1 = 0
                    #  do_add_left(val, add_left)
                    #  if isinstance(val.val2, int):
                    #      val.val2 += add_right
                    #      add_right = 0
                    #  else:
                    #      do_add_right(val, add_right) # walk to find and add
                    val.explode_val1()
                    exploded = True
                    return False  # Stop walking
                if isinstance(val.val2, Value):  # Explodes
                    #  add_left = val.val2.val1
                    #  add_right = val.val2.val2
                    #  assert isinstance(add_left, int)
                    #  assert isinstance(add_right, int)
                    #  val.val2 = 0
                    #  if isinstance(val.val1, int):
                    #      val.val1 += add_left
                    #      add_left = 0
                    #  else:
                    #      do_add_left(val, add_left) # walk to find and add
                    #  do_add_right(val, add_right)
                    val.explode_val2()
                    exploded = True
                    return False  # Stop walking
            exploded = False
            return True  # continue walking

        while exploded:
            self.walk(process)

        return self

        #  #  print("reduce", val, count_levels(val))
        #  exploded = True  # so the loop starts
        #  while exploded:
        #      #  print("explode", val, count_levels(val))
        #      val, _, _, exploded = explode_rec(val)
        #      splitted = True  # so the loop starts
        #      while splitted:
        #          #  print("split", val, count_levels(val))
        #          val, splitted = _split_rec(val)

        #  return val

    def split_val1(self):
        val = self
        assert self.val1 > 9
        assert val.level < 5
        num = val.val1
        val.val1 = Value(val, num // 2, num - num // 2, level=val.level + 1)
        if val.level == 4:  # newly created Value explodes
            val.explode_val1()

    def split_val2(self):
        val = self
        assert self.val2 > 9
        #  assert val.level < 5, self.get_root()
        num = val.val2
        val.val2 = Value(val, num // 2, num - num // 2, level=val.level + 1)
        if val.level == 4:  # newly created Value explodes
            val.explode_val2()

    def get_root(self):
        val = self
        while val.parent is not None:
            val = val.parent
        return val


def do_add_right(val: Value, how_mutch: int):
    while val.parent is not None and val is val.parent.val2:  # go up
        # as long as we are the rightmost value we need to go further up
        val = val.parent
    if val.parent is None:
        # there was no value to the right
        return
    val = val.parent  # Take a last step up
    if isinstance(val.val2, int):
        val.val2 += how_mutch
        # check if we breached 10 and needs to explode
        if val.val2 > 9:
            val.split_val2()
            #  # split
            #  num = val.val2
            #  val.val2 = Value(val, num // 2, num - num // 2, level=val.level + 1)
            #  if val.level == 4:  # newly created Value explodes
            #      val.explode_val2()
    else:
        val = val.val2  # Take a step down
        while isinstance(val.val1, Value):
            val = val.val1
        val.val1 += how_mutch
        if val.val1 > 9:
            val.split_val1()


def do_add_left(val: Value, how_mutch: int):
    while val.parent is not None and val is val.parent.val1:  # go up
        # as long as we are the leftmost value we need to go further up
        val = val.parent
    if val.parent is None:
        # there was no value to the left
        return
    val = val.parent  # Take a last step up
    if isinstance(val.val1, int):
        val.val1 += how_mutch
        # check if we breached 10 and needs to explode
        if val.val1 > 9:
            val.split_val1()
            #  # split
            #  num = val.val1
            #  val.val1 = Value(val, num // 2, num - num // 2, level=val.level + 1)
            #  if val.level == 4:  # newly created Value explodes
            #      val.explode_val1()
    else:
        val = val.val1  # Take a step down
        while isinstance(val.val2, Value):
            val = val.val2
        val.val2 += how_mutch
        if val.val2 > 9:
            val.split_val2()


#  def add_to_leftmostOLD(val: list, how_mutch: int) -> int:
#      """add how_mutch to LEFTMOST value in val

#      >>> add_to_leftmost(2, 1)
#      3
#      >>> add_to_leftmost([1, 1], 1)
#      [2, 1]
#      >>> add_to_leftmost([[[[[9, 8], 1], 2], 3], 4], 1)
#      [[[[[10, 8], 1], 2], 3], 4]
#      >>> add_to_leftmost([7, [6, [5, [4, [3, 2]]]]], 1)
#      [8, [6, [5, [4, [3, 2]]]]]
#      >>> add_to_leftmost([[6, [5, [4, [3, 2]]]], 1], 1)
#      [[7, [5, [4, [3, 2]]]], 1]
#      >>> add_to_leftmost([[3, [2, [1, [7, 3]]]], [6, [5, [4, [3, 2]]]]], 1)
#      [[4, [2, [1, [7, 3]]]], [6, [5, [4, [3, 2]]]]]
#      >>> add_to_leftmost([[3, [2, [8, 0]]], [9, [5, [4, [3, 2]]]]], 1)
#      [[4, [2, [8, 0]]], [9, [5, [4, [3, 2]]]]]
#      """

#      if isinstance(val, int):
#          return val + how_mutch
#      val1, val2 = val
#      return [add_to_leftmost(val1, how_mutch), val2]


#  def add_to_rightmostOLD(val: list, how_mutch: int) -> int:
#      """add how_mutch to RIGHTMOST value in val

#      >>> add_to_rightmost(2, 1)
#      3
#      >>> add_to_rightmost([1, 1], 1)
#      [1, 2]
#      >>> add_to_rightmost([[[[[9, 8], 1], 2], 3], 4], 1)
#      [[[[[9, 8], 1], 2], 3], 5]
#      >>> add_to_rightmost([7, [6, [5, [4, [3, 2]]]]], 1)
#      [7, [6, [5, [4, [3, 3]]]]]
#      >>> add_to_rightmost([[6, [5, [4, [3, 2]]]], 1], 1)
#      [[6, [5, [4, [3, 2]]]], 2]
#      >>> add_to_rightmost([[3, [2, [1, [7, 3]]]], [6, [5, [4, [3, 2]]]]], 1)
#      [[3, [2, [1, [7, 3]]]], [6, [5, [4, [3, 3]]]]]
#      >>> add_to_rightmost([[3, [2, [8, 0]]], [9, [5, [4, [3, 2]]]]], 1)
#      [[3, [2, [8, 0]]], [9, [5, [4, [3, 3]]]]]
#      """
#      if isinstance(val, int):
#          return val + how_mutch
#      val1, val2 = val
#      return [val1, add_to_rightmost(val2, how_mutch)]


#  def count_levelsOLD(val):
#      if isinstance(val, int):
#          return 0
#      levels = [count_levels(val2) for val2 in val]
#      return max(levels) + 1


#  def snailfish_addOLD(val1: list, val2: list) -> list:
#      """
#      >>> snailfish_add([[[0,[4,5]],[0,0]],[[[4,5],[2,6]],[9,5]]], \
#                        [7,[[[3,7],[4,3]],[[6,3],[8,8]]]])
#      [[[[4,0],[5,4]],[[7,7],[6,0]]],[[8,[7,7]],[[7,9],[5,0]]]])
#      """
#      print("add", val1, val2, count_levels(val1), count_levels(val2))
#      res = [val1, val2]
#      res = reduce(res)
#      return res


#  def reduceOLD(val: list) -> list:
#      """
#      >>> reduce([[[[[9, 9], 1], 2], 3], 4])
#      [[[[5, 0], 7], 3], 4]
#      """
#      print("reduce", val, count_levels(val))
#      exploded = True  # so the loop starts
#      while exploded:
#          print("explode", val, count_levels(val))
#          val, _, _, exploded = explode_rec(val)
#          splitted = True  # so the loop starts
#          while splitted:
#              print("split", val, count_levels(val))
#              val, splitted = _split_rec(val)

#      return val

#  if isinstance(val, int):
#      if val > 9:
#          return [val // 2, val - val // 2], True

#  val1, val2 = val
#  reduced = False

#  val1, reduced = reduce(val1, level=level + 1)
#  if not reduced:
#      val2, reduced = reduce(val2, level=level + 1)
#  return [val1, val2], reduced


#  def explode(val):
#      val, _add_to_left, _add_to_right, exploded = explode_rec(val)
#      if exploded:
#          (val,) = split(val)
#      return val


#  def split(val):
#      val, _splitted = _split_rec(val)


#  def _split_recOLD(val, level=1):
#      """
#      >>> _split_rec(9)
#      (9, False)
#      >>> _split_rec(10)
#      ([5, 5], True)
#      >>> _split_rec(11)
#      ([5, 6], True)
#      >>> _split_rec(12)
#      ([6, 6], True)
#      >>> _split_rec([1, [2, 11]])
#      ([1, [2, [5, 6]]], True)
#      >>> _split_rec([1, [2, 1]])
#      ([1, [2, 1]], False)
#      """

#      if isinstance(val, int):
#          if val > 9:
#              return [val // 2, val - val // 2], True
#          return val, False
#      val1, val2 = val
#      val1, splitted1 = _split_rec(val1, level=level + 1)
#      #  if splitted1 and level == 4:  # a new explodable was created
#      #      explode_rec(val, level)
#      if not splitted1:
#          val2, splitted2 = _split_rec(val2, level=level + 1)
#      return [val1, val2], splitted1 or splitted2


#  def explode_recOLD(
#      val, add_to_left=0, add_to_right=0, level=1
#  ) -> tuple[list, int, int]:
#      """
#      >>> explode_rec([[[[9, 8], 1], 2], 3])
#      ([[[[9, 8], 1], 2], 3], 0, 0, False)
#      >>> explode_rec([[[[[9, 9], 1], 2], 3], 4])
#      ([[[[0, 10], 2], 3], 4], 9, 0, True)
#      >>> explode_rec([7, [6, [5, [4, [3, 2]]]]])
#      ([7, [6, [5, [7, 0]]]], 0, 2, True)
#      >>> explode_rec([[6, [5, [4, [3, 2]]]], 1])
#      ([[6, [5, [7, 0]]], 3], 0, 0, True)
#      >>> explode_rec([[3, [2, [1, [7, 3]]]], [6, [5, [4, [3, 2]]]]])
#      ([[3, [2, [8, 0]]], [9, [5, [4, [3, 2]]]]], 0, 0, True)
#      >>> explode_rec([[3, [2, [8, 0]]], [9, [5, [4, [3, 2]]]]])
#      ([[3, [2, [8, 0]]], [9, [5, [7, 0]]]], 0, 2, True)
#      """
#      if isinstance(val, int):
#          return val, 0, 0, False
#      val1, val2 = val
#      exploded = False
#      if level > 4:
#          assert isinstance(val1, int), val
#          assert isinstance(val2, int), val
#          add_to_left = val1
#          add_to_right = val2
#          exploded = True
#          return 0, add_to_left, add_to_right, exploded

#          #  if isinstance(val1, list):
#          #      # explode
#          #      assert isinstance(val1, int)
#          #      val2 = add_to_leftmost(val2, val1)
#          #      add_to_left = val1
#          #      add_to_right = 0  # done
#          #      val1 = 0
#          #      exploded = True
#          #  elif isinstance(val2, list):
#          #      # explode
#          #      assert isinstance(val2, int)
#          #      val1 = add_to_rightmost(val1, val2)
#          #      add_to_left = 0  # done
#          #      add_to_right = val2
#          #      val2 = 0
#          #      exploded = True
#      else:
#          val1, add_to_left, add_to_right, exploded = explode_rec(val1, level=level +1)
#          if exploded:
#              if add_to_right:
#                  val2 = add_to_leftmost(val2, add_to_right)
#                  add_to_right = 0
#              # add_to_left is returned upward
#          else:  # val1 didn't explode
#              val2, add_to_left, add_to_right, exploded = explode_rec(
#                  val2, level=level + 1
#              )
#              if exploded:
#                  if add_to_left:
#                      val1 = add_to_rightmost(val1, add_to_left)
#                      add_to_left = 0
#                  # add_to_right is returned upward

#      return [val1, val2], add_to_left, add_to_right, exploded


#  def add_to_leftmostOLD(val: list, how_mutch: int) -> int:
#      """add how_mutch to LEFTMOST value in val

#      >>> add_to_leftmost(2, 1)
#      3
#      >>> add_to_leftmost([1, 1], 1)
#      [2, 1]
#      >>> add_to_leftmost([[[[[9, 8], 1], 2], 3], 4], 1)
#      [[[[[10, 8], 1], 2], 3], 4]
#      >>> add_to_leftmost([7, [6, [5, [4, [3, 2]]]]], 1)
#      [8, [6, [5, [4, [3, 2]]]]]
#      >>> add_to_leftmost([[6, [5, [4, [3, 2]]]], 1], 1)
#      [[7, [5, [4, [3, 2]]]], 1]
#      >>> add_to_leftmost([[3, [2, [1, [7, 3]]]], [6, [5, [4, [3, 2]]]]], 1)
#      [[4, [2, [1, [7, 3]]]], [6, [5, [4, [3, 2]]]]]
#      >>> add_to_leftmost([[3, [2, [8, 0]]], [9, [5, [4, [3, 2]]]]], 1)
#      [[4, [2, [8, 0]]], [9, [5, [4, [3, 2]]]]]
#      """

#      if isinstance(val, int):
#          return val + how_mutch
#      val1, val2 = val
#      return [add_to_leftmost(val1, how_mutch), val2]


#  def add_to_rightmostOLD(val: list, how_mutch: int) -> int:
#      """add how_mutch to RIGHTMOST value in val

#      >>> add_to_rightmost(2, 1)
#      3
#      >>> add_to_rightmost([1, 1], 1)
#      [1, 2]
#      >>> add_to_rightmost([[[[[9, 8], 1], 2], 3], 4], 1)
#      [[[[[9, 8], 1], 2], 3], 5]
#      >>> add_to_rightmost([7, [6, [5, [4, [3, 2]]]]], 1)
#      [7, [6, [5, [4, [3, 3]]]]]
#      >>> add_to_rightmost([[6, [5, [4, [3, 2]]]], 1], 1)
#      [[6, [5, [4, [3, 2]]]], 2]
#      >>> add_to_rightmost([[3, [2, [1, [7, 3]]]], [6, [5, [4, [3, 2]]]]], 1)
#      [[3, [2, [1, [7, 3]]]], [6, [5, [4, [3, 3]]]]]
#      >>> add_to_rightmost([[3, [2, [8, 0]]], [9, [5, [4, [3, 2]]]]], 1)
#      [[3, [2, [8, 0]]], [9, [5, [4, [3, 3]]]]]
#      """
#      if isinstance(val, int):
#          return val + how_mutch
#      val1, val2 = val
#      return [val1, add_to_rightmost(val2, how_mutch)]


#  def magnitudeOLD(val) -> int:
#      """
#      >>> magnitude(9)
#      9
#      >>> magnitude([[1,2],[[3,4],5]])
#      143
#      >>> magnitude([[[[8,7],[7,7]],[[8,6],[7,7]]],[[[0,7],[6,6]],[8,7]]])
#      3488
#      """
#      if isinstance(val, int):
#          return val
#      val1, val2 = val
#      return magnitude(val1) * 3 + magnitude(val2) * 2


#  def flatten(val: lislist) -> t:
#      for val in list:
#          if isinstance(val, list):
#              yield from flatten(val)
#          else:
#              yield val


def dtov(dataval, parent=None, level=1, debug_allow_level5=False):
    """Data (arrays) TO Value

    >>> dtov([9, 8]).to_list()
    [9, 8]
    >>> dtov([[9, 8], 1]).to_list()
    [[9, 8], 1]
    >>> dtov([[[[8, 7], [7, 7]], [8, 6]], [[[0, 7], [6, 6]], [8, 7]]]).to_list()
    [[[[8, 7], [7, 7]], [8, 6]], [[[0, 7], [6, 6]], [8, 7]]]
    """
    if isinstance(dataval, int):
        return dataval
    if not debug_allow_level5:
        assert level < 5
    assert isinstance(dataval, list) and len(dataval) == 2, dataval
    dval1, dval2 = dataval
    val = Value(parent=parent, val1=None, val2=None, level=level)
    val.val1 = dtov(
        dval1, parent=val, level=level + 1, debug_allow_level5=debug_allow_level5
    )
    val.val2 = dtov(
        dval2, parent=val, level=level + 1, debug_allow_level5=debug_allow_level5
    )
    return val


@timeme
def part1(data):
    "Do the part1 calculation"
    global SCAF
    SCAF = True
    data = [eval(line) for line in data]
    values = list(map(dtov, data))
    #  print("values", values)
    assert data == [val.to_list() for val in values]

    res = None
    for val in values:
        if res is None:
            res = val
            continue
        res += val
    return res.magnitude()


@timeme
def part2(_data):
    "Do the part2 calculation"


if __name__ == "__main__":
    main()
