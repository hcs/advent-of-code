#!/bin/env python3

import functools
import time

import input_readers


def timeme(method):
    """Decorator that prints the elapsed time after a call"""

    @functools.wraps(method)
    def wrapper(*args, **kw):
        start_time = time.time()
        result = method(*args, **kw)
        end_time = time.time()
        print("timed", wrapper.__name__, end_time - start_time, "s")
        return result

    return wrapper


def main():
    "read input data, process it if necessary and call the part workers"

    # input_reader = input_readers.read_input_str
    # input_reader = input_readers.read_input_int_list_multiline
    # input_reader = input_readers.read_input_int_list_comma
    input_reader = input_readers.read_input_str_list
    # input_reader = input_readers.read_input_table_tab

    example_data = input_reader(input_file=input_readers.EXAMPLE_INPUT_FILE)
    data = input_reader(input_file=input_readers.INPUT_FILE)

    part1_example = part1(example_data)
    part1_example_correct = 15
    assert (
        part1_example == part1_example_correct
    ), f"part 1 example expected {part1_example_correct} but got {part1_example}"
    print("part1:", part1(data))

    part2_example = part2(example_data)
    part2_example_correct = 1134
    assert (
        part2_example == part2_example_correct
    ), f"part 2 example expected {part2_example_correct} but got {part2_example}"
    print("part2:", part2(data))


def find_minimas(data):
    rows = len(data)
    cols = len(data[0])
    minimas = []  # [(row,col,val,risk)]
    for row in range(rows):
        for col in range(cols):
            val = int(data[row][col])
            surrounding = []
            if row > 0:
                surrounding.append(int(data[row - 1][col]))
            if col > 0:
                surrounding.append(int(data[row][col - 1]))
            if row < rows - 1:
                surrounding.append(int(data[row + 1][col]))
            if col < cols - 1:
                surrounding.append(int(data[row][col + 1]))
            if val < min(surrounding):
                risk = 1 + val
                minimas.append((row, col, val, risk))
    return minimas


@timeme
def part1(data):
    "Do the part1 calculation"
    minimas = find_minimas(data)

    return sum([risk for row, col, val, risk in minimas])


@timeme
def part2(data):
    "Do the part2 calculation"

    class Max:
        rows = len(data)
        cols = len(data[0])

    minimas = find_minimas(data)

    def find_surroundning_basin(row, col) -> set[tuple[int, int]]:
        surrounding = set()
        for row2, col2 in (
            (row - 1, col),
            (row, col - 1),
            (row + 1, col),
            (row, col + 1),
        ):
            match (row2, col2):
                case (-1, _):
                    continue
                case (_, -1):
                    continue
                case (Max.rows, _):
                    continue
                case (_, Max.cols):
                    continue
                case _ if data[row2][col2] != "9":
                    surrounding.add((row2, col2))
        return surrounding

    basin_sizes = []
    for mrow, mcol, _, _ in minimas:
        basin = {(mrow, mcol)}
        to_check = {(mrow, mcol)}  # have not checked the surrounding positions
        while to_check:
            row, col = to_check.pop()
            surrounding = find_surroundning_basin(row, col)
            to_check = to_check | (surrounding - basin)
            basin = basin | surrounding
        basin_sizes.append(len(basin))
    basin_sizes.sort()
    return basin_sizes[-1] * basin_sizes[-2] * basin_sizes[-3]


if __name__ == "__main__":
    main()
