#!/bin/env python3

import functools
import time

import input_readers
import utils


def timeme(method):
    """Decorator that prints the elapsed time after a call"""

    @functools.wraps(method)
    def wrapper(*args, **kw):
        start_time = time.time()
        result = method(*args, **kw)
        end_time = time.time()
        print("timed", wrapper.__name__, end_time - start_time, "s")
        return result

    return wrapper


def main():
    "read input data, process it if necessary and call the part workers"

    # input_reader = input_readers.read_input_str
    # input_reader = input_readers.read_input_int_list_multiline
    # input_reader = input_readers.read_input_int_list_comma
    input_reader = input_readers.read_input_str_list
    # input_reader = input_readers.read_input_table_tab

    example_data = input_reader(input_file=input_readers.EXAMPLE_INPUT_FILE)
    data = input_reader(input_file=input_readers.INPUT_FILE)

    part1_example = part1(example_data)
    part1_example_correct = 40
    assert (
        part1_example == part1_example_correct
    ), f"part 1 example expected {part1_example_correct} but got {part1_example}"
    print("part1:", part1(data))

    part2_example = part2(example_data)
    part2_example_correct = 315
    assert (
        part2_example == part2_example_correct
    ), f"part 2 example expected {part2_example_correct} but got {part2_example}"
    print("part2:", part2(data))


@timeme
def part1_2(data, part_2=False):
    "Do the part2 calculation"
    risks = dict()
    for (lno, line) in enumerate(data):
        for (cno, risk) in enumerate(line):
            risks[(lno, cno)] = int(risk)
    best_path_to = dict()  # (l,c) -> (risk_sum, [(l;c),...])
    best_path_to[(0, 0)] = (0, [(0, 0)])
    dlsize, dcsize = utils.board_size(risks)
    if part_2:
        lsize = dlsize * 5
        csize = dcsize * 5
    else:
        lsize = dlsize
        csize = dcsize
    print("size", lsize, csize)

    def get_risk(pos):
        line, col = pos
        l_tile = line // dlsize
        c_tile = col // dcsize
        rline = line % dlsize
        rcol = col % dcsize
        base_risk = risks[(rline, rcol)]

        risk = base_risk + l_tile + c_tile
        if risk > 9:
            risk -= 9
        assert risk < 10
        assert risk >= 1
        return risk

    if part_2:
        risks2 = dict()
        for line in range(lsize):
            for col in range(csize):
                risks2[(line, col)] = get_risk((line, col))
    else:
        risks2 = dict(risks)
    utils.print_board(risks2, col_first=False)

    def surrounding(pos) -> list[tuple]:
        line, col = pos
        for pos2 in (
            (line - 1, col),
            (line, col - 1),
            (line + 1, col),
            (line, col + 1),
        ):
            line2, col2 = pos2
            if line2 < 0 or col2 < 0 or line2 >= lsize or col2 >= csize:
                continue
            #  print("sur", pos, pos2)
            yield pos2

    #  def calc_ref_route():
    #     "walk one of the shortest routes to have a sum used for initial sanity checks"
    #      refroute = [(0, 0)]
    #      rsum = 0
    #      for shell in range(1, max(lsize, csize)):
    #          line = min(shell, lsize)
    #          col = min(shell, csize)
    #          refroute.append((line, col - 1))
    #          rsum += get_risk((line, col - 1))
    #          refroute.append((line, col))
    #          rsum += get_risk((line, col))
    #      return rsum, refroute

    #  ref_route = calc_ref_route()
    #  print(ref_route)

    #  def calc(pos, only_want_less_than=ref_route[0] + 1):
    def calc(pos):
        #  print("calc", pos)
        # om vi redan vet kortaste vägen, svara inte
        if (val := best_path_to.get(pos)) is not None:
            #  print(pos, val)
            rsum, _ = val
            #  print("already know", pos, "->", rsum)
            return rsum
        line, col = pos
        #  ref_path_to_here = None #
        best_found_frompos = None  # (rsum, pos)
        # lets find the shortest already calculated route
        for frompos in surrounding(pos):
            if frompos in best_path_to:
                rsum, path = best_path_to[frompos]
                #  print(frompos, 'is', rsum)
                if best_found_frompos is None:
                    best_found_frompos = rsum, frompos
                elif best_found_frompos[0] > rsum:
                    # overwrite if better
                    best_found_frompos = rsum, frompos
        assert best_found_frompos is not None
        # TODO calc recursive
        #  print("least found for", pos,  best_found_frompos[0], best_found_frompos[1],
        # "adding", get_risk(pos))
        best_path_to[pos] = (
            best_found_frompos[0] + get_risk(pos),
            best_path_to[best_found_frompos[1]][1] + [pos],
        )
        return best_path_to[pos][0]

    #  sorted((rsum, from_pos) for from_pos in ((),(),(),())))

    #  to_check = []
    for shell in range(1, max(lsize, csize)):
        #  print("SHELL", shell)
        to_calc = []
        to_calc.extend((line, shell) for line in range(shell))
        to_calc.extend((shell, col) for col in range(shell + 1))
        #  print("to_calc", to_calc)
        while to_calc:
            pos = to_calc.pop(0)
            calc(pos)
            #  print(pos, rsum)
            if pos == (lsize - 1, csize - 1):
                for ppos in best_path_to[pos][1]:
                    risks2[ppos] = "X"
                rsum, path = best_path_to[pos]

                #  utils.print_board(risks2, col_first=False)
                pathmap = dict()
                for pos in path:
                    pathmap[pos] = get_risk(pos)
                #  utils.print_board(pathmap, col_first=False)

                #  utils.print_board(
                #      best_path_to,
                #      get_char=lambda board, pos: f'{board.get(pos, (".", []))[0]:3}',
                #      end=" ",
                #      col_first=False,
                #  )
                return rsum


@timeme
def part1(data):
    "Do the part1 calculation"
    return part1_2(data, part_2=False)


@timeme
def part2(data):
    "Do the part2 calculation"
    return part1_2(data, part_2=True)


if __name__ == "__main__":
    main()


# part1 correct 390
# part2 2822 is to high
