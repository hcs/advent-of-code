#!/bin/env python3

import collections
import functools
import time

import input_readers


def timeme(method):
    """Decorator that prints the elapsed time after a call"""

    @functools.wraps(method)
    def wrapper(*args, **kw):
        start_time = time.time()
        result = method(*args, **kw)
        end_time = time.time()
        print("timed", wrapper.__name__, end_time - start_time, "s")
        return result

    return wrapper


def main():
    "read input data, process it if necessary and call the part workers"

    # input_reader = input_readers.read_input_str
    # input_reader = input_readers.read_input_int_list_multiline
    # input_reader = input_readers.read_input_int_list_comma
    input_reader = input_readers.read_input_str_list
    # input_reader = input_readers.read_input_table_tab

    example_data = input_reader(input_file=input_readers.EXAMPLE_INPUT_FILE)
    data = input_reader(input_file=input_readers.INPUT_FILE)

    example_data = parse_data(example_data)
    data = parse_data(data)

    part1_example = part1(example_data)
    part1_example_correct = 26
    assert (
        part1_example == part1_example_correct
    ), f"part 1 example expected {part1_example_correct} but got {part1_example}"
    print("part1:", part1(data))

    part2_example = part2(example_data)
    part2_example_correct = 61229
    assert (
        part2_example == part2_example_correct
    ), f"part 2 example expected {part2_example_correct} but got {part2_example}"
    print("part2:", part2(data))


def parse_data(data):
    res = []
    for line in data:
        left, right = line.split(" | ")
        left = tuple(left.strip().split(" "))
        right = tuple(right.strip().split(" "))
        res.append((left, right))
    # print(res)
    return tuple(res)


@timeme
def part1(data):
    "Do the part1 calculation"
    counter = 0
    for _left, right in data:
        for val in right:
            if len(val) in (2, 4, 3, 7):
                counter += 1
    return counter


def identify_digits(left: tuple[str]):
    """
     a
    b c
     d
    e f
     g
    """
    assert len(left) == 10
    nm = number_map = {}  # int -> str
    bl = by_length = collections.defaultdict(list)
    for val in left:
        by_length[len(val)].append(set(val))

    number_map[1] = set(by_length[2].pop())  # 1 is the only one with 2 segments
    number_map[4] = set(by_length[4].pop())  # 4 is the only one with 4 segments
    number_map[7] = set(by_length[3].pop())  # 7 is the only one with 3 segments
    number_map[8] = set(by_length[7].pop())  # 8 is the only one with 7 segments

    # acedgfb cdfbe gcdfa fbcad dab cefabd cdfgeb eafb cagedb ab
    # 6seg 3st  0 6 9
    # 5seg 3st  2 3 5
    a = nm[7] - nm[1]
    cf = nm[1]
    bd = nm[4] - cf
    eg = nm[8] - nm[7] - nm[4]
    assert len(eg) == 2
    # look at the 6seg
    # The only 6 seg without both cf is 6
    nm[6] = [val for val in bl[6] if not val.issuperset(cf)][0]
    bl[6].remove(nm[6])

    # The only 6 seg left with both bd is 9
    nm[9] = [val for val in bl[6] if val.issuperset(bd)][0]
    bl[6].remove(nm[9])

    # only 6 seg left is now is 0
    nm[0] = bl[6].pop()

    c = nm[8] - nm[6]
    d = nm[8] - nm[0]
    e = nm[8] - nm[9]
    b = bd - d
    f = cf - c
    g = nm[8] - a - b - c - d - e - f

    nm[2] = a | c | d | e | g
    nm[3] = a | c | d | f | g
    nm[5] = a | b | d | f | g

    assert len(nm) == 10
    return {frozenset(segments): number for number, segments in nm.items()}


def decode(left, right):
    digits = identify_digits(left)
    #  print(digits)
    res = []
    for val in right:
        #  print(val, frozenset(val))
        res.append(str(digits[frozenset(val)]))
    #  print(res)
    return int("".join(res))


@timeme
def part2(data):
    "Do the part2 calculation"
    res = 0
    for left, right in data:
        number = decode(left, right)
        res += number
    return res


if __name__ == "__main__":
    main()
