#!/bin/env python3

import functools
import time

import input_readers


def timeme(method):
    """Decorator that prints the elapsed time after a call"""

    @functools.wraps(method)
    def wrapper(*args, **kw):
        start_time = time.time()
        result = method(*args, **kw)
        end_time = time.time()
        print("timed", wrapper.__name__, end_time - start_time, "s")
        return result

    return wrapper


def main():
    "read input data, process it if necessary and call the part workers"

    # input_reader = input_readers.read_input_str
    # input_reader = input_readers.read_input_int_list_multiline
    input_reader = input_readers.read_input_int_list_comma
    # input_reader = input_readers.read_input_str_list
    # input_reader = input_readers.read_input_table_tab

    example_data = input_reader(input_file=input_readers.EXAMPLE_INPUT_FILE)
    data = input_reader(input_file=input_readers.INPUT_FILE)

    part1_example = part1(example_data)
    part1_example_correct = 37
    assert (
        part1_example == part1_example_correct
    ), f"part 1 example expected {part1_example_correct} but got {part1_example}"
    print("part1:", part1(data))

    part2_example = part2(example_data)
    part2_example_correct = 168
    assert (
        part2_example == part2_example_correct
    ), f"part 2 example expected {part2_example_correct} but got {part2_example}"
    print("part2:", part2(data))


@timeme
def part1(data):
    "Do the part1 calculation"
    #  avg = round(sum(data) / len(data))
    p_min = min(data)
    p_max = max(data)
    each_fuel_consumed = []
    for target in range(p_min, p_max + 1):
        fuel_consumed = sum((abs(target - pos) for pos in data))
        #  print(target, "->", fuel_consumed)
        each_fuel_consumed.append(fuel_consumed)
    return min(each_fuel_consumed)


@timeme
def part2(data):
    "Do the part2 calculation"
    p_min = min(data)
    p_max = max(data)
    dist_fuel_map = {}  # {0: 0, 1: 1, 2: 3, 3: 6, 4: 10}
    dist_fuel = 0
    for dist in range(p_max - p_min + 1):
        dist_fuel += dist
        dist_fuel_map[dist] = dist_fuel
    #  print(dist_fuel_map)
    each_fuel_consumed = []
    for target in range(p_min, p_max + 1):
        fuel_consumed = sum((dist_fuel_map[abs(target - pos)] for pos in data))
        #  print(target, "->", fuel_consumed)
        each_fuel_consumed.append(fuel_consumed)
    return min(each_fuel_consumed)


if __name__ == "__main__":
    main()
