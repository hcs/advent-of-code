#!/bin/env python3

import functools
import time

import networkx as nx

import input_readers


def timeme(method):
    """Decorator that prints the elapsed time after a call"""

    @functools.wraps(method)
    def wrapper(*args, **kw):
        start_time = time.time()
        result = method(*args, **kw)
        end_time = time.time()
        print("timed", wrapper.__name__, end_time - start_time, "s")
        return result

    return wrapper


def main():
    "read input data, process it if necessary and call the part workers"

    # input_reader = input_readers.read_input_str
    # input_reader = input_readers.read_input_int_list_multiline
    # input_reader = input_readers.read_input_int_list_comma
    input_reader = input_readers.read_input_str_list
    # input_reader = input_readers.read_input_table_tab

    example_data = input_reader(input_file=input_readers.EXAMPLE_INPUT_FILE)
    data = input_reader(input_file=input_readers.INPUT_FILE)

    part1_example = part1(example_data)
    part1_example_correct = 10
    assert (
        part1_example == part1_example_correct
    ), f"part 1 example expected {part1_example_correct} but got {part1_example}"
    print("part1:", part1(data))

    part2_example = part2(example_data)
    part2_example_correct = 36
    assert (
        part2_example == part2_example_correct
    ), f"part 2 example expected {part2_example_correct} but got {part2_example}"
    print("part2:", part2(data))


@timeme
def part1(data):
    "Do the part1 calculation"
    gr = nx.Graph()
    edges = [line.split("-") for line in data]
    gr.add_edges_from(edges)
    print(gr)
    paths = set()  # {[start,A,end]}
    incomplete = {("start",)}
    while incomplete:
        base = incomplete.pop()
        for cave in gr.adj[base[-1]]:
            path = base + (cave,)
            if cave.islower() and cave in base:
                continue
            if cave == "end":
                # print(path)
                paths.add(path)
            else:
                incomplete.add(path)
    return len(paths)


@timeme
def part2(data):
    "Do the part2 calculation"
    gr = nx.Graph()
    edges = [line.split("-") for line in data]
    gr.add_edges_from(edges)
    print(gr)
    paths = set()  # {[start,A,end]}
    incomplete = {("start",)}
    while incomplete:
        base = incomplete.pop()
        for cave in gr.adj[base[-1]]:
            path = base + (cave,)
            if cave == "end":
                #  print(path)
                paths.add(path)
            elif cave == "start":
                continue
            elif cave.islower() and cave in base:
                # We may visit a single small cave twice
                if path[0] == "visited_a_small_cave_twice":
                    continue
                path = ("visited_a_small_cave_twice",) + path  # add marker to path
                incomplete.add(path)
            else:
                incomplete.add(path)
    return len(paths)


if __name__ == "__main__":
    main()
