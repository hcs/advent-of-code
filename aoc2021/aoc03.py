#!/bin/env python3

import functools
import time

import input_readers


def timeme(method):
    """Decorator that prints the elapsed time after a call"""

    @functools.wraps(method)
    def wrapper(*args, **kw):
        start_time = time.time()
        result = method(*args, **kw)
        end_time = time.time()
        print("timed", wrapper.__name__, end_time - start_time, "s")
        return result

    return wrapper


def main():
    "read input data, process it if necessary and call the part workers"
    # data = input_readers.read_input_str()
    # data = input_readers.read_input_int_list_multiline()
    data = input_readers.read_input_str_list()
    # data = input_readers.read_input_table_tab()

    assert (
        part1(
            """00100
11110
10110
10111
10101
01111
00111
11100
10000
11001
00010
01010""".splitlines()
        )
        == 198
    )
    print("part1:", part1(data))

    # assert part2('') ==
    assert (
        part2(
            """00100
11110
10110
10111
10101
01111
00111
11100
10000
11001
00010
01010""".splitlines()
        )
        == 230
    )
    print("part2:", part2(data))


@timeme
def part1(_data):
    "Do the part1 calculation"
    gamma_rate_bits = ""
    epsilon_rate_bits = ""

    counts = {}

    # count bit occurance in each pos
    for row in _data:
        for pos, char in enumerate(row):
            counts[char][pos] = counts.setdefault(char, {}).get(pos, 0) + 1
    print(counts)

    # analyze counts
    for pos in range(len(counts["0"])):
        if counts["0"][pos] > counts["1"][pos]:
            gamma_rate_bits += "0"
            epsilon_rate_bits += "1"
        else:
            gamma_rate_bits += "1"
            epsilon_rate_bits += "0"
    print("g", gamma_rate_bits, int(gamma_rate_bits, base=2))
    print("e", epsilon_rate_bits, int(epsilon_rate_bits, base=2))

    power_consumption = int(gamma_rate_bits, base=2) * int(epsilon_rate_bits, base=2)
    return power_consumption


@timeme
def part2(_data):
    "Do the part2 calculation"

    def count_bits(data):
        counts = {}

        # count bit occurance in each pos
        for row in data:
            for pos, char in enumerate(row):
                counts[char][pos] = counts.setdefault(char, {}).get(pos, 0) + 1
        return counts

    oxy_data = list(_data)
    co2_data = list(_data)

    for pos in range(len(_data[0])):
        print(pos, len(oxy_data), len(co2_data))

        # drop unwanted values
        if len(oxy_data) > 1:
            counts = count_bits(oxy_data)
            most_common = "0" if counts["0"][pos] > counts["1"][pos] else "1"
            oxy_data = [row for row in oxy_data if row[pos] == most_common]
        if len(co2_data) > 1:
            counts = count_bits(co2_data)
            least_common = "0" if counts["0"][pos] <= counts["1"][pos] else "1"
            co2_data = [row for row in co2_data if row[pos] == least_common]

        print("most_common", most_common, "least_common", least_common)

        #  print('oxy', oxy_data)
        #  print('co2', co2_data)
        if len(oxy_data) == len(co2_data) == 1:
            break

    life_support_rating = int(oxy_data[0], base=2) * int(co2_data[0], base=2)
    return life_support_rating

    #   3529911 is to low


if __name__ == "__main__":
    main()
