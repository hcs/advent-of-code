#!/bin/env python3

import functools
import itertools
import time

import input_readers


def timeme(method):
    """Decorator that prints the elapsed time after a call"""

    @functools.wraps(method)
    def wrapper(*args, **kw):
        start_time = time.time()
        result = method(*args, **kw)
        end_time = time.time()
        print("timed", wrapper.__name__, end_time - start_time, "s")
        return result

    return wrapper


def main():
    "read input data, process it if necessary and call the part workers"

    # input_reader = input_readers.read_input_str
    # input_reader = input_readers.read_input_int_list_multiline
    # input_reader = input_readers.read_input_int_list_comma
    input_reader = input_readers.read_input_str_list
    # input_reader = input_readers.read_input_table_tab

    example_data = input_reader(input_file=input_readers.EXAMPLE_INPUT_FILE)
    data = input_reader(input_file=input_readers.INPUT_FILE)

    part1_example = part1(example_data)
    part1_example_correct = 1656
    assert (
        part1_example == part1_example_correct
    ), f"part 1 example expected {part1_example_correct} but got {part1_example}"
    print("part1:", part1(data))

    part2_example = part2(example_data)
    part2_example_correct = 195
    assert (
        part2_example == part2_example_correct
    ), f"part 2 example expected {part2_example_correct} but got {part2_example}"
    print("part2:", part2(data))


def surroundning(row, col):
    """returns a list of (row, col)"""
    res = []
    for rowd, cold in [
        (-1, -1),
        (-1, 0),
        (-1, 1),
        (0, -1),
        (0, 1),
        (1, -1),
        (1, 0),
        (1, 1),
    ]:
        row2, col2 = row + rowd, col + cold
        if row2 < 0 or row2 > 9 or col2 < 0 or col2 > 9:
            continue
        res.append((row2, col2))
    #  print(f"({row},{col}) -> {res}")
    return res


def print_data(data, msg=None):
    if msg:
        print(msg)
    for line in data:
        print("".join(map(str, line)))


@timeme
def part1(data):
    "Do the part1 calculation"
    data = [list(map(int, line)) for line in data]
    flash_counter = 0
    #  print_data(data, "Before any steps")
    for _step in range(1, 101):
        to_flash = []
        for row in range(10):
            for col in range(10):
                new_val = data[row][col] = (data[row][col] + 1) % 10
                if new_val == 0:
                    to_flash.append((row, col))
        #  print(to_flash)
        while to_flash:
            flash_counter += 1
            row, col = to_flash.pop(0)
            for srow, scol in surroundning(row, col):
                if data[srow][scol] == 0:
                    continue  # already flashed during this step
                new_val = data[srow][scol] = (data[srow][scol] + 1) % 10
                if new_val == 0:
                    to_flash.append((srow, scol))
        #  print_data(data, f"After step {_step}")
    return flash_counter


@timeme
def part2(data):
    "Do the part2 calculation"
    data = [list(map(int, line)) for line in data]
    #  print_data(data, "Before any steps")
    for _step in itertools.count(start=1):
        flash_counter = 0
        to_flash = []
        for row in range(10):
            for col in range(10):
                new_val = data[row][col] = (data[row][col] + 1) % 10
                if new_val == 0:
                    to_flash.append((row, col))
        #  print(to_flash)
        while to_flash:
            flash_counter += 1
            row, col = to_flash.pop(0)
            for srow, scol in surroundning(row, col):
                if data[srow][scol] == 0:
                    continue  # already flashed during this step
                new_val = data[srow][scol] = (data[srow][scol] + 1) % 10
                if new_val == 0:
                    to_flash.append((srow, scol))
        print_data(data, f"After step {_step}")
        print(flash_counter)
        if flash_counter == 100:
            return _step


if __name__ == "__main__":
    main()
