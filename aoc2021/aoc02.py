#!/bin/env python3

import functools
import time

import input_readers


def timeme(method):
    """Decorator that prints the elapsed time after a call"""

    @functools.wraps(method)
    def wrapper(*args, **kw):
        start_time = time.time()
        result = method(*args, **kw)
        end_time = time.time()
        print("timed", wrapper.__name__, end_time - start_time, "s")
        return result

    return wrapper


def main():
    "read input data, process it if necessary and call the part workers"
    # data = input_readers.read_input_str()
    # data = input_readers.read_input_int_list_multiline()
    data = input_readers.read_input_str_list()
    # data = input_readers.read_input_table_tab()

    # assert part1('') ==
    print("part1:", part1(data))

    # assert part2('') ==
    print("part2:", part2(data))


@timeme
def part1(_data):
    "Do the part1 calculation"
    hor_poz = depth = 0
    for line in _data:
        command, val = line.split(" ")
        val = int(val)
        match command:
            case "up":
                depth -= val
            case "down":
                depth += val
            case "forward":
                hor_poz += val
            case _:
                raise Exception("Unknown command '{command}'")
    return hor_poz * depth


@timeme
def part2(_data):
    "Do the part2 calculation"
    aim = hor_poz = depth = 0
    for line in _data:
        command, val = line.split(" ")
        val = int(val)
        match command:
            case "up":
                aim -= val
            case "down":
                aim += val
            case "forward":
                hor_poz += val
                depth += aim * val
            case _:
                raise Exception("Unknown command '{command}'")
    return hor_poz * depth


if __name__ == "__main__":
    main()
